package pracownicy;

import java.io.*;
import java.util.Scanner;

public class Pracownik {
    private String imie;
    private String nazwisko;
    private int placa;
    private char plec;
    private int dzial;

    Pracownik() {
    }

    Pracownik(String imie, String nazwisko, int placa, char plec, int dzial) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.placa = placa;
        this.plec = plec;
        this.dzial = dzial;
    }

    public static Pracownik pobraniePracownika() {
        Pracownik nowyPracownik = new Pracownik();
        Scanner input = new Scanner(System.in);

        System.out.println("Podaj Imię: ");
        nowyPracownik.setImie(input.nextLine());

        System.out.println("Podaj Nazwisko: ");
        nowyPracownik.setNazwisko(input.nextLine());                    //String stringNazwisko = input.nextLine();

        System.out.println("Podaj Płacę: ");
        nowyPracownik.setPlaca(Integer.parseInt(input.nextLine()));     //int intPlaca = Integer.parseInt(input.nextLine());

        System.out.println("Podaj Płeć (K/M): ");
        nowyPracownik.setPlec((input.nextLine().charAt(0)));            //char charPlec= input.nextI();charAt(0);

        System.out.println("Podaj Dział: ");
        nowyPracownik.setDzial(Integer.parseInt(input.nextLine()));     //int intDzial = Integer.parseInt(input.nextLine());

        return nowyPracownik;
    }

    public static void wypisanieTablicy (Pracownik[] tablica) {
        System.out.println("Tablica wyglada teraz tak:");
        for (int lWier = 0; lWier < tablica.length; lWier++) {
            System.out.println(tablica[lWier].doZapisu());
        }
    }

    //Odczyt całej tablicy z pliku
    public static Pracownik[] odczytDoTablicy(String pathName, Pracownik[] clearTable) {
        Pracownik[] allWorkers = clearTable;
        try {
            File myObj = new File(pathName);
            Scanner myReader = new Scanner(myObj);
            int i = 0;
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] array = data.split(" ");
                if (array.length == 5) {  //czy lista ma 5 elementów
                    Pracownik worker = new Pracownik(array[0], array[1], Integer.parseInt(array[2]), array[3].charAt(0), Integer.parseInt(array[4]));
                    allWorkers[i] = worker;
                }
                i++;
            }
            myReader.close();
            System.out.println("Wczytano " + i + " pracowników, czyli " + i*5 + " komórek danych.");
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred." + e);
            e.printStackTrace();
        }
        return allWorkers;
    }


    //Srednie zarobki
    public static int srednieZarobki (Pracownik[] workersTable, int dzial, char plec) {
        Pracownik[] allWorkers = workersTable;
        int dept = dzial;
        char sex = plec;
        int sumaPlac = 0;
        int ilePlac = 0;
        int srednia=0;
        for (int i = 0; i < allWorkers.length; i++) {
            if (allWorkers[i].getDzial() == dzial && allWorkers[i].getPlec() == sex) {
                sumaPlac += allWorkers[i].getPlaca();
                ilePlac++;
            }
        }
        if (ilePlac != 0) {
            srednia = sumaPlac / ilePlac;
        } else {
            System.out.println("Błędne żądanie");
        }
        return srednia;
    }

    //Zapis całej tablicy (nadpisanie pliku)
    public static void zapisTablicy(String pathname, Pracownik[] tablicaDoZapisu) {
        try {
            FileWriter myWriter = new FileWriter(pathname);

            for (int i=0; i< tablicaDoZapisu.length; i++) {
                myWriter.write(tablicaDoZapisu[i].doZapisu()+"\n");
            }
            myWriter.close(); //po tym już nic nie dodamy
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred. " + e);
            e.printStackTrace();
        }
    }

    //Zapis jednej linii na końcu
    public static void dopisanieLinii(String pathname, Pracownik pracownikDoZapisu) {
        try {
            FileWriter myWriter = new FileWriter(pathname, true);
            myWriter.write(pracownikDoZapisu.doZapisu()+"\n");
            myWriter.close(); //po tym już nic nie dodamy
            System.out.println("Successfully wrote one line to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred. " + e);
            e.printStackTrace();
        }
    }
    //   BufferedWriter bw = new BufferedWriter();

    //Tworzenie pliku
    public static void tworzeniePliku (String pathName) {
        try {
            File myObj = new File(pathName);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }


    //Test pliku
    public static void testPliku(String pathname) {
        File myObj = new File(pathname);
        if (myObj.exists()) {
            System.out.println("File name: " + myObj.getName());
            System.out.println("Absolute path: " + myObj.getAbsolutePath());
            System.out.println("Writeable: " + myObj.canWrite());
            System.out.println("Readable " + myObj.canRead());
            System.out.println("File size in bytes " + myObj.length());
        } else {
            System.out.println("The file does not exist.");
        }
    }


    /**
     * Count file rows.
     *
     * @param pathname file
     * @return file row count
     * @throws IOException
     */
    public static int getLineCount(String pathname) throws IOException {
        File myObj = new File(pathname);
        try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(myObj), 1024)) {

            byte[] c = new byte[1024];
            boolean empty = true,
                    lastEmpty = false;
            int count = 0;
            int read;
            while ((read = is.read(c)) != -1) {
                for (int i = 0; i < read; i++) {
                    if (c[i] == '\n') {
                        count++;
                        lastEmpty = true;
                    } else if (lastEmpty) {
                        lastEmpty = false;
                    }
                }
                empty = false;
            }

            if (!empty) {
                if (count == 0) {
                    count = 1;
                } else if (!lastEmpty) {
                    count++;
                }
            }
            System.out.println(count);
            return count;
        }
    }

    //Settery
    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setPlaca(int placa) {
        this.placa = placa;
    }

    public void setPlec(char plec) {
        this.plec = plec;
    }

    public void setDzial(int dzial) {
        this.dzial = dzial;
    }


    //Gettery
    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getPlaca() {
        return placa;
    }

    public char getPlec() {
        return plec;
    }

    public int getDzial() {
        return dzial;
    }

    public String doZapisu() {
        return imie + " " + nazwisko + " " + placa + " " + plec + " " + dzial;
    }

    @Override
    public String toString() {
        return "Pracownik{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", placa=" + placa +
                ", plec=" + plec +
                ", dzial=" + dzial +
                '}';
    }


}
