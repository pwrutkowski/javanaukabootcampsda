package pracownicy;

import java.io.IOException;
import java.util.Scanner;

public class PracownicyMain  {

    public static void main(String[] args) throws IOException {

        Scanner input = new Scanner(System.in);

        String pathName = "C:\\Users\\Rudy\\IdeaProjects\\JavaNaukaBootcampSDA\\src\\main\\java\\pracownicy\\pracownicy.txt";
        //String pathName2 = "C:\\Users\\Rudy\\IdeaProjects\\JavaNaukaBootcampSDA\\src\\main\\java\\pracownicy\\pracownicy2.txt";

        int linesNumber = Pracownik.getLineCount(pathName);
        Pracownik[] clearTable = new Pracownik[linesNumber];
        Pracownik[] workersTable = Pracownik.odczytDoTablicy(pathName, clearTable);
        Pracownik.wypisanieTablicy(workersTable);

        System.out.println("Czy chcesz wprowadzić nowego pracownika? (T/N)");
        char decyzja= input.nextLine().charAt(0);
        if (decyzja == 'T') {
            Pracownik nowyPracownik = Pracownik.pobraniePracownika();
            Pracownik.dopisanieLinii(pathName, nowyPracownik);
           //Załadowanie nowej większej tablicy
            linesNumber = Pracownik.getLineCount(pathName);
            Pracownik[] clearTable2 = new Pracownik[linesNumber];
            workersTable = Pracownik.odczytDoTablicy(pathName, clearTable2);
            Pracownik.wypisanieTablicy(workersTable);
        }

        //Pracownik.tworzeniePliku(pathName2);
        Pracownik.testPliku(pathName);
        //Pracownik.zapisTablicy(pathName2, workersTable);
        //Pracownik.odczytProsty(pathname);

        System.out.println();

        System.out.println("Srednia wynosi: " + Pracownik.srednieZarobki(workersTable, 3, 'M'));

    }
}
