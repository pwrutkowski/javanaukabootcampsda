package podstawy;

import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class WyrazeniaRegularne {
    public static void main(String[] args) {
        /*Wyrażenia regularne / regexp
        Przeszukiwanie tekstu, logów. Sprawdzanie w formularzach internetowych, czy wprowadzono dane we właściwy sposób
        czy jest ul. lub al., czy jest numer domu, czy mail ma dobry format.
         */

        //klasa pattern
        //klasa matcher
        //Pierwszy przykład
        System.out.println("\nPierwszy przykład:");
        //Pattern pattern = Pattern.compile("w3schools", Pattern.CASE_INSENSITIVE);
        Pattern pattern = Pattern.compile(".*w3schools.*", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher("Visit W3Schools!");
        //boolean matchFound = matcher.find();      //czy w wyrażeniu jest zawarte szukany pattern
        boolean matchFound = matcher.matches();  //czy nasze wyrażenie pokrywa całe przeszukiwane zdanie
        if(matchFound) {
            System.out.println("Match found");
        } else {
            System.out.println("Match not found");
        }

        //Drugi przykład
        System.out.println("\nDrugi przykład:");
        Pattern pattern2 = Pattern.compile("[A-Za-z\\s]+ \\d+ zl odsetki wynosza \\d+ zl", Pattern.CASE_INSENSITIVE);
        //Pattern pattern2 = Pattern.compile("[\w \s]", Pattern.CASE_INSENSITIVE)
        Matcher matcher2 = pattern2.matcher("Mam kredyt w wysokosci 1000 zł, odsetki wynosza 200 zł.");
        boolean matchFound2 = matcher2.matches();  //czy nasze wyrażenie pokrywa całe przeszukiwane zdanie
        if(matchFound2) {
            System.out.println("Match found");
        } else {
            System.out.println("Match not found");
        }

        //Cwiczenie
        System.out.print("\nCwiczenie:\nWpisz adres e-mail: ");
        //Scanner scan = new Scanner(System.in).useLocale(Locale.US); //raz wystarczy przywołać, ale oczekuje polskiego separatora dziesiętnego. jeśli chcemy przyjmował kropki to ".localeUS"
        //String inputCW = scan.nextLine();
        Pattern patternCW = Pattern.compile("\\w+@\\w+\\.[a-z]{2,3}", Pattern.CASE_INSENSITIVE);
        Matcher matcherCW = patternCW.matcher("rudziu@gmail.com");
        //Matcher matcherCW = patternCW.matcher(inputCW);
        boolean matchFoundCW = matcherCW.matches();  //czy nasze wyrażenie pokrywa całe przeszukiwane zdanie
        if(matchFoundCW){
            System.out.println("Adres OK");
        } else {
            System.out.println("Format adresu niewłaściwy");
        }

        //Cwiczenie 2
        System.out.print("\nCwiczenie 2:\nWpisz adres: ");
        //Scanner scan = new Scanner(System.in).useLocale(Locale.US); //raz wystarczy przywołać, ale oczekuje polskiego separatora dziesiętnego. jeśli chcemy przyjmował kropki to ".localeUS"
        //String inputCW = scan.nextLine();
        Pattern patternCW2 = Pattern.compile("[A-Za-z]{2}\\.\\s?([A-Za-z0-9]+\\s)*[0-9]{1,4}[a-z]{0,1}\\W[0-9]{0,3}", Pattern.CASE_INSENSITIVE);
        Matcher matcherCW2 = patternCW2.matcher("ul. Waleriana 24");
        //Matcher matcherCW = patternCW.matcher(inputCW);
        boolean matchFoundCW2 = matcherCW2.matches();  //czy nasze wyrażenie pokrywa całe przeszukiwane zdanie
        if(matchFoundCW2){
            System.out.println("Adres OK");
        } else {
            System.out.println("Format adresu niewłaściwy");
        }

        //Trzeci przykład
        System.out.println("\nTrzeci przykład:");
        // String to be scanned to find the pattern.
        String line = "This order was placed for QT3000! OK?";
        String pattern3 = "(?<Pawel1>.*)(?<Pawel2>\\d+)(?<Pawel3>.*)";   //Cyfry conajmniej raz

        // Create a Pattern object
        Pattern r = Pattern.compile(pattern3);  // Tworzymy obiekt klasy pattern o nazwie "r"  zamieniający stringa na zdatny dla klasy matcher

        // Now create matcher object.
        Matcher m = r.matcher(line);
        if (m.find( )) {
            System.out.println("Found value indeks grupy (0): " + m.group(0) );
            System.out.println("Found value: " + m.group("Pawel1") );
            System.out.println("Found value: " + m.group("Pawel2") );
            System.out.println("Found value: " + m.group("Pawel3") );
        }else {
            System.out.println("NO MATCH");
        }

        //Czwarty przykład
        System.out.println("\nCzwarty przykład:");
        // String to be scanned to find the pattern.
        String line4 = "Ala ma kota. Kot ma na imie --Zygmunt--. Kot jest czarny.";
        String pattern4 = "[^-]*--(\\w+)(--).*";
        // Create a Pattern object
        Pattern r4 = Pattern.compile(pattern4);  // Tworzymy obiekt klasy pattern o nazwie "r"  zamieniający stringa na zdatny dla klasy matcher

        // Now create matcher object.
        Matcher m4 = r4.matcher(line4);
        if (m4.find( )) {
            System.out.println("Found value indeks grupy (0): " + m4.group(0) );
            System.out.println("Found value: " + m4.group(1));
            System.out.println("Found value: " + m4.group(2));
        }else {
            System.out.println("NO MATCH");
        }

        //Piąty przykład
        System.out.println("\nPiaty przykład:");
        // String to be scanned to find the pattern.
        String line5 = "Mam kredyt 3000 zl odsetki to 200 zl";
        //String pattern5 = "[A-Za-z\\s]+(\\d+)[A-Za-z\\s]+(\\d+).*";
        String pattern5 = "\\D+(\\d+)+\\D+(\\d+).*";
        // Create a Pattern object
        Pattern r5 = Pattern.compile(pattern5);  // Tworzymy obiekt klasy pattern o nazwie "r"  zamieniający stringa na zdatny dla klasy matcher

        // Now create matcher object.
        Matcher m5 = r5.matcher(line5);
        if (m5.find( )) {
            System.out.println("Found value indeks grupy (0): " + m5.group(0) );
            System.out.println("Found value: " + m5.group(1));
            System.out.println("Found value: " + m5.group(2));
        }else {
            System.out.println("NO MATCH");
        }

// Outputs Match found
    }
}
