package podstawy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import static java.lang.Integer.valueOf;

/*
    List<Integer> przykladowaLista = new ArrayList<>();  //deklaracja tablicy, nie możemy używać typów prymitywnych, muszą być klasy opakowywujące
    przykladowaLista.add(9);         //nie musimy pamiętać o indeksie dodając element

    int[] przykladowaTablica new int[10];   //deklaracja tablicy - możemy używać typy prymitywne, deklarujemy rozmiar na początku
    for(Integer elementTablicy:przykladowaTablica){
    System.out.println(elementTablicy);
    }

    De facto lista jest interfejsem, klasą bez ciała.
    interfejsy mogą być różne: ArrayList - implementacja tablicowa, LinkedList - implementacja wiązana - częste pytanie na rozmowach kwalifikacyjnych


 */


public class Listy {
    public static void main(String[] args) {
        System.out.println("Lista pierwsza: z liczbami");
        List<Integer> mojaLista = new ArrayList<>();
        mojaLista.add(10);
        mojaLista.add(5);
        for (Integer elementListy:mojaLista) {
            System.out.println(elementListy);
        }
        System.out.println("Wielkość listy (.size): " + mojaLista.size());
        mojaLista.remove(Integer.valueOf(5));
        System.out.println();
        for (Integer elementListy:mojaLista) {
            System.out.println(elementListy);
        }

        System.out.println("Lista z komentu, powtórka teorii");
        List<Integer> przykladowaLista = new ArrayList<>();
        przykladowaLista.add(9);
        przykladowaLista.add(99);
        przykladowaLista.add(999);
        przykladowaLista.add(2);
        przykladowaLista.add(9999);
        System.out.println(przykladowaLista);
        System.out.println("Elemet o indeksie 2, wywołujemy funkcją przykladowaLista.get(2). Wynik : " + przykladowaLista.get(2));

        przykladowaLista.remove(0);
        System.out.println("przykladowaLista.remove(0) " + przykladowaLista);
        przykladowaLista.remove(valueOf(2));
        System.out.println("przykladowaLista.remove(valueOf(2)) " + przykladowaLista);
        System.out.println(Collections.max(przykladowaLista));

        System.out.println("Tablica z komentu, powtórka teorii");
        int[] przykladowaTablica = new int[10];
        przykladowaTablica[1] = 6;
        for(Integer elementTablicy:przykladowaTablica) {
            System.out.print(elementTablicy + " ");
        }
    }

}
