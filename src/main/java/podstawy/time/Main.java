package podstawy.time;

/*
LocalTime
LocalDate
instant - liczba nanosekund od Epoch time - 1 stycznia 1970

java.time.Duration
java.time.Period
 */

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;

public class Main {
    public static void main(String[] args) {
        final long poczatek = System.currentTimeMillis();

        final LocalTime now = LocalTime.now().withNano(0); //.withSecond(50); - wymuszamy sekundy
        System.out.println(now);

        final LocalDate nowDate = LocalDate.now();
        System.out.println(nowDate);

        //    final Instant poczatek = Instant();


        final long koniec = System.currentTimeMillis();
        System.out.println(koniec-poczatek);

    }
}
