package podstawy.calculator;

import java.util.Arrays;

public class Calculator {
    private int[] array;

    Calculator(int[] array) {
        this.array = array;
    }

    public int getSum() {
        int sum = 0;
        for (int i : this.array) {
            sum += i;
        }
        return sum;

    }

    public int getDiff() {
        int diff = array[0];
        for (int i = 1; i<array.length; i++) {
            diff -= this.array[i];
        }
        return diff;
    }

    public int getMult() {
        int mult = 1;
        for (int i = 0; i<array.length; i++) {
            mult *= this.array[i];
        }
        return mult;
    }

    public double getDiv() {
        double[] doubles = Arrays.stream(array).asDoubleStream().toArray();
        double div = doubles[0];
        for (int i = 1; i<doubles.length; i++) {
            div = div/doubles[i];
        }
        return div;
    }

    public int getMod() {
        int mod = array[0];
        for (int i = 1; i<array.length; i++) {
            mod = mod%array[i];
        }
        return mod;
    }
}
