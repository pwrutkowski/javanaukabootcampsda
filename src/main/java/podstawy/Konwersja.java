package podstawy;

public class Konwersja {
    /*Konwersja typów || Rzutowanie
    Bezstratne:
                        char
                        \/
    byte --> short --> int --> long
                        |  \  /  |
                        \/  /\   \/

     Przy tych możemy stracić dane
         int --> float
         long --> float
         long --. double


         Rzutowanie / Kastowanie
         (typ_konwertowany)
         Jawne i niejawne
         */
    public static void main(String[] args) {
        int zmienna = 100;
        //Integer zmienna = 100;
        float kastowanie = (float) zmienna;
        double pi = 3.14;
        double result;
        result = pi + (double) zmienna;
        //String liczba=zmienna.toString();

        double liczba1 = 3.56;
        int m = (int) Math.round(liczba1);
        System.out.println("Rzutowanie odwrotne z double na int: " + m + " z zaokrągleniem Math.round");

        System.out.println(kastowanie + " " + result + " " + liczba1);
    }


}
