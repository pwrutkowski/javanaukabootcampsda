package podstawy.obiektowoscstatyczna;

public class Okrag {

    public static double getCirc(double rad) {
        Double circ = 2 * rad * Math.PI;
        return circ;
    }
    public static double getArea(double rad) {
        Double area = Math.pow(rad, 2) * Math.PI;
        return area;
    }

    /*
    Klasycznie byłoby:
    private Double promien;
    public Okrag (final Double promien) {
        this.promien = promien;
    }

    public Double getObwod(){
        Double obwod = 2*
        return obwod;
     */
}
