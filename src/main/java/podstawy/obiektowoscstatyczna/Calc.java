package podstawy.obiektowoscstatyczna;

import java.util.Arrays;

public class Calc {

    public static int getSum(int[] array) {
        int sum = 0;
        for (int i : array) {
            sum += i;
        }
        return sum;

    }

    public static int getDiff(int[] array) {
        int diff = array[0];
        for (int i = 1; i<array.length; i++) {
            diff -= array[i];
        }
        return diff;
    }

    public static int getMult(int[] array) {
        int mult = 1;
        for (int i = 0; i<array.length; i++) {
            mult *= array[i];
        }
        return mult;
    }

    public static double getDiv(int[] array) {
        double[] doubles = Arrays.stream(array).asDoubleStream().toArray();
        double div = doubles[0];
        for (int i = 1; i<doubles.length; i++) {
            if (doubles[i] == 0) {
                System.out.println("Błąd dzielenia przez zero!");
                continue;
            } else {
                div = div/doubles[i];
            }

        }
        return div;
    }

    public static int getMod(int[] array) {
        int mod = array[0];
        for (int i = 1; i<array.length; i++) {
            mod = mod%array[i];
        }
        return mod;
    }
}
