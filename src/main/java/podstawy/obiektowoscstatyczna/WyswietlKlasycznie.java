package podstawy.obiektowoscstatyczna;


public class WyswietlKlasycznie {
    public String poleTekstu;

    public WyswietlKlasycznie(String poleTekstu) {
        this.poleTekstu = poleTekstu;
    }
    public void wyswietlKlasycznie() {
        System.out.println("Mój tekst metodą niestatyczną to: " + poleTekstu);
    }


}
