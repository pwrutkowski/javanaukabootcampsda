package podstawy.obiektowoscstatyczna;

import java.util.Scanner;

public class OkragMain {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj promien");
        double rad = Double.parseDouble(scan.nextLine());

        System.out.println("Co chccesz obliczyć?");
        String operacja = scan.nextLine();
        switch (operacja) {
            case "obwod":
                System.out.println("Obwód okręgu o promieniu " + rad + " jednostek, wynosi: " +Okrag.getCirc(rad) + " jednostek");
                break;
            case "pole":
                System.out.println("Pole koła o promieniu " + rad + " jednostek, wynosi: " + Okrag.getArea(rad) + " jednostek^2");
                break;
            default:
                System.out.println("Wpis nierozpoznany");
                break;

        }

        /*
        Klasycznie byłoby
        Double promien = ;
        Okrag okrag = new Okrag();
        sout("Obwod " +okrag.getObwod() )
         */
    }

}
