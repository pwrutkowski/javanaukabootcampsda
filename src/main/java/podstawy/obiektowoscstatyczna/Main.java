package podstawy.obiektowoscstatyczna;

public class Main {
    public static void main(String[] args) {  //static pozwala nie tworzyć obiektu

        //    Wyswietl wyswietl = new Wyswietl("testowy tekst");

        Wyswietl.wyswietlTekst("testowy tekst");  //bez tworzenia obiekt - wywołanie metody statycznej
        //wywołujemy metodę "wyswietlTekst" z klasy "Wyswietl"


        WyswietlKlasycznie wyswietlK = new WyswietlKlasycznie("niestatycznie");
        wyswietlK.wyswietlKlasycznie();


        /*Klasy wewnętrzne
        Do stworzenia instancji klasy wewnętrznej potrzebujemy instancji klasy zewnętrznej

         */

        /*pola statyczne
        pola statyczne są atrybutem klasy a nie obiektu
        - czyli można się do niego odwołać bez potrzeby kreacji instancji obiektu.
        - nie trzeba tworzyć obiektów w innej klasie żeby to wywołać

        public static String sciezkaDoPliku = "C:\\mojplik.txt";

        System.out.println(Wyswietl.sciezkaDoPliku);
         */
        System.out.println("Pole statyczne: " + Wyswietl.sciezkaDoPliku);
    }

}
