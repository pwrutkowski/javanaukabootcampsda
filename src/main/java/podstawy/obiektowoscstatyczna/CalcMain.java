package podstawy.obiektowoscstatyczna;

import java.util.Scanner;

public class CalcMain {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Podaj ile liczb chcesz wprowadzić");
        int ileLiczb = Integer.parseInt(scan.nextLine());

        int[] array = new int[ileLiczb];

        for (int kol = 0; kol < array.length; kol++) {
            System.out.println("Podaj liczbę nr " + kol + ": ");
            int n = Integer.parseInt(scan.nextLine());
            array[kol] = n;
        }

        System.out.println("Jaką operację chcesz wykonać?");
        String operacja = scan.nextLine();
        switch (operacja) {
            case "dodawanie":
                System.out.println("Suma: " +Calc.getSum(array));
                break;
            case "odejmowanie":
                System.out.println("Różnica: " +Calc.getDiff(array));
                break;
            case "mnozenie":
                System.out.println("Mnożenie: " +Calc.getMult(array));
                break;
            case "dzielenie":
                System.out.println("Iloraz: " +Calc.getDiv(array));
                break;
            case "modulo":
                System.out.println("Modulo: " +Calc.getMod(array));
                break;
            default:
                System.out.println("Wpis nierozpoznany");
                break;

        }



    }
}
