package podstawy.obiektowoscstatyczna;

public class Wyswietl {
    public static String sciezkaDoPliku = "C:\\mojplik.txt";       //to jest pole statyczne, możemy je wywołac z maina bez tworzenia obiektu

    static void wyswietlTekst(final String tekst) {  //static pozwala nie tworzyc obiektu
        System.out.println("Mój tekst przywołany metodą statyczną to: " + tekst);



    }

}
