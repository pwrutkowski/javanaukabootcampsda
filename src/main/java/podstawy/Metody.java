package podstawy;

import java.util.Locale;
import java.util.Scanner;

public class Metody {
        /*
        Metody

        Metoda - worek grupujący zestaw instrukcji.
        Dlaczego się tworzy metody:
        - reużywalność powtarzających się fragmentów kodu
        - sensowny podział kodu na mniejsze części pozwala szybciej go zrozumieć
        Schemat definiowania:
        <typ zwracany=""> <nazwa metody="">(<opcjonalna lista="" argumentów="">) {
            <ciało metody>
        }</ciało></opcjonalna><>

        void printName(String name) {                       //void oznacza, że metoda nic nie zwraca - bez return
            System.out.println("My name is: " +name);
        }

        int x = y()j

        int y () {
        mięso metody
        returny z //musi być tego samego rodzaju co metoda (czyli int)
        }

        int diiff (int arg1, string arg2) {     //może być dowolnie dużo argumentów
            return arg1-arg2
        }       //ale return może być tylko jednego rodzaju, jak zadeklarowano w metodzie (int diff)

        void returnExample(int number) {        //metoda może zostać przerwana
            if (number % 2 == 0) {
                return;
                }

        }

        metoda powinna być krótka
        nazewnictwo : camelCase

         */
    static int dodLiczb(int l1, int l2) {
        int suma = l1 + l2;
        return suma;
    }
    static int odeLiczb(int l1, int l2) {
        int rozn = l1 - l2;
        return rozn;
    }
    static int mnoLiczb(int l1, int l2) {
        int iloc = l1 * l2;
        return iloc;
    }
    static int dziLiczb(int l1, int l2) {
        int ilor = l1 / l2;
        return ilor;
    }
    public static void main(String[] args) {
      //  int a1;
       // int a2;
        System.out.println("\nPodaj pierwszą liczbę: ");
        Scanner scan = new Scanner(System.in).useLocale(Locale.US);
        int a1 = Integer.parseInt(scan.nextLine());
        System.out.println("\nPodaj drugą liczbę: ");
        int a2 = Integer.parseInt(scan.nextLine());

        System.out.println("Pierwsza liczba to " + a1 + " a druga to " + a2);

        System.out.println("\nPodaj rodzaj działania: \nDodawanie - Wybierz \"1\"\nOdejmowanie l1-l2 - Wybierz \"2\"\nMnożenie - Wybierz \"3\"\nDzielenie l1/l2 - Wybierz \"4\"\n");
        int a3 = Integer.parseInt(scan.nextLine());
        System.out.println("Wpisałeś: "+ a3);

        switch (a3) {
            case 1:
                System.out.println("Wynik dodawania l1 i l2 to: " + dodLiczb(a1, a2));
                break;
            case 2:
                System.out.println("Wynik odejmowania l2 od l1 to: " + odeLiczb(a1, a2));
                break;
            case 3:
                System.out.println("Wynik mnożenia l1 przez l2 to: " + mnoLiczb(a1, a2));
                break;
            case 4:
                System.out.println("Wynik dzielenia l1 przez l2 to: " + dziLiczb(a1, a2));
                break;
            default:
                System.out.println("Coś nie halo");
        }

    }
}
//1465118/2021/6316860/01
//