package podstawy;

public class Petle {
    /*
    for
    while
    do while - ta się wykona conajmniej raz
     */
    public static void main(String[] args) {
        //Pierwsza pętla for (co i od jakiej wartości się zmienia ; do kiedy się zmienia ; jak się zmienia)
        for (int i = 1; i < 10; i++) {
            System.out.println("Hello World! po raz " + i);
        }
        System.out.println();

        //Parzyste z pętli for
        for (int i = 0; i <= 100; i = i + 2) {
            System.out.println("Kolejna liczba parzysta: " + i);
        }
        System.out.println();

        //Pętla for ze sprawdzeniem
        for (int i = 1; i < 10; i++) {
            if (i % 2 == 0) {
                System.out.println("Liczba " + i + " jest liczbą parzystą");
            } else {
                System.out.println("Liczba " + i + " jest liczbą nieparzystą");
            }
        }
        System.out.println();

        //String
        String[] array = {"Ala", "ma", "kota"};
        for (String element : array) {
            System.out.print(element + " ");
        }
        System.out.println();
        System.out.println();

        //Pętla while
        System.out.println("Pętla while");
        int j = 0;
        while (j <= 20) {
            System.out.print(j++ + " ");
            //++j;
            //j++;
            //j=j+5;
        }
        System.out.println();
        System.out.println();

        //Pętla do while
        int k = 0;
        System.out.println("Pętla do while");
        do {
            System.out.print(k++ + " ");
        }
        while (k <= 10);
        System.out.println();
        System.out.println();


        //Pętla z break
        System.out.println("Pętla do while z break");
        int l = 0;
        do {
            System.out.print(l + " ");
            if (l++ == 33) {
                break;                  //break przerywa przebieg pętli i wychodzi z niej
            }
        }
        while (l <= 100);

        System.out.println();
        System.out.println();

        //Pętla for ze sprawdzeniem i break
        for (int i = 1; i < 100; i++) {
            if (i % 2 == 0) {
                System.out.println("Liczba " + i + " jest liczbą parzystą");
            } else {
                System.out.println("Liczba " + i + " jest liczbą nieparzystą");
            }
            if (i == 33) {
                break;
            }
        }
        System.out.println();
        System.out.println();


        //For z break
        System.out.println("For z break");
        for (int i = 0; i <= 10; i++) {
            System.out.println("Hello World! " + i);
            if (i == 8) {
                break;
            }
        }

        System.out.println();
        System.out.println();

        //Continue
        System.out.println("Continue");
        for (int i = 0; i <= 10; i++) {
            if (i == 8) {
                continue;
                //To kończy pętlę i zaczyna nową
            }
            System.out.println("Hello World! " + i);
        }

        System.out.println();
        System.out.println();


    }
}