package podstawy;

import java.sql.SQLOutput;

public class Operatory {

    public static void main(String[] args) {
        Integer x=10;
        Integer y=-2;
        System.out.println("Start:");
        System.out.println(x);
        System.out.println(y);
        x+=100;
        //int a = x-=y;
        System.out.println("Działanie x+=100");
        System.out.println("x " + x);
        System.out.println("y " + y);
        //System.out.println("a " + x);
        x+=y;
        System.out.println("Działanie x+=y");
        System.out.println(x);
        System.out.println(y);
        x-=y;
        System.out.println("Działanie x-=y");
        System.out.println(x);
        System.out.println(y);
        x/=y;
        System.out.println("Działanie x/=y");
        System.out.println(x);
        System.out.println(y);
        x*=y;
        System.out.println("Działanie x*=y");
        System.out.println(x);
        System.out.println(y);
        System.out.println();

        System.out.println("compareTo");
        System.out.println(x.compareTo(y));

        System.out.println("3%2 = " + 3%2);

        //post- i pre-inkrementacja
        int someVariable = 5;
        System.out.println("Post i preinkrementacja: i = " + someVariable);
        System.out.println(someVariable++);
        System.out.println(++someVariable);
        System.out.println(--someVariable);
        System.out.println(someVariable--);
        System.out.println(someVariable);

        //Relacje
        int a=5;
        int b=6;
        System.out.println(a == b);
        System.out.println(a != b);

        System.out.println("Char");
        char d = '7';
        int e = d;
        System.out.println(d);
        System.out.println(e);
        //char na int zostanie zamieniony (na indeks)

        String zmienna="100";
        //Integer liczba=zmienna;
        //System.out.println(liczba);
        //String na int nie przejdzie tak po prostu
        Integer liczba=Integer.valueOf(zmienna);
        System.out.println("Ze stringa do Integera: " + liczba);


     /*
     Operatory logiczne
     && - suma
     // - różnica
     || - alternatywa
     != - czy różne
     == - czy równe
     !  - negacja

      */

        /*Konwersja typów
        Konwersj
         */
    }
}
