package podstawy;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Tablice {
    public static void main(String[] args) {
    /*
    Tablice jako kontener na dane jednego typu. Elementy są indeksowane, indeks od 0. Rozmiar tablicy powinien być stały.
    Są tablice jedno i wielowymiarowe.
    Tablice deklarujemy:
    typ[] nazwa_tablicy = new typ[liczba_elementów]
    String [] myArray = new String[10];
    int[] myArray = new int[10]
    Integer[] Array = new Integer[]
    Można inicjalizować tablicę konkretnymi elementami
    String[] array = new String[]{"Hello", "World",  "!"};
    Jeśli nie podamy danych, będzie uzupełniona wartościami domyślnymi dla danego typu, np 0 dla cyft lub null dla stringów.
     */


        System.out.println("\nPierwsza tablica:");
        int rozTab1 = 4;
        String[] tablica1 = new String[rozTab1];
        tablica1[0] = "W";
        for (int i = 0; i < tablica1.length; i++) {
            System.out.println("Element numer " + i + ": " + tablica1[i]);
        }

        System.out.println("\nKolejna Tablica z podmianką C na Z w polu 3");
        String[] tablica2 = new String[]{"A", "B", "C"};
        tablica2[1] = tablica2[2];
        tablica2[2] = "Z";

        for (String part : tablica2) {
            System.out.println("Kolejny element tablicy: " + part);
        }
/*
        System.out.println("\nZe scannerem?");
        System.out.println("\nPodaj rozmiar tablicy: ");
        Scanner scan = new Scanner(System.in).useLocale(Locale.US); //raz wystarczy przywołać, ale oczekuje polskiego separatora dziesiętnego. jeśli chcemy przyjmował kropki to ".localeUS"
        int rozTab3 = Integer.parseInt(scan.nextLine());     //nextInt nie pobiera entera dlatego trzeba tak udziwnić
        System.out.println("Rozmiar tablicy to " +rozTab3);
        String[] tablica3 = new String[rozTab3];
        for (int i=0 ; i < rozTab3 ; i++) {
            System.out.println("\nWpisz zawartość pola "+ i +":");
            String zawKomorki = scan.nextLine();            //dopiero tutaj pobrałby enter gdyby nie ten parseInt w linii 41 nie wczytując wartości
            tablica3[i] = zawKomorki;
            System.out.println("\nWpisałeś: "+zawKomorki);
        }
        for (int i = 0; i < rozTab3; i++) {
            System.out.println("Element numer "+i+": "+tablica3[i]);
        }
*/


        //Tablice wielowymiarowe
        /*
        Tablice wielowymiarowe to właściwie tablica z tablic
        typ[][] nazwa_tablicy;                                             deklaracja
        nazwa_tablicy = new typ[liczba_wierszy][liczba_kolumn]             inicjalizacja
        typ[][] nazwa_tablicy = new typ[liczba_wierszy][liczba_kolumn]
        String[][] myArray = new String [2][]
        myArray[0] = new String[]{"Ala", "ma", "kota"}
        myArray[1] = new String[]{"Kot", "ma", "Alę"}
        System.out.println(myArray[0][0]);

        przez pętle - 2 pętle potrzebne zagnieżdżone
        for (int i = 0; i < myArray.length; i++) {
            for int j = 0; j-myArray[i].length; j++) {
            System.out.print();
            }
        }
         */
        System.out.println("\nTablice wielowymiarowe:");

        //Przykład
        System.out.println("\nPrzykład z tablicą 2x3:");
        String[][] myArray = new String[2][];
        myArray[0] = new String[]{"Ala", "ma", "kota"};
        myArray[1] = new String[]{"Kot", "ma", "Alę"};
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("\nWiersz numer " + i + ":");
            for (int j = 0; j < myArray[i].length; j++) {
                System.out.print(myArray[i][j] + ", ");
            }
        }

        System.out.print("\n\nPrzykład z fill i int'em:");
        int[][] liczby = new int[4][4];
        for (int[] row : liczby) {
            Arrays.fill(row,10 );
        }
        for (int i = 0; i < liczby.length; i++) {
            System.out.println("\nWiersz numer " + i + ":");
            for (int j = 0; j < liczby[i].length; j++) {
                System.out.print(liczby[i][j] + ", ");
            }
        }

        System.out.print("\n\nPrzykład z fill i Integerem:");
        Integer[][] liczbyInt = new Integer[4][4];
        for (Integer[] row : liczbyInt) {
            Arrays.fill(row,3 );
        }
        for (int i = 0; i < liczbyInt.length; i++) {
            System.out.println("\nWiersz numer " + i + ":");
            for (int j = 0; j < liczbyInt[i].length; j++) {
                System.out.print(liczbyInt[i][j] + ", ");
            }
        }
        System.out.println();

/*

        System.out.println("\nTablice wielowymiarowe Ze scannerem:");
        System.out.println("\nPodaj liczbę wierszy: ");
        Scanner scan = new Scanner(System.in).useLocale(Locale.US);
        int lWier = Integer.parseInt(scan.nextLine());
        System.out.println("Liczba wierszy " +lWier);

        System.out.println("\nPodaj liczbę kolumn: ");
        int lKol = Integer.parseInt(scan.nextLine());
        System.out.println("Liczba kolumn to " +lKol);
        System.out.println("Rozmiar tablicy to " +lKol + " x " +lWier);

        System.out.println("\nCzy chcesz wypełnić tablicę losowymi liczbami: ");
        String check = scan.nextLine();

        if (check.equals("tak")) {
            Integer[][] tablica4Sc = new Integer[lWier][lKol];
            for (int i = 0; i < lWier; i++) {
                for (int j = 0; j < lKol; j++) {
                    tablica4Sc[i][j]= ((int)(Math.random()*100));
                    System.out.print(" " + tablica4Sc[i][j]);

                }
                System.out.println();
            }
        } else {
            System.out.println("Wypełnij tabelę ręcznie");
        }
*/
        System.out.println("Tabliczka mnożenia");
        int[][] tabliczka = new int[10][10];
        for (int i = 1; i<= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                System.out.print(String.format("%5s", i*j));
            }
            System.out.println();
        }



    }
}