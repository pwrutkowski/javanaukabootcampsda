package podstawy;

public class InstrukcjeSterujace {
    public static void main(String[] args) {
        //Temperatura
        float temper = 36.6F;
        if (temper >= 37.0) {
            System.out.println("Masz gorączkę:\nfloat: " +temper +"\ndouble: "+(double)temper);
        }
            else {
                System.out.println("Jest ok");
                System.out.println("Temperatura:\nfloat: " +temper +"\ndouble: "+(double)temper);
                }
        //Tutaj rzutowanie z float na double przekłamuje wyniki, trzeba uważać

        //Zarobki
        System.out.println();
        double zarobek = 2000;
        System.out.println("Zarabiasz "+zarobek+ " PLN");
        if (zarobek >=4000) {
            System.out.println("Płacisz 4% podatku: " + zarobek*0.04 +" PLN");
        }
        else if (zarobek < 2000) {
            System.out.println("Płacisz 0% podatku: 0 PLN");
        }
        else
            System.out.println("Płacisz 2% podatku: " + zarobek*0.02 +" PLN");


        //Switch /default
        System.out.println();
        String dzien = "Poniedzialek";
        switch (dzien) {
            case "poniedzialek":
            case "wtorek":
            case "sroda":
            case "czwartek":
            case "piatek":
                System.out.println("Do roboty, jest "+dzien+"!");
                break;
            case "sobota":
            case "niedziela":
                    System.out.println("Zasłużony odpoczynek");
                    break;
            default:
                System.out.println("Dzień nierozpoznany");
        }
    }

}
