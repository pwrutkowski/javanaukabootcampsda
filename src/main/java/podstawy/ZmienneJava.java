package podstawy;

public class ZmienneJava {
    static int myGlobalVariable = 5;
    static Integer myGlobalVariableInteger=87; //obiekt a nie typ prosty
    //psvm ->public static void main(String{} args)
    //sout-> System.out.println()
    public static void main(String[] args) {
        final int mySecGlobalVariable = 6;
        System.out.println("Zmienna:" +myGlobalVariable);
        System.out.println("Zmienna finalna:" +mySecGlobalVariable);
        System.out.println("Zmieniona zmienna:" +myGlobalVariable);
        innaMetoda();
        float liczbaZmiennoprzecinkowa=9999999999999999999999999999.11111111111111111111F;
        Float liczbaZmiennoprzecinkowaFloat=9999999999999999999999999999.11111111111111111111F;
        double liczbaDouble=12.1;
        System.out.println("Float: " + liczbaZmiennoprzecinkowa);
        System.out.println("Obiekt Float: " + liczbaZmiennoprzecinkowaFloat);
        System.out.println("Double: " + liczbaDouble);
        //byte zmiennaByte=128;
        //short zmiennaShort=32768;
        //myGlobalVariableInteger.

        boolean myTrueValue=true;
        boolean myFalseValue=false;
        boolean myBooleanValue=myFalseValue && myTrueValue;
        System.out.println(myBooleanValue);

        char signValue='y';
        char tabValue='\t';
        System.out.println(signValue + tabValue + "koniec");

        String mojTekst="tutaj jest tekst";
        System.out.println(mojTekst);
    }

    public static void innaMetoda(){
        int innaZmienna=0;
        myGlobalVariable=10;
        System.out.println(innaZmienna);
        System.out.println(myGlobalVariable);
    }
}