package podstawy;

import java.util.Locale;
import java.util.Scanner;

public class WeWyScanner {
    public static void main(String[] args) {
        //Klasa Scanner


        System.out.println("\nWpisz jakiś tekst i naciśnij Enter:");
        Scanner scan = new Scanner(System.in).useLocale(Locale.US); //raz wystarczy przywołać, ale oczekuje polskiego separatora dziesiętnego. jeśli chcemy przyjmował kropki to ".localeUS"
        String textLine = scan.nextLine();
        System.out.println("Twój tekst to: " +textLine);
        System.out.println("Długość tekstu to: " +textLine.length());

        String textLine2 = textLine.replace(" ","");
        System.out.println("Długość tekstu bez spacji to: " +textLine2.length());
        System.out.println("Czyli ilość spacji to: " + (textLine.length()-textLine2.length()));

        int iloscSpacji = 0;
        for(int i = 0; i < textLine.length(); i++) {
            char ch = textLine.charAt(i);
            if (ch == ' ') {
                iloscSpacji++;
            }
        }
        System.out.println("Ilość spacji z pętli to: " + iloscSpacji);
/*
        System.out.println("\nWpisz jakąś liczbę całkowitą i naciśnij Enter:");
        int intValue = scan.nextInt();
        System.out.println("Twoja liczba to: " +intValue);

        System.out.println("\nWpisz jakąś liczbę zmiennoprzecinkową i naciśnij Enter:");
        double doubleValue = scan.nextDouble();
        System.out.println("Twoja liczba to: " +doubleValue);
*/
        //Wyjścia
        //System.out.println();
        //System.out.print();
       // System.out.printf(); printf Formatuje
        /*
         * e – liczba zmiennoprzecinkowa w notacji wykładniczej
         * f – liczba zmiennoprzecinkowa
         * x – liczba całkowita w szesnastkowym systemie liczbowym (hexadycymalnym)
         * o – liczba całkowita w ósemkowym systemie liczbowym (oktalnym)
         * s – łańcuch znaków
         * c – znak
         * b – wartość logiczna
         */
/*
        int x=20;
        float y = 5.5F;
        char c = 'J';
        String str = "Hello Java";
        System.out.printf("\nThe formatted string: " + x + " " +y + " " + c + " " + str);
        System.out.printf("\nThe formatted string: %d %f %c %s", x, y, c, str);
       // System.out.printf("100.0 / 3.0 = %5.2f");
*/
    }
}
