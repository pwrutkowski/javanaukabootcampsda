package podstawy.rudy;

//pakiety
//porządkują kod (np tworzymy sobie zasobnik z narzędziami do obliczeń
//pozwalają logicznie uporządkować projekt
//pozwalają użyć kilku klas o takich samych nazwach ale różnym działaniu/konstrukcji

public class Samochod {
    /*
 Dowolną klasę lub metodę można określić modyfikatorem dostępu jako:
 public
 private - widoczne tylko w ramch klasy
 protected - widoczne tylko w ramach pakietu
 W klasie info.pawel.Samochod zmieniliśmy na private:
   */
    private int rokProdukcji;    //teraz będzie można wywołać rokProdukcji tylko wewnątrz klasy w której się znajdują
    private String rodzajSamochodu;

    Samochod() { //domyślny konstruktor bezargumentowy zniknie kiedy ręcznie zrobimy argumentowy, więc trzeba go dopisać, jeśli chcemy z niego dalej korzystać
    }

    Samochod(int rokProdukcji, String rodzajSamochodu) {
        this.rokProdukcji = rokProdukcji;
        this.rodzajSamochodu = rodzajSamochodu;
    }

    void setRokProdukcji(final int rokProdukcji) {
        this.rokProdukcji = rokProdukcji;
    }

    public void setRodzajSamochodu(String rodzajSamochodu) {
        this.rodzajSamochodu = rodzajSamochodu;
    }

    public int getRokProdukcji() {
        return rokProdukcji;
    }

    public String getRodzajSamochodu() {
        return rodzajSamochodu;
    }

    /*
    Jeśli któregoś gettera zmienilibyśmy na private, to już go w innej klasie nie wywołamy,
    ale może być wywołany na zewnątrz na przykład poprzez metodę toString
     */
    @Override
    public String toString() {
        return "{" +
                "rokProdukcji=" + rokProdukcji +
                ", rodzajSamochodu='" + rodzajSamochodu + '\'' +
                '}';
    }
}
