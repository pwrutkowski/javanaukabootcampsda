package podstawy;

import java.sql.SQLOutput;
import java.util.Locale;

public class Stringi {
    public static void main(String[] args) {
        //Sposoby deklaracji:

        //Literał
        //String myText = "This is a simple text";

        //Konstruktor
        String myText = new String("This is a simple text");
        System.out.println(myText);

        //Niemutowalność
        String text = "This is ";   //<-- Alokacja w pamięci żeby przechowywać tą wartość
        text += "my text.";         //<-- Nie dodaje/podmieni do poprzedniej komórki pamięci, ale tworzy zupełnie nowy wpis i zabija stary
        System.out.println(text);
                                    //Może mieć to znaczenie przy porównywaniu. Nie zawsze porównuje się wartości, ale czasem wpisy pamięci?

        //Porównanie
        System.out.println("\nPorównanie - testy:\nString text1 = \"This is a test\"\nString text2 = \"This is a test\"\nString text3 = new String(\"This is a test\")" );
        String text1 = "This is a test";        // tworzy string w komórce pamięci
        String text2 = "This is a test";        // ponieważ to jest taki sam string jak wcześniej, to nie tworzy nowej komórki pamięci, tylko oba stringi sa trzymane w jednej
        //String text3 = "This is a test";
        String text3 = new String("This is a test");  //wymusiliśmy stworzenie NOWEJ komórki pamięci z identycznym stringiem

        System.out.print("\nNajpierw gołe stringi:");

        System.out.print("\ntext1.equals(text2): ");
        System.out.println(text1.equals(text2));    //porównanie zawartości komórek pamięci
        System.out.print("text1.equals(text3): ");
        System.out.println(text1.equals(text3));
        System.out.print("text1 == text2: ");       //porównanie komórek pamięci a nie zawartości komórek pamięci (a własciwie pytamy czy to ta sama komórka)
        System.out.println(text1 == text2);
        System.out.print("text1 == text3: ");
        System.out.println(text1 == text3);

        System.out.print("\nUżywamy:\nString val1 = text1.intern()\nString val2 = text2.intern()\n");
        String val1 = text1.intern();
        String val2 = text2.intern();

        System.out.print("\nval1.equals(val2): ");
        System.out.println(val1.equals(val2));
        System.out.print("val1.equals(text3): ");
        System.out.println(val1.equals(text3));
        System.out.print("val1 == val2: ");
        System.out.println(val1 == val2);
        System.out.print("val1 == text3: ");
        System.out.println(val1 == text3);

        System.out.println("\nTeraz z String val3 = text3.intern()");
        String val3 = text3.intern();
        System.out.print("val1.equals(val2): ");
        System.out.println(val1.equals(val2));
        System.out.print("val1.equals(val3): ");
        System.out.println(val1.equals(val3));
        System.out.print("val1 == val2: ");
        System.out.println(val1 == val2);
        System.out.print("val1 == val3: ");
        System.out.println(val1 == val3);


        //Konkatenacja
        String textT1 = "My name is ";
        String textT2 = "John Doe.";
        String finalText1 = textT1 + textT2;
        System.out.println("\nPlus: " + finalText1);

        String textT3 = "This is ";
        String textT4 = "a test.";
        String finalText2 = textT3.concat(textT4);
        System.out.println("\nConcat: " + finalText2);

        //Inne nie mniej ważne
        System.out.println("\nLength() test na \"This is test value\":");
        System.out.println("This is test value".length());
        String testValue = "This is test value";
        System.out.println(testValue.toUpperCase() + " (.toUpperCase())");
        System.out.println(testValue.toLowerCase() + " (.toLowerCase())");
        System.out.println("Test indexOf(\"is\") na testValue daje wartość: ");
        System.out.println(testValue.indexOf("is"));

        String haha = "Hahahah! Funny joke!";
        System.out.println(haha + " w tym zamienimy a na o za pomocą replaceAll");
        System.out.println(haha.replaceAll("a","o"));




    }
}
