package podstawy.pawel;

public class Czlowiek {

    short wiek;
    String imie;
    String nazwisko;
    int wzrost;

    //Teraz wprowadzamy sztucznie metodę zwaną konstruktorem (java generuje go sobie automatycznie)
    Czlowiek() { //konstruktor bezargumentowy bo wszystko ustawiamy za pomocą setterów
    }           //domyślny konstruktor bezargumentowy zniknie kiedy ręcznie zrobimy argumentowy, więc trzeba go dopisać, jeśli chcemy z niego dalej korzystać
    //nie może być dwóch konstruktorów pobierających takie same parametry, czymś się muszą różnić
    Czlowiek(String imie, String nazwisko, short wiek, int wzrost) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wzrost = wzrost;
        this.wiek = wiek;
    }

    void setImie(final String imie) {    //final oznacza, że się nie zmieni //tutaj imie to jest parametr metody
        this.imie = imie; //słowo kluczowe this oznacza "to pole klasy"
    }

    void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    void setWzrost(int wzrost) {
        this.wzrost = wzrost;
    }

    void setWiek(short wiek) {
        this.wiek = wiek;
    }

    String getImie() {
        return this.imie;  //dało by radę bez "this" ale lepiej tak dla czytelnosci
    }

    String getNazwisko() {
        return this.nazwisko;
    }

    short getWiek() {
        return this.wiek;
    }

    int getWzrost() {
        return this.wzrost;
    }

    @Override
    public String toString() {
        return "info.pawel.Czlowiek{" +
                "wiek=" + wiek +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", wzrost=" + wzrost +
                '}';
    }
}