package podstawy.pawel;
//pakiety
//porządkują kod (np tworzymy sobie zasobnik z narzędziami do obliczeń
//pozwalają logicznie uporządkować projekt
//pozwalają użyć kilku klas o takich samych nazwach ale różnym działaniu/konstrukcji
//Teraz przywołując konstruktor Samochod, dostaniemy wybór z ktorego pakietu samochod chcemy

public class MainDoKlas {

    /*
    Dowolną klasę lub metodę można określić modyfikatorem dostępu jako:
    public
    private - widoczne tylko w klasie
    protected - widoczne w ramach pakietu

     */

    public static void main(String[] args) {
        Czlowiek czlowiek1 = new Czlowiek();    //to jest konstruktor
        czlowiek1.setImie("Paweł");             //to jest setter (set)
        czlowiek1.setNazwisko("Rutkowski");
        czlowiek1.setWiek((short) 36);
        czlowiek1.setWzrost(177);
        //Powyżej stworzyliśmy OBIEKT "czlowiek1" klasy "info.pawel.Czlowiek"
        //Spróbujmy coś wydostać:
        System.out.println(czlowiek1);

        Czlowiek czlowiek2 = new Czlowiek("Ewa", "Skowron", (short) 33, 169);
        System.out.println(czlowiek2);

        //Można poprzez zmienne pośrednie:
        String czlowiek1Imie = czlowiek1.getImie(); //to jest getter (get)
        System.out.println(czlowiek1Imie);
        String czlowiek1Nazwisko = czlowiek1.getNazwisko();
        System.out.println(czlowiek1Nazwisko);
        short czlowiek1Wiek = czlowiek1.getWiek();
        System.out.println(czlowiek1Wiek);

        //Albo bezpośrednio:
        System.out.printf("\n%s %s ma %s lat i %s cm wzrostu.", czlowiek1.getImie(), czlowiek1.getNazwisko(), czlowiek1.getWiek(), czlowiek1.getWzrost());

        System.out.printf("\n%s %s ma %s lat i %s cm wzrostu.", czlowiek2.getImie(), czlowiek2.getNazwisko(), czlowiek2.getWiek(), czlowiek2.getWzrost());
        System.out.println();

        //Samochód
        Samochod samochod1 = new Samochod();  //to jest konstruktor

        Samochod samochod2 = new Samochod(2015,"sedan");  //to jest konstruktor
        samochod1.setRokProdukcji(2015);
        samochod1.setRodzajSamochodu("sedan");

        int samochod1Rok = samochod1.getRokProdukcji();
        String samochod1Rodzaj = samochod1.getRodzajSamochodu();
        System.out.printf("\nAuto 1 jest z %s roku i jest typu %s.\n", samochod1Rok, samochod1Rodzaj);
        System.out.println("Samochod2" + samochod2.toString());

            /*
    Dowolną klasę lub metodę można określić modyfikatorem dostępu jako:
    public
    private
    protected

    W tej chwili nie musimy używać seterów i getterów:
     */
        Czlowiek czlowiek3 = new Czlowiek();
        czlowiek3.imie="Janek";
        System.out.println(czlowiek3.toString());
    }
    /*
    W klasie info.pawel.Samochod zmieniliśmy na private:
     */
    //  info.pawel.Samochod samochod3 = new info.pawel.Samochod();
    //  samochod3.rokProdukcji=1999;
    //     System.out.println(samochod3.toString());

}

