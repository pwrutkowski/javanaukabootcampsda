package podstawy;

import java.util.HashMap;
import java.util.Scanner;

public class HashMapy {
    public static void main(String[] args) {
        HashMap<Integer, Integer> myHashMap = new HashMap<>();
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < 10; i++) {
            int intUsera = input.nextInt();
            if (!myHashMap.containsKey(intUsera)) { //sprawdzamy czy liczba nie jest już jest kluczem w mapie
                myHashMap.put(intUsera,1);        //jeżeli nie jest to wpisuje w mapę klucz (liczbę) z liczbą wystąpień 1
            } else { //jeżeli w mapie jest już wpis z takim kluczem
                Integer dotyczasowaLiczbaWystapien = myHashMap.get(intUsera); //pobieramy dotychczasową liczbę wystapień
                myHashMap.put(intUsera,dotyczasowaLiczbaWystapien+1); //zwiększamy liczbę wystapień o 1
            }
        }
      //  myHashMap.put(2,1);
       // System.out.println(myHashMap);
      //  myHashMap.put(2,2);
      //  myHashMap.put(999,1);
     //   System.out.println(myHashMap);

        for (Integer key : myHashMap.keySet()) {
            System.out.println(key);
            Integer wartoscDlaKlucza= myHashMap.get(key);
        }
        System.out.println(myHashMap);

    }
}
