package livecode1.fala;

import java.util.Scanner;

public class GwiazdkiInaczej {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int height = 4;
        int patternLenght = 8;          //długość wzoru to jest połówka fali
        System.out.println("Podaj liczbę iteracji:");
        int a = scanner.nextInt();
        int counter = 0 ;

        for(int i=0;i < height;i++){        //dla i od zera do 3 (cztery poziomy)
            counter = 0;                    //licznik =0
            for (int j=0;j<=a;j++){                 //dla j od 0 do ilości iteracji
                if (counter < patternLenght){       //gdy licznik jest krótszy od długości wzoru (połówka fali)
                    if (counter == i || counter == (patternLenght-1) - i) System.out.print("*");    //gdy licznik równy jest i lub licznik jest równy długości wzoru -1
                    else System.out.print("_");
                    counter++;
                } else counter = 0;
            }
            System.out.print("\n");
        }
    }
}
