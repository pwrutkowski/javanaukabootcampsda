package livecode1.fala;

import java.util.Arrays;
import java.util.Scanner;

public class FalaMain {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą większą od zera:");

        int lg = scan.nextInt();
        if (lg <= 0) {
            System.out.println("Błąd, liczba nie jest większa od zera! foch");
            System.exit(1);
        } else {
            System.out.println("Liczba dodatnia, OK.");
        }
        String[][] tabFali = new String[4][lg];
        for (String[] row : tabFali) {
            Arrays.fill(row, " ");
        }

        int j = 0;
        boolean wDol = true;

        for (int i = 0; i < lg; i++) {
            tabFali[j][i] = "*";
            if (j == 0 && wDol) {
                j++;
            } else if (j != 3 && wDol) {
                j++;
            } else if (j == 3 && wDol) {
                wDol = false;
            } else if (j == 3 && !wDol) {
                j--;
            } else if (j != 0 && !wDol) {
                j--;
            } else if (j == 0 && !wDol) {
                wDol = true;
            }
        }

        for (int k = 0; k < tabFali.length; k++) {
            System.out.println();
            for (int l = 0; l < tabFali[k].length; l++) {
                System.out.print(tabFali[k][l]);
            }
        }
    }
}