package livecode1.dwukrotnie;

import java.util.*;

/*
public static boolean Contains (final int[] array, final int v) {

        boolean result = false;
        for(int i : array){
        if(i == v){
        result = true;
        break;
        }
        }
        return result;
        }
 */

public class Dwukrotnie {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Wpisz 10 liczb całkowitych i oddziel je spacją: ");
        String wczytaneLiczby = scan.nextLine();
        String[] liczby = wczytaneLiczby.split("\\s+");
        List<String> lista = Arrays.asList(liczby);
/*
        Set<String> printed = new HashSet<>();
        for (String liczba : lista) {
            if (printed.add(liczba)) {// Set.add() also tells if the element was in the Set!
                if (Collections.frequency(lista, liczba) != 1) {
                    System.out.println("Liczba: " + liczba + ", występuje : " + Collections.frequency(lista, liczba) + " razy");
                }
            }
        }
*/
        Integer[] tablica = new Integer[10];
        Set<Integer> duplicat = new HashSet<>();
        System.out.println("Podaj liczby");
        for (int i = 0; i < 10; i++) {
            tablica[i] = Integer.parseInt(scan.nextLine());
        }

        for (int i = 0; i < 10; i++) {
            for (int j = i + 1; j < 10; j++) {
                if (tablica[i].equals(tablica[j])) {
                    duplicat.add(tablica[i]);
                }
            }
        }
        System.out.println(duplicat);
    }
}
