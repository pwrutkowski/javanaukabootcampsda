package livecode1.zaduzozamalo;

import java.util.Scanner;

public class ZaDuzoZaMalo {
    public static void main(String[] args) {
        Scanner scaner = new Scanner(System.in);
        int gornaGranica = 10;
        int liczbaUzytkownika;
        int losowa;
        do {
            losowa = ((int)(Math.random()*gornaGranica));
            System.out.println("Podaj liczbę z zakresu od 0 do " + (gornaGranica-1) + ": ");
            liczbaUzytkownika = Integer.parseInt(scaner.nextLine());
            if (liczbaUzytkownika > losowa) {
                System.out.println("Za dużo!");
            } else if (liczbaUzytkownika < losowa) {
                System.out.println("Za mało!");
            } else {
                System.out.println("Dobrze!");
            }
            System.out.println("Moją liczbą było " + losowa);

        } while (liczbaUzytkownika != losowa);

        System.out.println("Bingo!");
    }
}
