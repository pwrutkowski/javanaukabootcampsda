package livecode1.bmi;

import java.util.Scanner;

public class BMIMain {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj wagę w [kg]:");
        int masa = scan.nextInt();

        System.out.println("Podaj wzrost w [cm]:");
        int wzrost = scan.nextInt();

        double bmi = BMI.getBMI(masa, wzrost);

        System.out.println("Wynik:");

        BMI.getResult(bmi);
    }
}


