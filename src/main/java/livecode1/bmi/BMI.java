package livecode1.bmi;

public class BMI {
    public static double getBMI(int masa, int wzrost) {
        double bmi = masa/Math.pow((wzrost/100.0), 2);
        return bmi;
    }

    public static void getResult(double bmi) {
        if (bmi < 18.5) {
            System.out.println("Masz niedowagę.");
        } else if (bmi > 24.9) {
            System.out.println("Masz nadwagę.");
        } else {
            System.out.println("Jest super, tak trzymaj!");
        }
    }

}
