package livecode1.sumacyfr;

import java.util.Scanner;

public class SumaCyfrLiczby {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą większą od zera:");
        int numberIn = scan.nextInt();
        if (numberIn <= 0) {
            System.out.println("Błąd, liczba nie jest większa od zera! foch");
            System.exit(1);
        } else {
            System.out.println("Liczba dodatnia, OK.");
        }
        int number = numberIn;
        int suma = 0;
        do {
            suma += number % 10;
            number = number / 10;
        } while (number != 0);
        System.out.println("Suma cyfr liczby " + numberIn + " wynosi " + suma);
    }
}
