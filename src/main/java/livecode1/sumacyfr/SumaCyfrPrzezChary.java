package livecode1.sumacyfr;

import java.util.Scanner;

public class SumaCyfrPrzezChary {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą większą od zera:");
        Integer numberInTC = scan.nextInt();
        if (numberInTC <= 0) {
            System.out.println("Błąd, liczba nie jest większa od zera!");
            System.exit(1);
        } else {
            System.out.println("Liczba dodatnia, OK.");
        }
        String stringIn = numberInTC.toString();
        Integer suma = 0;
        for (char i : stringIn.toCharArray()) {
            suma += Character.getNumericValue(i);
        }
        System.out.println("Suma cyfr liczby " + numberInTC + " wynosi " + suma);
    }
}
