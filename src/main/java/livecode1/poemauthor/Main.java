package livecode1.poemauthor;

public class Main {
    public static void main(String[] args) {


        Author autor1 = new Author("Psikuta", "Polska");
        Author autor2 = new Author("Shakespeare", "England");
        Author autor3 = new Author("Alighieri", "Italia");

        Poem poem0 = new Poem(autor1, 49);
        Poem poem1 = new Poem(autor2, 144);
        Poem poem2 = new Poem(autor3, 81);

        Poem[] poemTab = new Poem[3];
        poemTab[0] =  poem0;
        poemTab[1] =  poem1;
        poemTab[2] =  poem2;

        int indeksPoem = 0;
        int najwieksza=0;

        for (int i = 0; i < poemTab.length; i++) {
            if (poemTab[i].getStropheNumbers() > najwieksza) {
                najwieksza = poemTab[i].getStropheNumbers();
                indeksPoem = i;
            }
        }
        System.out.println("Największa liczba wierszy to: " + najwieksza + ".\nWystępuje ona dla autora:\n" +poemTab[indeksPoem].getCreator().getSurname() + ".\nKraj pochodzenia: " + poemTab[indeksPoem].getCreator().getNationality());

    }
}
