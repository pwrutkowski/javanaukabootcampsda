package livecode1.poemauthor;

public class Poem {
    private Integer stropheNumbers;
    Author creator;

    Poem(Author creator, Integer stropheNumbers) {
        this.creator = creator;
        this.stropheNumbers = stropheNumbers;
    }

    public void setStropheNumbers(Integer stropheNumbers) {
        this.stropheNumbers = stropheNumbers;
    }

    public void setCreator(Author creator) {
        this.creator = creator;
    }

    public Integer getStropheNumbers() {
        return stropheNumbers;
    }

    public Author getCreator() {
        return creator;
    }

    @Override
    public String toString() {
        return "Poem{" +
                "stropheNumbers=" + stropheNumbers +
                ", creator=" + creator +
                '}';
    }
}

