package livecode1.aaapsik;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AaaPsik {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Wpisz tekst, tylko nie kichaj");
        String wczytanyTekst = scan.nextLine();

        Pattern szukanyWzor = Pattern.compile(".*a+\\s?psik.*", Pattern.CASE_INSENSITIVE);
        Matcher przeszukanieStringa = szukanyWzor.matcher(wczytanyTekst);
        boolean matchFound2 = przeszukanieStringa.matches();  //czy nasze wyrażenie pokrywa całe przeszukiwane zdanie
        if(matchFound2) {
            System.out.println("Na zdrowie, przegrałeś");
        } else {
            System.out.println("Brawo");
        }

        // String[] slowa = wczytanyTekst.split("\\s+");
    }
}
