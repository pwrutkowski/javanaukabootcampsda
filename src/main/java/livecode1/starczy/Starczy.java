package livecode1.starczy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Starczy {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<String> przykladowaLista = new ArrayList<>();
        String zawKomorki;
        do {
            System.out.println("\nWpisz tekst: ");
            zawKomorki = scan.nextLine();
            if (!zawKomorki.equalsIgnoreCase("starczy")) {
                przykladowaLista.add(zawKomorki);
            }
        } while (!zawKomorki.equalsIgnoreCase("starczy"));

        System.out.println("Lista na koniec wygląda tak:\n"+przykladowaLista);
        if (przykladowaLista.size() == 0) {
            System.out.println("Lista jest pusta, nie podano żadnego tekstu.");
        } else {
            System.out.println("Najdłuższy tekst to: " + Collections.max(przykladowaLista));
        }

    }
}
