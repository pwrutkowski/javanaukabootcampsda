package livecode1.fibonacci;

import java.util.Scanner;

public class FibonacciMain {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą większą od zera:");
        int lf = scan.nextInt();
        if (lf <= 0) {
            System.out.println("Błąd, liczba nie jest większa od zera! foch");
            System.exit(1);
        } else {
            System.out.println("Liczba dodatnia, OK.");
        }
        int[] libFib = new int[lf];
        libFib[0] = 0;
        libFib[1] = 1;
        for (int i = 2; i < lf; i++) {
            libFib[i] = libFib[i - 2] + libFib[i - 1];
        }
        System.out.println(lf + " z kolei liczba Fibonacciego to " + libFib[lf-1]);
    }
}
