package livecode1.iledni;

import java.time.Duration;
import java.time.LocalDate;
import java.util.Scanner;

public class IleDniInaczej {
    public static void main(String[] args) {
        LocalDate dataObecna = LocalDate.now();
        System.out.println("Dzisiaj jest: " + dataObecna);
        Scanner scann = new Scanner(System.in);
        System.out.println("Podaj datę kolejnych zajęć: ");

        String dataKolejnychZajec= scann.nextLine();;
        LocalDate dataKolejnychZajecDate = LocalDate.parse(dataKolejnychZajec);
        Duration difference = Duration.between(dataObecna.atStartOfDay(), dataKolejnychZajecDate.atStartOfDay());
        System.out.println("Dni pozostałe do dnia zajęć to: "+ difference.toDays());
    }
}

/*  Rozwiązanie Przemka
Scanner scan = new Scanner(System.in);
System.out.println("Podaj date kursu w formacie ddmmyyyy");
String dzienKursu = scan.nextLine();
DateTimeFormatter format = DateTimeFormatter.ofPattern("ddMMyyyy");
LocalDate dataKursu = LocalDate.parse(dzienKursu, format);
System.out.println(dataKursu);
System.out.println(dataKursu.toEpochDay()-LocalDate.now().toEpochDay());
 */