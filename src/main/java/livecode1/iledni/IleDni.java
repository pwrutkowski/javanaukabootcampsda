package livecode1.iledni;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Scanner;

public class IleDni {
    public static void main(String[] args) throws Exception {
        final long poczatek = System.currentTimeMillis();

        final String OLD_FORMAT = "dd.MM.yyyy";     //wpisywany format daty
        final String NEW_FORMAT = "DD";             //DD - dzień roku (nowy format daty)

        Scanner input = new Scanner(System.in);
        System.out.println("Podaj datę następnych zajęć w formacie " + OLD_FORMAT + ": ");
        String stringDatyIn = input.nextLine();                               //Wczytanie stringa daty

        SimpleDateFormat date1 = new SimpleDateFormat(OLD_FORMAT);   //.parse(stringDatyIn)  //nowa data jako string klasy SimpleDateFormat

        Date dataSDA = date1.parse(stringDatyIn);       //nowa data jako data klasy Date
        date1.applyPattern(NEW_FORMAT);         //zastosowanie nowego wzoru daty - czyli na który dzień roku

        String sdaDateString = date1.format(dataSDA);     //zamiana daty na stringa
        Integer sdaDateInt = Integer.parseInt(sdaDateString);  //zamiana stringa na Integera
        System.out.println("To jest " + sdaDateInt + " dzień roku.");

        final int nowDayOfYear = LocalDate.now().getDayOfYear();
        System.out.println(nowDayOfYear);


        System.out.println("Do najbliższych zajęć z SDA zostało Ci " + (sdaDateInt - nowDayOfYear) + " dni.");

        final long koniec = System.currentTimeMillis();     //eksperyment
        System.out.println("Trwało to " + (koniec - poczatek) + " milisekund.");
    }
}