package livecode1.liczbypierwsze;

import java.util.Scanner;

public class LiczbyPierwsze {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą większą od zera:");
        int lp = scan.nextInt();
        if (lp <= 0) {
            System.out.println("Błąd, liczba nie jest większa od zera! foch");
            System.exit(1);
        } else {
            System.out.println("Liczba dodatnia, OK.");
        }

        for (int i=2; i <= lp; i++) {
            boolean check = false;
            if (i == 2) {
                System.out.println(i);
            }
            else {

                for (int j = (i/2)+1; j > 1; j--) {
                    if (i % j == 0) {
                        check = true;
                    }
                    if (check) {
                        break;

                    } else if (!check && j==2) {
                        System.out.println(i);
                        continue;
                    }
                }
            }
        }
    }
}
   /*
        for (int i = lp-1; i > 1; i--) {
            if (lp%i ==0 ) {
                System.out.println("Liczba ma conajmniej jeden inny dzielnik niż tylko 1 i " + lp + ", czyli nie jest pierwsza. Ten dzielnik to " + i);
                //System.exit(1);
            } else {
               continue;
            }
        }
       // System.out.println("Liczba jest pierwsza");

    */