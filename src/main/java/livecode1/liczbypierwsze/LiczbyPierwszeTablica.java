package livecode1.liczbypierwsze;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LiczbyPierwszeTablica {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe ");
        int liczbaUzytkownika = Integer.valueOf(scan.nextLine());   //sczytanie liczby

        List<Integer> listaLiczbpierwszych = new ArrayList();       //nowa lista
        listaLiczbpierwszych.add(2);                                //dodanie liczby 2 do listy

        for (int liczba = 3; liczba <= liczbaUzytkownika; liczba++) { //dodawanie kolejnych liczb sprawdzonych jako pierwsza
            if (isPierwsza(liczba,listaLiczbpierwszych)){
                listaLiczbpierwszych.add(liczba);
            }
        }
        System.out.println(listaLiczbpierwszych);       //wyrzucenie listy na ekran
    }
    public static boolean isPierwsza(Integer liczba, List<Integer> listaLiczbPierwszych) {
        boolean jestPierwsza = true;
        for (Integer liczbaPierwsza:listaLiczbPierwszych){          //jako bazę dzielników wykorzystujemy liczby pierwsze zapisane już w tablicę
            if(liczba%liczbaPierwsza==0){                           //czy nasza liczba jest podzielna przez liczby pierwsze z listy
                jestPierwsza=false;
            }
        }
        return jestPierwsza;
    }
}