package livecode1.jakala;

import java.util.Scanner;

public class Jakala {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Wpisz tekst, tylko nie kichaj");
        String wczytanyTekst = scan.nextLine();
        String[] slowa = wczytanyTekst.split("\\s+");

    /*
        for (String slowo : slowa) {
            System.out.print(slowo + " " + slowo + " ");
        }

    */
        for (String pojSlowo : slowa) {
            pojSlowo+=" ";
            System.out.print(pojSlowo.repeat(2) );
        }
    }
}
