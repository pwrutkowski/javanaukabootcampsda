package livecode1.znakiASCII;

import java.util.Scanner;

public class ZnakiASCII {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Wpisz pierwszy znak: ");
        char znak1 = scan.next().charAt(0);
        System.out.println("Wpisz drugi znak: ");
        char znak2 = scan.next().charAt(0);

        int asc1 = znak1;
        int asc2 = znak2;

        System.out.println("Odległość między znakami w alfabecie (tablicy ASCII) to " + (Math.abs(asc1-asc2)-1) + " znak(ów).");
    }
}
