package livecode1.procentspacji;

import java.util.Scanner;

public class ProcentSpacji {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Wpisz tekst: ");
        String wczytanyTekst = scan.nextLine();
        int ilSpac = (100*(wczytanyTekst.length() - (wczytanyTekst.replaceAll(" ","")).length()))/(wczytanyTekst.length());
        System.out.println("Spacje stanowią " + ilSpac +"% wszystkich znaków");
    }
}
