package livecode1.szereg;

import java.util.Scanner;

public class SzeregMain {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą większą od zera:");
        int sz = scan.nextInt();
        if (sz <= 0) {
            System.out.println("Błąd, liczba nie jest większa od zera! foch");
            System.exit(1);
        } else {
            System.out.println("Liczba dodatnia, OK.");
        }

        double szer =0;
        for (int i = 1; i <= sz; i++) {
            szer +=(1.0/i);
        }

        System.out.println("Wprowadziłeś liczbę "+sz+". Szereg 1+1/2+1/3+...+1/n ma sumę: " + szer);
    }
}
