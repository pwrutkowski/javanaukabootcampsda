package livecode1.rownaniekwadratowe;

import java.util.Scanner;

public class RownKwaMain {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Obliczymy pierwiastki równania kwadratowego typu:\n\na x^2 + bx + c = 0");
        System.out.println("Podaj współczynnik a (liczbę całkowitą):");
        int aKwa = scan.nextInt();

        System.out.println("Podaj współczynnik b (liczbę całkowitą):");
        int bKwa = scan.nextInt();

        System.out.println("Podaj współczynnik c (liczbę całkowitą):");
        int cKwa = scan.nextInt();

        double delta = RownKwa.getDelta(aKwa, bKwa, cKwa);
        String decisionDelta = RownKwa.getDeltaResult(delta);

        switch (decisionDelta) {
            case "DwaP":
                double x1 = RownKwa.getX1(aKwa, bKwa, delta);
                double x2 = RownKwa.getX2(aKwa, bKwa, delta);
                System.out.println("Delta większa od zera");
                System.out.println("Wynikiem są dwa pierwiastki:\nx1 = " + x1 + " oraz x2 = " + x2);
                break;
            case "JedenP":
                double xx = RownKwa.getXX(aKwa, bKwa);
                System.out.println("Delta równa zero");
                System.out.println("Wynikiem jest jeden pierwiastek podwójny:\nx = " + xx);
                break;
            case "ZeroP":
                System.out.println("Delta ujemna");
                //System.exit(1);
                break;
            default:
                System.out.println("Coś nie poszło.");
                //System.exit(1);
                break;

        }
    }
}
