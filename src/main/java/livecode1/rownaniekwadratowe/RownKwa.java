package livecode1.rownaniekwadratowe;

public class RownKwa {

    public static double getDelta(int aKwa, int bKwa, int cKwa) {
        double delta =  Math.pow((double) bKwa, 2)- (4 * aKwa * cKwa);
        return delta;
    }

    public static double getX1(int a, int b, double delta) {
        double x1 = (-b + Math.sqrt(delta))/(2*a);
        return x1;
    }

    public static double getX2(int a, int b, double delta) {
        double x2 = (((-b)-(Math.sqrt((double)delta)))/(2*a));
        return x2;
    }

    public static double getXX(int a, int b) {
        double xx = (-b)/(2*a);
        return xx;
    }

    public static String getDeltaResult(double delta) {
        String decision;
        if (delta > 0) {
            decision = "DwaP";
        } else if (delta < 0) {
            decision = "ZeroP";
        } else {
            decision = "JedenP";
        }
        return decision;
    }



}
