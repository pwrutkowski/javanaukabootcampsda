package livecode1.pifpaf;

import java.util.Scanner;

public class PifPafMain {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą większą od zera:");
        int pp = scan.nextInt();
        if (pp <= 0) {
            System.out.println("Błąd, liczba nie jest większa od zera! foch");
            System.exit(1);
        } else {
            System.out.println("Liczba dodatnia, OK.");
        }
        for (int i = 1; i <= pp; i++) {
            if (i%3 ==0 && i%7!=0) {
                System.out.println("Pif");
            } else if (i%7 == 0 && i%3!=0) {
                System.out.println("Paf");
            } else if (i%7==0 && i%3==0) {
                System.out.println("Pif Paf!");
            } else {
                System.out.println(i);
            }
        }

    }
}
