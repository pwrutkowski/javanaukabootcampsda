package livecode1.rosnacyciag;

import java.util.Scanner;

public class CiagRosnacy {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        Integer[] tablica = new Integer[10];
        System.out.println("Podaj liczby");
        for (int i = 0; i < 10; i++) {
            tablica[i] = Integer.parseInt(scan.nextLine());
        }
        int dlugoscObecnego = 1;                        // bo dwa kolejne wpisy dają liczbę dwa
        int najdluzszyZnaleziony = 1;

        for (int i = 1; i < 10; i++) {                  //sprawdzamy 10 kolejnych elementów
            if (tablica[i-1] < tablica[i]) {            //jeśli i-ty element jest większy niż poprzedni i-1
                dlugoscObecnego++;                      //to counter zwiększamy o jeden
                if (dlugoscObecnego > najdluzszyZnaleziony) {       //jeśli obecny staje się dłuższy od najdłuższego
                    najdluzszyZnaleziony = dlugoscObecnego;         //to przypisujemy najdłuższemu wartość obecnego
                }
                System.out.println("if " + najdluzszyZnaleziony + " " + dlugoscObecnego); //to tylko kontrola działania
                continue;
            } else {
                dlugoscObecnego = 1;        //jeśli kolejny element tablicy nie jest większy niż poprzedni to obecny ustawiamy na 1
                System.out.println("el " + najdluzszyZnaleziony + " " + dlugoscObecnego); //to tylko kontrola działania
            }
        }

        if (najdluzszyZnaleziony >= 2) { //to tylko warunek na wypisywanie wyniku
            System.out.println("Najdłuższy rosnący ciąg liczb zawiera " + najdluzszyZnaleziony + " elementów");
        } else {
            System.out.println("Brak rosnącego ciągu elementów.");
        }
    }
}
