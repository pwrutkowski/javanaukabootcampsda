package livecode1.rosnacyciag;

import java.util.Scanner;

public class RosnacyBezTablic {
    public static void main(String[] args) {
        Scanner scaner = new Scanner(System.in);
        System.out.println("Podaj 10 liczb: ");
        int licznikElementowWCiagu = 0;
        int dlugoscNajdluzszegoCiagu = 0;
        int ostatniaLiczbaZCiagu = scaner.nextInt();
        for (int i = 0; i < 9; i++) {
            int liczbaUzytkownika = scaner.nextInt();
            if (liczbaUzytkownika > ostatniaLiczbaZCiagu) {
                licznikElementowWCiagu += 1;
            } else {
                licznikElementowWCiagu = 0;
            }
            if (licznikElementowWCiagu > dlugoscNajdluzszegoCiagu) {
                dlugoscNajdluzszegoCiagu = licznikElementowWCiagu;
                ostatniaLiczbaZCiagu = liczbaUzytkownika;
            }
        }
        System.out.println(dlugoscNajdluzszegoCiagu);
    }
}