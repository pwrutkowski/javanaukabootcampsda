package zaawansowana.vehicle;

public class App {
    public static void main(String[] args) {
        Bicycle bicycle = new Bicycle("Kross");
        System.out.println(bicycle);
        //  bicycle.printManufacturer();

        SUV suv = new SUV("Chevy", "CHVZZZ1D1D3432423424", true);
       suv.security();
        System.out.println(suv.toString());

        //przypisałem do referencji klasy bazowej CAR obiekt klasy pochodnej
        Car iii = new SUV("Buick", "BZBASDDA234322342423", false);
        //mam dostęp do metod zgodnie z typem, ale uruchamiam metodę z klasy rzeczywiście przekazanej
        //Polimorfizm
        //ale tak się nie uda: SUV suuuv = new Car();

        iii.security();

        System.out.println("-------");
        Car kia = new Car("KIA", "KKAASDASDASD234324234234");
        Car hyundai = new SUV("Hyundai", "HYYSADASDDA23423423", true);
        Car[] cars = {kia, hyundai};
        for (Car eachCar : cars) {
            eachCar.security();
            System.out.println("-------");
        }

    }
}
