package zaawansowana.vehicle;

public class Car extends Vehicle {
//    public final class Car extends Vehicle {
    //można wstawić final - i wtedy nie pozwolimy zmienić klasy bazowej
    private String vin;

    public Car(String manufacturer, String vin) {
        super(manufacturer);
        this.vin = vin;
    }

    public void drive(int distance) {
            System.out.println("Przejedziesz: " + distance);
    }

    public void security() {
        System.out.println("Sprawdź Alarm!");
    }
    // dając final możemy też nie pozwolić na zmianę konkretnej metody
    public String getVin() {
        return vin;
    }

}
