package zaawansowana.vehicle;

public class SUV extends Car{
    private boolean is4WD;

    public SUV(String manufacturer, String vin, boolean is4WD) {
        //nie możemy tu zrobić validacji argumentu
        super(manufacturer, vin);  //super musi być pierwszy w konstruktorze
        this.is4WD = is4WD;
    }

    public boolean isIs4WD() {
        return is4WD;
    }

    @Override  //dajemy znać kompilatorowi, że moją intencją jest nadpisanie jakiejś innej metody, a ten sprawdza, czy ona istnieje jeszcze
                // żebyśmy dali znać w kodzie że odwołujemy się do czegoś dziedziczonego
                //To się nazywa Przesłanianie metod
    public void security() {
        //mamy 2 drogi: rozszerzyć to co jest używając super i dodając swoją logikę
        //lub bez super zmienić logikę działania metody
        super.security();
        System.out.println("SUV - przed wyłączeniem przełącz reduktor na HIGH");
    }

    @Override
    public String toString() {
        return "SUV{" +
                "is4WD=" + is4WD +
                '}' +super.toString();
    }
}
