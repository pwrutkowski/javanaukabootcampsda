package zaawansowana.pomidor;

public interface Ipomidor {
    public String pomidor(String stringIn);
    public String pomidorTablicaZnakow(String stringIn);
}
