package zaawansowana.pomidor;

public class Pomidor implements Ipomidor{


    @Override
    public String pomidor(String stringIn) {
        char firstChar = stringIn.charAt(0);
        if (Character.isLowerCase(firstChar)) {
            return "Mała litera";
        } else if(Character.isUpperCase(firstChar)) {
            return "Wielka litera";
        } else if (Character.isDigit(firstChar)) {
            return "Cyfra";
        } else {
            return "Pomidor!";
        }
    }

    @Override
    public String pomidorTablicaZnakow(String stringIn) {
        char firstChar = stringIn.charAt(0);
        if ((int) firstChar <= 122 && (int) firstChar >=97) {
            return "Mała litera";
        } else if((int) firstChar <= 90 && (int) firstChar >=65) {
            return "Wielka litera";
        } else if ((int) firstChar <= 57 && (int) firstChar >=48) {
            return "Cyfra";
        } else {
            return "Pomidor!";
        }
    }
}
