package zaawansowana.pomidor;

import java.util.Scanner;
/*
Napisz metode, ktora przyjmuje jako argument typ String i zwraca obiekt String.
W zaleznosci od tego, jaka jest pierwsza litera przekazanego Stringa metoda zwraca:
jesli przekazany obiekt zaczyna sie na mala litere zwraca "mala lietera"
jesli przekazany obiekt zaczyna sie na duza litere zwraca "duza lietera"
jesli przekazany obiekt zaczyna sie na cyfre zwraca "cyfra"
w przeciwnym wypadku zwraca zwraca "pomidor"
 */
public class PomidorApp {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj słowo:");
        String stringIn = scan.nextLine();

        Ipomidor pommm = new Pomidor();
        System.out.println(pommm.pomidor(stringIn));

        System.out.println(pommm.pomidorTablicaZnakow(stringIn));


    }
}
