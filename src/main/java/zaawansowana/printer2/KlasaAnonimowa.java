package zaawansowana.printer2;

public class KlasaAnonimowa {

    public static void main(String[] args) {
        PrinterService ps = new PrinterService();

        IPrinter printer = new IPrinter() {  //klasa anonimowa, nie ma zmiennych, tylko coś robi
            @Override
            public void print(String wordToPrint) {
                System.out.println(wordToPrint);
            }
        };

        ps.print(printer, "tekst Do druku");
    }
        //ponizej też klasa anonimowa, może implementować interfejs z wiecej niz jedna metodą, tutaj lambdy się nie da
    ITV tv = new ITV() {
        @Override
        public boolean isOn() {
            return false;
        }

        @Override
        public void nextChannel() {

        }
    };
}
