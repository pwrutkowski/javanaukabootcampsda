package zaawansowana.printer2;

public class PrinterApp {
    public static void main(String[] args) {
        PrinterService printerService = new PrinterService();
        IPrinter p = new PrinterToUpperCase();
        printerService.print(p, "Tekst do wydrukowania");


        IPrinter pp = new IPrinter() {  //klasa anonimowa  //implementacja interfejsu za pomocą klasy anonimowej
            @Override                   //w tej chwili używamy lambdy w takim przypadku
            public void print(String wordToPrint) {
                System.out.println(wordToPrint.toLowerCase());
            }
        };

        printerService.print(pp, "TO JEST za DUZO SPACJI");

//        Wyrażenie lambda składa się z trzech części:
//
//        listy argumentów, która:
//        musi znaleźć się w nawiasie, jeżeli ilość argumentów jest różna od 1
//        może, ale nie musi, podawać typy argumentów
//        operatora ->, który jest używany po liście argumentów
//        ciała implementowanej metody, które znajduje się za operatorem ->
//        jeżeli ciało składa się z jednego wyrażenia to:
//        ciało to nie musi być wewnątrz klamer, tzn. { i }
//        w przypadku, gdy to wyrażenie zwraca pewien obiekt, możemy pominąć słowo kluczowe return
//                jeżeli ciało składa się z wielu wyrażeń, to:
//        ciało to musi znaleźć się wewnątrz klamer, tzn. { i }.

        IPrinter ppp = text -> System.out.println(text.toLowerCase());
        printerService.print(ppp, "AAA");

        printerService.print(text -> System.out.println(text.toUpperCase()), "hahaha");

        //        liczba arg (text)    operator (->)       implementacja ( System.out.println(text.toUpperCase()) )   coś jak ciało metody


        printerService.print(o -> System.out.println(o.toUpperCase()), "hahaha");
    }
}
