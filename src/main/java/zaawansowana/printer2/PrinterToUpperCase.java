package zaawansowana.printer2;

public class PrinterToUpperCase implements IPrinter {
    @Override
    public void print(String wordToPrint) {
        System.out.println(wordToPrint.toUpperCase());
    }
}
