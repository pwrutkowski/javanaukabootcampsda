package zaawansowana.printer2;
@FunctionalInterface // takie coś można wstawić, wtedy informujemy innych że to jest funkcyjny ale też kompilator pilnuje, czy pozostaje funkcyjny (nie da się dopisać drugiej metody)
public interface IPrinter {

    //interfejs funkcyjny, jest to interfejs z jedną metodą
    //interfejs jest potrzebny żeby w ogole skorzystać z wyrazenia lambda

    void print(String wordToPrint);

}
