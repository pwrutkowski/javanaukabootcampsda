package zaawansowana.enuM.samochody;

public class CarApp {
    public static void main(String[] args) {

        Car bmw = Car.BMW;
        System.out.println(bmw.isPremium());
        System.out.println(bmw.isRegular());
        System.out.println(bmw.compareTo(Car.TOYOTA));
        System.out.println(bmw.isFaster(Car.FERRARI));

    }
}
