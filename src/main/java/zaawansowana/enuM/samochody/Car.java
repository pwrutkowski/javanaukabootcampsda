package zaawansowana.enuM.samochody;

/*
Stwórz klasę enum Car ze stałymi FERRARI, PORSCHE, MERCEDES, BMW, OPEL, FIAT, TOYOTA, itp. Każdy z pojazdów ma własne parametry np. cena, moc, itp.
Enum powinien zawierać metody boolean isPremium() oraz boolean isRegular().
Metoda isPremium() powinna zwracać rezultat przeciwny od rezultatu wywołania metody isRegular()
Dodatkowo w ramach klasy enum powinna być zadeklarowana i zaimplementowana metoda boolean isFasterThan().
Metoda ta powinna przyjmować obiekt typu Car oraz wyświetlać informacje o tym, że wskazany pojazd jest szybszy bądź nie od pojazdu przekazanego w argumencie. W tym celu skorzystaj z metody compareTo()
 */
public enum Car {

    FERRARI(2000000, 500, true), PORSCHE(1000000, 400, true),
    MERCEDES(500000, 250, true), BMW(500000, 250, false),
    TOYOTA(150000, 200, false), OPEL(100000, 150, false), FIAT(100000, 110, false);

    private int cena;
    private int mocHP;
    private boolean isPremiumCar;

    Car(int cena, int mocHP, boolean isPremiumCar) {
        this.cena = cena;
        this.mocHP = mocHP;
        this.isPremiumCar = isPremiumCar;
    }

    public boolean isPremium() {
        return isPremiumCar;
    }

    public boolean isRegular() {
        return !isPremium();
    }

    public boolean isFaster(Car o) {
        return this.mocHP>o.mocHP;
    }
}
