package zaawansowana.enuM.transport;

public enum Transport {

    CAR(100,77), TIR(80, 77), BIKE(30, 22), PLANE(800, 88);


    private int speed;
    private int code;

    Transport(int speed, int code) {
        this.speed = speed;
        this.code = code;
    }

    public int getSpeed() {
        return speed;
    }

    public int getCode() {
        return code;
    }

    public boolean isTheSameCode(Transport o) {
        return getCode() == o.getCode();
    }
}
