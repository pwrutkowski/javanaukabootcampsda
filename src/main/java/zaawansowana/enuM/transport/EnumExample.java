package zaawansowana.enuM.transport;

public class EnumExample {

    public static void main(String[] args) {

        Transport enum1 = Transport.BIKE;
        System.out.println(enum1.getCode());
        System.out.println(enum1.getSpeed());
        System.out.println(enum1.isTheSameCode(Transport.CAR));
        System.out.println(Transport.TIR.isTheSameCode(Transport.CAR));

        System.out.println(Transport.TIR.compareTo(Transport.TIR));
        //compare przy enum bazuje na kolejności
        System.out.println(Transport.BIKE.compareTo(Transport.TIR));

    }
}
