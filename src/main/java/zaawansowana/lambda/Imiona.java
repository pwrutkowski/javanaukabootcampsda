package zaawansowana.lambda;

//        Napisz program, który najpier poprosi o 5 imion na standardowym wejściu.
//        Wyświetli je i zapyta, które skasować, po czym wyświetli listę po skasowaniu,
//        posortuje ją i wyświetli ponownie i po skasowaniu i zakończy działanie.

import java.util.*;

public class Imiona {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Wpisz pięć imion oddzielonych spacją");


        String wczytanyTekst = scan.nextLine();
        String[] imiona = wczytanyTekst.split("\\s+");
        List<String> lista = new ArrayList<>(Arrays.asList(imiona));

        System.out.println(lista);

        System.out.println("Które skasować?");
        String imieDoSkaowania = scan.nextLine();

        lista.removeIf(name -> name.equals(imieDoSkaowania));

        Collections.sort(lista);

        System.out.println(lista);

    }
}
