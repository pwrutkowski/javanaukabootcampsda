package zaawansowana.lambda.consumer;

import java.util.function.Consumer;

public class LambdaConsumerExample {



    //    /*
//    @FunctionalInterface
//    public interface Consumer<T> {
//
//    /**
//     * Performs this operation on the given argument.
//     *
//     * @param t the input argument
//     */
//    void accept(T t);
//
    public static void main(String[] args) {

        //  Consumer<String> consumer = (String x) -> System.out.println(x);

        Consumer<String> consumer = x -> System.out.println(x);

        consumer.accept("text");

        Consumer<String> c = x -> {
            System.out.println("Hello from lambda");
            System.out.println(x);
        };

        c.accept("halo halo");


        Consumer<Integer> cc = x -> System.out.println(x*x);
        cc.accept(4);

        /*
        Napisz implementacje interfejsu Consumer, ktora bedzie przyjmowala jako argument
        int i drukowala na konsoli sume cyfr w tym typie prostym
         */

        Consumer<Integer> sumaCyfr = x -> {
            int suma = 0;
            do {
                suma += x % 10;
                x = x / 10;
            } while (x != 0);
            System.out.println(suma);
        };
        sumaCyfr.accept(15);





    }
}
