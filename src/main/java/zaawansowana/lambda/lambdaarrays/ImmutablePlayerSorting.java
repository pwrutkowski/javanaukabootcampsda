package zaawansowana.lambda.lambdaarrays;

import zaawansowana.wielowatkowe.ImmutablePlayer;

import java.util.*;
import java.util.function.Function;

public class ImmutablePlayerSorting {

    public static void main(String[] args) {

        ImmutablePlayer player1 = new ImmutablePlayer(150, 18, "Terrmi", null);
        ImmutablePlayer player2 = new ImmutablePlayer(2222, 28, "Rockkyy", null);
        ImmutablePlayer player3 = new ImmutablePlayer(999, 40, "John", null);
        ImmutablePlayer player4 = new ImmutablePlayer(111, 35, "Terrmi", null);

        List<ImmutablePlayer> team = new ArrayList<>(Arrays.asList(player1, player2, player3, player4));
        System.out.println(team);

        Comparator<ImmutablePlayer> cmpName = (p1, p2) -> p1.getName().compareTo(p2.getName());
        Comparator<ImmutablePlayer> cmpRanking = (p1, p2) -> Integer.compare(p1.getRanking(), p2.getRanking());
        team.sort(cmpName);
        System.out.println(team);

        team.sort(cmpRanking);
        System.out.println(team);

        Collections.sort(team, cmpName.thenComparing(cmpRanking));
        System.out.println(team);


        Function<String, Integer> f = (String s) -> s.length();
        Function<ImmutablePlayer, Integer> playerName = (ImmutablePlayer s) -> s.getName().length();

        Comparator<ImmutablePlayer> o = Comparator.comparing(playerName);
        team.sort(o);

        System.out.println(team);


    }
}
