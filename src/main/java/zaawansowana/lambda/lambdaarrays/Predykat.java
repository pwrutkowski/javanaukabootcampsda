package zaawansowana.lambda.lambdaarrays;

import java.util.function.Predicate;
/*
public interface Predicate<T> {

    /**
     * Evaluates this predicate on the given argument.
     *
     * @param t the input argument
     * @return {@code true} if the input argument matches the predicate,
     * otherwise {@code false}
     *
    boolean test(T t);
 */
public class Predykat {

    public static void main(String[] args) {

        String john = "john";
        Predicate<String> predicate;
        //predicate = (String name) -> name.equals("john");
        predicate = name -> name.equals("john");

        System.out.println(predicate.test("adam"));
        System.out.println(predicate.test("john"));

        Predicate<String> isBlank = stringg -> stringg.isBlank();
        System.out.println("is word \"house\" blank? " + isBlank.test("house"));
        System.out.println("is \"\" blank? " + isBlank.test(""));

        //można też tak, ale dłużej:
        //boolean result = isBlank.test("house");
        //System.out.println("result");

        Predicate<String> isPusty = o -> o.isEmpty();
        System.out.println("is \"\" empty? " + isPusty.test(""));


        Predicate<String> isPalindrome = p -> {
            char[] word = p.toCharArray();
            int i1 = 0;
            int i2 = word.length -1;
            while (i2>i1) {
                if (word[i1] != word[i2]) {
                    return false;
                }
                ++i1;
                --i2;
            }
            return true;
        };

        System.out.println(isPalindrome.test("abaa"));
    }
}
