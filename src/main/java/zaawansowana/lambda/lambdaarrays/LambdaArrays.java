package zaawansowana.lambda.lambdaarrays;

import jdk.swing.interop.SwingInterOpUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class LambdaArrays {

    public static void main(String[] args) {

        List<String> names = new ArrayList<>(Arrays.asList("John", "Adam", "Zbyszek", "Beniu", ""));
        System.out.println(names);
//        String nameToRemove = "Adam";
//        Predicate<String> predicate = name -> name.equals(nameToRemove);
//        System.out.println(predicate.test("Adam"));
//        System.out.println(predicate.test("Marianem"));

        names.removeIf(name -> name.isEmpty());
        names.removeIf(name -> name.equals("Adam"));

        System.out.println(names);

        names.forEach(n -> System.out.println(n));
     //   names.forEach(n-> n.);




    }
}
