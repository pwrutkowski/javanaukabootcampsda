package zaawansowana.lambda.powtorka;

public interface Human {

    void eat();
    void sleep();
}
