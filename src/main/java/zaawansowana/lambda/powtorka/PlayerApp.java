package zaawansowana.lambda.powtorka;

/*
interfejsy rozdzielają to co klasa robi od tego jak to robi. można podmienić klasę implementującą interfejs i będzie ok

 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PlayerApp {

    public static void main(String[] args) {

        List<Player> team = new ArrayList<>();
        team.add(new Player("John", "Rambo", 19));
        team.add(new Player("Frank", "Rambo", 22));
        team.add(new Player("Arnold", "Terminator", 100));


        //Collections.sort(team);  tak się nie da od razu, trzeba zaimplementowac komparator

        //Comparator<Player> cmpPlayerNameComparator = new PlayerNameComparator();  można tak

        //System.out.println(team);

        //albo tak
       // Collections.sort(team, new PlayerNameComparator());

        System.out.println(team);

        //albo tak, klasą anonimową: (klasa anonimowa jeszcze obsłuży dwie metody, ale lambda tylko jedną)
        Comparator<Player> cmp = new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };

        Collections.sort(team, cmp);

        System.out.println(team);

        //albo tak
        Comparator<Player> cmplam = (p1, p2) -> p1.getName().compareTo(p2.getName());
                //  int compare(T o1, T o2);
        Collections.sort(team, cmplam);

        //można to zapisać jeszcze krócej lambdą
        team.sort((p1, p2) -> p1.getName().compareTo(p2.getName()));

        //referencja metody to inny sposób zapisywania lambdy, bardziej czytelny, wada: brak możliwości przekazania argumentów
        team.sort(Comparator.comparing(Player::getName));

        //można rozbudować
       // team.sort(Comparator.comparing(Player::getNazwisko)).thenComparing((Player::getName));



    }
}
