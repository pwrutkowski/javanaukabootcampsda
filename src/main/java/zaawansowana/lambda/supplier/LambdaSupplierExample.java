package zaawansowana.lambda.supplier;

import java.util.Random;
import java.util.function.Supplier;

public class LambdaSupplierExample {


    public static void main(String[] args) {

//        @FunctionalInterface
//        public interface Supplier<T> {
//
//            /**
//             * Gets a result.
//             *
//             * @return a result
//             */
//            T get();
//        }

        //Krok 1: tworzymy zmienną typu interfejsu funkcyjnego
            //będziemy tworzyć instancję interfejsu funkcyjnego
        Supplier<String> supplier;   //to typ generyczny, więc trzeba go zparametryzować ( tutaj do Stringa)
            //Wchodzimy z ctrl przez suppliera i kopiujemy metodę (wklejamy przed^)

        //Krok 2: Skopiuj argumenty
            // supplier = ()
        //Krok 3: Postaw znaczek lambda
            // supplier = () ->
        //Krok 4: Jaki typ zwraca? (Dostarcz implementację)
        // supplier = () -> "";
        //Krok 5: Co zwraca?
        // supplier = () -> "Hello!";
        supplier = () -> {
            return "Hello!";
        };

        supplier = () -> "jasdasdasd";   //on bierze pod uwagę ostatnie przypisanie tylko

     //   String result = supplier.get();
    //    System.out.println(result);
     //   to samo co:

        System.out.println(supplier.get());

        supplier = () -> {
            return "Hello!";
        };

        System.out.println(supplier.get());




        /*
        Napisz dwie implementacje interfejsu Supplier<T>,
        jedna bedzie zwracala obiekt typu Integer, druga typu String zawieraja liczbe z przedzialu 1 - 49
         */

        //zwracają Integer
        Supplier<Integer> supplierInt;
        Supplier<Integer> supplierInt2;

        supplierInt = () -> {
            return (int) ((Math.random() * (49 - 1)) + 1);
        };

        supplierInt2 = () -> (int) ((Math.random() * (49 - 1)) + 1);

        System.out.println(supplierInt.get());
        System.out.println(supplierInt2.get());


        //zwraca String
        Supplier<String> supplierString;

        supplierString = () -> String.valueOf((int)((Math.random() * (49 - 1)) + 1));

        System.out.println(supplierString.get());
    }
}
