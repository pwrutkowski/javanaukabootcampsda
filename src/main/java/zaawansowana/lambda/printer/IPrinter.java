package zaawansowana.lambda.printer;

@FunctionalInterface //interfejs funkcyjny 'mozemy' oznaczyc adnotacja
//@FunctionalInterface i wtedy kompilator sprawdza, czy interfejs jest funkcyjny
//jesli to nie sprawdzi
public interface IPrinter {

    //interfejs funkcyjny, jest to interfejs z jedna metoda abstrakcyjna
    void print(String wordToPrint);



}