package zaawansowana.lambda.printer;
//Zaimplementuj interfejs IPrinter z uzyciem lambdy. Jedna drukarka niech drukuje do malych liter, druga do duzych


public class PrinterApp {

    public static void main(String[] args) {

        String doDruku = "Tekst Do Druku";

        IPrinter ps = o -> System.out.println(o.toUpperCase());   //pierwsza wersja
        ps.print(doDruku);

        String doDruku2 = "Tekst Do Druku";

        PrinterService prService = new PrinterService();  //to musi być, chcemy przepuścić przez printeservice
        prService.print(o -> System.out.println(o.toUpperCase()), doDruku2);  //druga wersja
        prService.print(o -> System.out.println(o.toLowerCase()), doDruku2);


    }
}
