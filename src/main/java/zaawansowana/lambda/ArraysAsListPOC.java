package zaawansowana.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArraysAsListPOC {

    public static void main(String[] args) {
        List<String> names = new ArrayList<>(Arrays.asList("John", "Adam", "Zbyszek", "Beniu"));
        //mamy listę      stworzylismy nowa arrayliste która robi plytka kopie tej listy z tablicy

        System.out.println(names);

        names.add("Wojtek");
        System.out.println(names);

        List<String> namesAsList = Arrays.asList("John", "Adam", "Zbyszek", "Beniu");
        System.out.println(namesAsList);
      //  namesAsList.add("Wojtek");
      //  System.out.println(namesAsList);

        //metoda Arrays.asList - ponieważ nie da się dodać ani odjąć, więc mozna
        // spojrzeć jako na oszczednosc pamieciqA
    }
}
