package zaawansowana;

import java.util.Locale;

public class Rozgrzewka {
    public static void main(String[] args) {
        // obiekt immutable nie zmienia swojego stanu, jeśli wywołamy na nim jakąś metodę
        // Przyklady klas immutable: String, Character, BigDecimal, Byte
        //final String letters = "abc";
        String letters = "abc";
        //letters.toUpperCase();

        // w ten sposob 'modyfikujemy' wartosc obiektu immutable
        //letters = letters.toUpperCase();

        System.out.println(letters.toUpperCase());
        System.out.println(letters);

        //String letters to referencja
        //        Referencja to zmienna, która wskazuje na obiekt.
        //        Referencja to wartosc liczbowa, ktora okresla lokalizacje danego obiektu w pamieci.

        letters = "23232";
        System.out.println(letters);
    }
}
