package zaawansowana.liczbyzkonsoli;

import java.util.Scanner;

public class LiczbyApp {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ILiczby liczby = new Liczby();
        String text;

        do {
            System.out.println("Podaj nową liczbę lub napisz \"exit\":");
            text = input.nextLine();
            if (text.equalsIgnoreCase("exit")) {
                break;
            }
            double scannedNumber = Double.parseDouble(text);
            liczby.addNumber(scannedNumber);
            System.out.println(liczby);
        } while (!text.equalsIgnoreCase("exit"));

        System.out.println("Suma: " + liczby.sum());
        System.out.println("Srednia: " + liczby.avg());
        System.out.println("Min: " + liczby.min());
        System.out.println("Max: " + liczby.max());
    }
}
