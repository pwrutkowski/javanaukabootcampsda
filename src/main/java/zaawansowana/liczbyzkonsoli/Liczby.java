package zaawansowana.liczbyzkonsoli;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Liczby implements ILiczby {
    List<Double> liczby = new ArrayList<>();
    double suma;

    @Override
    public double avg() {
        return suma / liczby.size();
    }

    @Override
    public double sum() {
        for (int i = 0; i < liczby.size(); i++) {
            suma += liczby.get(i);
        }
        return suma;
    }

    @Override
    public double min() {
        return Collections.min(liczby);
    }

    @Override
    public double max() {
        return Collections.max(liczby);
    }

/*    public double max() {
        double max = liczby.get(0);
        for (int i = 0; i < liczby.size(); i++ +) {
            if (liczby.get(i) > max) {
                max = liczby.get(i);
            }
        }
        return max;
    }*/


    @Override
    public void addNumber(double x) {
        liczby.add(x);
    }

    @Override
    public String toString() {
        return "Liczby{" +
                "liczby=" + liczby +
                '}';
    }

}
