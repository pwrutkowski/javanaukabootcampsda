package zaawansowana.liczbyzkonsoli;

public interface ILiczby {
    double avg();

    double sum();

    double min();

    double max();

    void addNumber(double x);


}
