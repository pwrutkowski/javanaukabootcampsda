package zaawansowana;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ListaPOC {
    public static void main(String[] args) {

        //Odwoływanie się przez Interfejs List jest bardziej poprawne.
        ArrayList<String> names = new ArrayList<>();  //nawias diamentowy przekazuje typ.
        List<String> cars = new ArrayList<>();          //typ sparametryzowany,
        cars = new LinkedList<>();

        cars.isEmpty() ;
        names.ensureCapacity(20);

        long start = System.nanoTime();
        long finish = System.nanoTime();
        long timeElapsed = finish - start;
        System.out.println(timeElapsed);

        cars.add("bmw");
        cars.add("audi");
        cars.add("dacia");
        System.out.println(cars.size());
        System.out.println(cars);
        cars.add(1, "mercedes");
        System.out.println(cars);
        cars.remove("audi");
        System.out.println(cars);
        System.out.println(cars.get(2));

        //jesli przekroczymy indeks to błąd
        if (cars.size() >= 4) {
            System.out.println(cars.get(3));
        }

        //nie zastanawiajmy się jak oczyścić listę
//        cars.clear();
//        System.out.println(cars);

        for (int i = 0; i < cars.size(); i++) {
            System.out.println(cars.get(i));
            cars.set(i, "psikus");  //to możemy zmienić
        }

        //tutaj nie mamy dostępu do indeksu i nie musimy pilnować rozmiaru
        for (String car: cars) {
            System.out.println(car);
            car = "hahahah"; //to niczego nie zmieni
        }

        //trzeci sposób
        //musimy sprawdzić czy element jest zanim o niego poprosimy, inaczej błąd jeśli nie będzie elementu
        Iterator<String> iterator = cars.iterator();
        while (iterator.hasNext()) { //pętla while się dobrze nadaje to Iteratora. do while czasem też
            String carFromIterator = iterator.next();
            System.out.println(carFromIterator + " iterator");
            //w iteratorze możemy usuwać konkretny element
            if (carFromIterator.equals("jakiś konkretny samochód do usunięcia")) {
                iterator.remove();
            }
        }
        //iterator.next(); tego się nie da, bo przejechaliśmy przez wszystkie iteratory
        System.out.println(cars);
    }
}
