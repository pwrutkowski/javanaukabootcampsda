package zaawansowana.personduplikatyset;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class PersonApp {
    public static void main(String[] args) {
        //nie ma znaczenia nazwa zmiennej do której jest przypisany obiekt
        //tworząc klasę, my musimy zdefiniować kiedy obiekty są takie same.
        Person osoba1 = new Person("Kasia", "Nowak", "4534345345345");
        Person osoba2 = new Person("Kasia", "Nowak", "4534345345345");
        Person osoba3 = new Person("Mariusz", "Adamek", "4534345345345");
        Person osoba4 = new Person("Krzysztof", "Adamek", "4534345345345");
        Person osoba5 = new Person("Janek", "Adamek", "4534345345345");
        Person osoba6 = osoba3;

        Set<Person> setOsob = new HashSet<>();
        setOsob.add(osoba1);
        setOsob.add(osoba2);
        setOsob.add(osoba3);
        setOsob.add(osoba4);
        setOsob.add(osoba5);
        setOsob.add(osoba6);
        System.out.println(setOsob);
        System.out.println();
        //
        System.out.println(osoba6.equals(osoba3));  //equals sprawdza czy obiekty są takie same. ale tworząc klasę, my musimy zdefiniować kiedy obiekty są takie same.
        System.out.println(osoba1.equals(osoba2));

        System.out.println();
        System.out.println(Objects.hash(1,"2"));
        System.out.println(Objects.hash("Kasia", "Nowak", "4534345345345"));
        System.out.println(Objects.hash("Kasia", "Nowak", "4534345345345"));
    }
}
