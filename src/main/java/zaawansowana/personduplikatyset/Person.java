package zaawansowana.personduplikatyset;

import java.util.Objects;

/*
Utworz klase Person z polami
Name
Surname
pesel
Utworz liste obiektow klasy Person, dodaj np 5 obiektow, wazne musza byc duplikaty
Wyswietl liste z duplikatami
Uzywajac seta usun z listy duplikaty i wyswietl liste Person
 */
public class Person {
    private String name;
    private String surname;
    private String pesel;

    public Person(String name, String surname, String pesel) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Person)) return false;
//        Person person = (Person) o;
//        return Objects.equals(name, person.name) && Objects.equals(surname, person.surname) && Objects.equals(pesel, person.pesel);
//    }
//      //equals sprawdza czy obiekty są takie same (nie te same. tan sam obiekt jest wariantem takiego samego obiektu).
//      //ale tworząc klasę, my musimy zdefiniować kiedy obiekty są takie same.
//
//    @Override     //generujemy sobie cyfrowy podpis/reprezentację jakiegoś obiektu
//    public int hashCode() {
//        return Objects.hash(name, surname, pesel);
//    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", pesel='" + pesel + '\'' +
                '}';
    }
}
