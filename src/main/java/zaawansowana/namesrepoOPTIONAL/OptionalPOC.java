package zaawansowana.namesrepoOPTIONAL;

import java.util.Optional;

public class OptionalPOC {
    public static void main(String[] args) {

        OptionalNamesRepo repo = new OptionalNamesRepo();
        System.out.println(repo.findUserByLogin("psikus"));

        //jeśli metoda zwraca potencjalnie null to musimy w toku działania programu sprawdzać,
        //czy wartość zwracana nie jest nullem aby uniknac wyjatku
        String result = repo.findUserByLogin("terminatora");

        if (result != null) {
            System.out.println(repo.findUserByLogin("expected Result"));
        }

        Optional<String> resultFromOptional = repo.findUserByLoginOptional("costam");
        if (resultFromOptional.isPresent()) {
            String userName = resultFromOptional.get();
            System.out.println(userName);
        }
    }
}
