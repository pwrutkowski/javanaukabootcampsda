package zaawansowana.namesrepoOPTIONAL;

public class NamesRepository {
    private String[] names;  //problem jest, że tu dostajemy referencję
    private int counter;

    public NamesRepository(int size) {
        this.names = new String[size];
    }

    public void addName(String name) {
        if (counter >= names.length) {
            System.out.println("List full");
            return;
        }
        names[counter++] = name;
    }

    public String[] getNames() {
        //tu musielibysmy zrobic kopie, aby przeciwdzialas psikusowi

        //String[] copy = Arrays.copyOf(names)
        //return copy;
        return names;
    }
}
