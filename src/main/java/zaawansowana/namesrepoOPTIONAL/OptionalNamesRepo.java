package zaawansowana.namesrepoOPTIONAL;

import java.util.Optional;

public class OptionalNamesRepo {

    //super logika
    String findUserByLogin(String login) {
        String result = "John Rambo";
       // String result = null;
       // String result = "";

        //Zakładamy że tu wywołuje  się jakaś logika poszukiwana usera w bazie użytkowników
        return result;

        //pytanie co zwraca jesli nie ma takiego użytkownika
        //generalnie często można się spotkać z praktyką zwracania null przy braku rezultatów

    }

    //Optional stosujemy jako typ zwracany dla metod, które mogą zwracać pusty rezultat
    //Przykład poszukiwanie klienta w bazie, produktu, którego nie ma...
    Optional<String> findUserByLoginOptional(String login) {
        //Zakładamy że tu wywołuje  się jakaś logika poszukiwana usera w bazie użytkowników
        //String result = "John Rambo";
        String result = null;

        //scenariusz jeden zwracamy pustego Optionala
        //return Optional.empty(); //scenariusz braku rezulatów

        //scenariusz drugi
        //scenariusz 2 jestesmy pewnie, ze result nie jest nullem
        //jesli bedzie nullem to ten sposob rzucyi wyjatkiem
//        return Optional.of(result);

        //scenariusz 3
        //Tworzomy opiek Optional na bazie obiektu lub nulla
        //najbardziej uniwersalny sposob
        return Optional.ofNullable(result);

    }
}
