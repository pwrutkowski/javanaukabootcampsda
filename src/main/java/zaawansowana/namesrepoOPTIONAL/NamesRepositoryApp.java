package zaawansowana.namesrepoOPTIONAL;
import java.util.Arrays;
public class NamesRepositoryApp {
    public static void main(String[] args) {
        NamesRepository repository = new NamesRepository(4);
        repository.addName("John");
        repository.addName("Rambo");
        repository.addName("Rocky");

        //wydruk tablicy z uzyciem metody statycznej klas Arrays
        System.out.println(Arrays.asList(repository.getNames()));

        //Nie powinnismy tak robic
        String[] names = repository.getNames();
        names[0] = "psikus";            //names to ta sama tablica ale w dwóch różnych miejscach
        names[1] = "psikus";
        names[1] = "psikus";
        System.out.println(Arrays.asList(repository.getNames()));
    }
}

//my nie nadpisujemy tylko zmieniamy stan
