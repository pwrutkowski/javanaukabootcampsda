package zaawansowana.homework.hashmapa;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/*
Stwórz metodę, która jako parametr przyjmuje mapę, gdzie kluczem jest string, a wartością liczba, a następnie wypisuje każdy element mapy do konsoli w formacie:
Klucz: <k>, Wartość: <v> v>.
Na końcu każdego wiersza poza ostatnim, powinien być przecinek, a w ostatnim kropka.
 */
public class WypisanieHashmapy {

    static void printMap(Map<String, Integer> map) {
        Iterator<Map.Entry<String, Integer>> iterator = map.entrySet().iterator();
        StringBuilder builder = new StringBuilder();
        while (iterator.hasNext()) {
            Map.Entry pair = iterator.next();
            builder.append("Klucz: ");
            builder.append(pair.getKey());
            builder.append(" Wartość: ");
            builder.append(pair.getValue());
            builder.append(iterator.hasNext() ? "," : ".");
            builder.append("\n");
        }

        System.out.println(builder);
    }

    public static void main(String[] args) {
        Map<String, Integer> mapa = new HashMap<>();
        mapa.put("Java", 18);
        mapa.put("Python", 1);
        mapa.put("PHP", 0);
        mapa.put("CSS", 24);
        mapa.put("C++", 13);

        int count = 0;
        for (String each : mapa.keySet()) {
            System.out.print("Klucz: " + each + ", Wartość: " + mapa.get(each));
            count++;
            if (count != mapa.size()) {
                System.out.println();
            } else {
                System.out.println(".");
            }
        }

        System.out.println();
        printMap(mapa);


    }

}

