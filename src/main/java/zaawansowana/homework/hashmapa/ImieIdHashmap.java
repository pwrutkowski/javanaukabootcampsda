package zaawansowana.homework.hashmapa;

import java.util.HashMap;
import java.util.Map;

/**
 * Zadanie
 * 1.W metodzie main utwórz mape < Long , String>, gdzie docelowo klucz bedzie reprezentowal id, a wartosc imie.
 * 2.Dodaj rekordy(pary) do mapy, tak aby kilka z nich zawieralo w wartosci imie na litere A.
 * 3.Korzystajac z mapy, wyswietl wszystkie imiona zaczynajace sie na litere A.
 * 4.Jesli jest taka potrzeba, dodaj do mapy rekordy tak aby imie „Jan” wystepowalo kilkukrotnie.
 * 5.Korzystajac z mapy, wyswietl wszystkie id, które przechowuja wartosc „Jan”.
 */

public class ImieIdHashmap {





    public static void main(String[] args) {
        Map<Long, String> mapa = new HashMap<>();
        mapa.put((long) 2525, "Ania");
        mapa.put((long) 3121, "Andrzej");
        mapa.put((long) 1634, "Anita");
        mapa.put((long) 7563, "Antoni");
        mapa.put((long) 2434, "Gimli");
        mapa.put((long) 0007, "Gandalf");
        mapa.put((long) 0005, "Sauron");
        mapa.put((long) 0157, "Obieżyświat");

        System.out.println(mapa);

        System.out.println("\nPierwszy sposób");
        for (Map.Entry<Long, String> each : mapa.entrySet()) {
            if (each.getValue().charAt(0) == 'A') {
                System.out.println(each.getValue());
            }
        }

        System.out.println("\nDrugi sposób");
        mapa.forEach((key, value) -> {
                    if (value.charAt(0) == 'A') {
                        System.out.println(mapa.get(key));
                    }
                }
        );


        System.out.println("\nDodajemy trzech Janów");
        mapa.put((long) 6574, "Jan");
        mapa.put((long) 1234, "Jan");
        mapa.put((long) 7878, "Jan");

        System.out.println(mapa);

        mapa.forEach((key, value) -> {
                    if (value == "Jan") {
                        System.out.println(key);
                    }
                }
        );
    }
}

/**
 *  default void forEach(BiConsumer<? super K, ? super V> action) {
 *         Objects.requireNonNull(action);
 *         for (Map.Entry<K, V> entry : entrySet()) {
 *             K k;
 *             V v;
 *             try {
 *                 k = entry.getKey();
 *                 v = entry.getValue();
 *             } catch (IllegalStateException ise) {
 *                 // this usually means the entry is no longer in the map.
 *                 throw new ConcurrentModificationException(ise);
 *             }
 *             action.accept(k, v);
 *         }
 *     }
 */