package zaawansowana.homework.lista20losowych;

import java.util.List;
import java.util.Random;

/*
1.Utwórz liste liczb calkowitych.
2.Wypelnij ja dwudziestoma wylosowanymi liczbami z zakresu <0, 10>.
3.Wyswietl zawartosc listy.
4.Usun z listy wszystkie liczby parzyste.
5.Wyswietl zawartosc listy.
Postaraj sie zrealizowac obiektowo
 */

public class Lista20 {
    List<Integer> lista;

    public Lista20(List<Integer> lista) {
        this.lista = lista;
    }

    public void fillList() {
        Integer x = null;
        for (int i = 0; i < 20; i++) {
            Random rnd = new Random();
            //random.nextInt(max - min + 1) + min
            x = rnd.nextInt(10);
            lista.add(x);
        }
    }

    public List<Integer> getLista() {
        return lista;
    }

    public void removeEven() {
        int i = 0;
        while (i < lista.size()) {
            if (lista.get(i)%2 == 0 && lista.get(i)!=0) {
                lista.remove(i);
            } else {
                i++;
            }
        }

    }

}
