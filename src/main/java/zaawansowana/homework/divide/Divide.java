package zaawansowana.homework.divide;

/*
Zadanie
Zaimplementuj metodr divide, ktora docelowo ma podzielic dwie liczby bedące atrybutami metody.
W przypadku, gdy drugi parametr metody jest rowny 0, powinien zostac wyrzucony utworzony typ wyjatku CannotDivideByZeroException.
 */

public class Divide {

    public Divide() {
    }

    public double divideFirstCheckZero(int x, int y) throws CannotDivideByZeroException {
        if (isYzero(y)) {
            throw new CannotDivideByZeroException("Zły argument, y nie może być 0");
        }
        return x / y * 1.0;
    }

    private boolean isYzero(int y) {
        if (y == 0) {
            return true;
        }
        return false;
    }


    public double DivideXbyY(int x, int y) {
        try {
            divideFirstCheckZero(x, y);
        } catch (CannotDivideByZeroException e) {
             e.printStackTrace();
            System.out.println();
            System.out.println(e.getMessage());

        }

        return 0;
    }

    public static void main(String[] args) {
        Divide div = new Divide();

        double z = div.DivideXbyY(5, 0);
        System.out.println(z);
    }
}