package zaawansowana.homework.divide;

public class CannotDivideByZeroException extends ArithmeticException{
    public CannotDivideByZeroException(String s) {
        super(s);
    }
}
