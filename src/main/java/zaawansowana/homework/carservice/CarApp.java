package zaawansowana.homework.carservice;

public class CarApp {
    public static void main(String[] args) {

        CarService service = new CarService();

        service.addCarToList(new Car("Dodge", "RAM", 300000, 2010, "V8"));
        service.addCarToList(new Car("Chevrolet", "Corvette C8", 500000, 2015, "V8"));
        service.addCarToList(new Car("Ford", "C-Max", 100000, 2020, "R4"));
        service.addCarToList(new Car("Audi", "A4 Avant", 100000, 2020, "R4"));
        service.addCarToList(new Car("Audi", "Q7", 200000, 2018, "V12"));
        service.addCarToList(new Car("Porsche", "911 Carrera", 300000, 2020, "B6"));
        service.addCarToList(new Car("Ferrari", "812 Superfast", 500000, 2020, "V12"));
        service.addCarToList(new Car("Ferrari", "Portofino", 400000, 2020, "V8"));
        service.addCarToList(new Car("Fiat", "Portofino", 400000, 2020, "V8"));

        service.listCarList();

        service.getCarList().get(0).addToManufacturersList("Dodge", 1913, "USA");
        service.getCarList().get(1).addToManufacturersList("Chevrolet", 1911, "USA");
        service.getCarList().get(2).addToManufacturersList("Ford", 1903, "USA");

        service.getCarList().get(3).addToManufacturersList("Audi", 1909, "DE");
        service.getCarList().get(3).addToManufacturersList("Bosch", 1886, "DE");
        service.getCarList().get(3).addToManufacturersList("Mahle", 1920, "DE");

        service.getCarList().get(4).addToManufacturersList("Audi", 1909, "DE");
        service.getCarList().get(4).addToManufacturersList("Bosch", 1886, "DE");
        service.getCarList().get(4).addToManufacturersList("Mahle", 1920, "DE");

        service.getCarList().get(5).addToManufacturersList("Porsche", 1931, "DE");
        service.getCarList().get(6).addToManufacturersList("Ferrari", 1938, "IT");
        service.getCarList().get(7).addToManufacturersList("Ferrari", 1938, "IT");
        service.getCarList().get(8).addToManufacturersList("FIAT", 1899, "IT");

        System.out.println("\nTestowe wypisanie listy producentów dla jednego elementu");
        service.getCarList().get(3).listManufacturersList();

        System.out.println("\nWypisanie V12");
        service.listV12();

        System.out.println("\nWypisanie przed 2018 rokiem");
        service.listBeforeYear(2018);

        System.out.println("\nMax cena");
        service.maxPrice();

        System.out.println("\nMin cena");
        service.minPrice();

        System.out.println("\nZnalezienie auta z min 3 producentami i wypisanie ich i ich producentów");
        service.minManufacturers(3);

        System.out.println("\nCzy auto jest na liście");
        //zrobiłem oddzielne "equalsSearch" sprawdzanie list producentów bez porównywania  ceny i listy producentów
        //zrobiłem nowy konstruktor dla samochodu sprawdzanego bez ceny i nie inicjujący listy porducentów
        Car checkCar = new Car("Ferrari", "Portofino", 2020, "V8");
        System.out.println(service.checkIfOnList(checkCar));

        System.out.println("\nCzy auto jest zrobione przez danego producenta?");
        Manufacturer checkFerrari = new Manufacturer("Ferrari", 1938, "IT");
        System.out.println("new Manufacturer(\"Ferrari\", 1938, \"IT\");");
        service.checkCarsByManufacturer(checkFerrari);


    }
}

//        List<Manufacturer> listaProd = new ArrayList<>();
//        listaProd.add(new Manufacturer("Dodge", 1913, "USA"));
//        listaProd.add(new Manufacturer("Chevrolet", 1911, "USA"));
//        listaProd.add(new Manufacturer("Ford", 1903, "USA"));
//        listaProd.add(new Manufacturer("Audi", 1909, "DE"));
//        listaProd.add(new Manufacturer("Porsche", 1931, "DE"));
//        listaProd.add(new Manufacturer("Ferrari", 1938, "IT"));
//        listaProd.add(new Manufacturer("FIAT", 1899, "IT"));

//        Manufacturer dodge = new Manufacturer("Dodge", 1913, "USA");
//        Manufacturer chevy = new Manufacturer("Chevrolet", 1911, "USA");
//        Manufacturer ford = new Manufacturer("Ford", 1903, "USA");
//        Manufacturer audi = new Manufacturer("Audi", 1909, "DE");
//        Manufacturer porsche = new Manufacturer("Porsche", 1931, "DE");
//        Manufacturer ferrari = new Manufacturer("Ferrari", 1938, "IT");
//        Manufacturer fiat = new Manufacturer("FIAT", 1899, "IT");
//
//        Car.setManufacturersList(listaProd);
//
//        Car.listManufacturersList();
//
//        System.out.println();

