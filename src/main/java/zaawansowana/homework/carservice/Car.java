package zaawansowana.homework.carservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/*
Stwórz klasę Car , która będzie zawierać pola: nazwa, model, cena, rok produkcji, lista producentów
Manufacturer ), oraz typ silnika.
Uwzględnij wszystkie niezbędne dane metody oraz parametry konstruktora. Zaimplementuj metody hashcode() i equals()
 */
public class Car {
    private String nazwa;
    private String model;
    private int cena;
    private int rokProd;
    private String typSilnika;
    private List<Manufacturer> manufacturersList;

    public Car(String nazwa, String model, int cena, int rokProd, String typSilnika) {
        this.nazwa = nazwa;
        this.model = model;
        this.cena = cena;
        this.rokProd = rokProd;
        this.typSilnika = typSilnika;
        manufacturersList = new ArrayList<>();
    }

    public Car(String nazwa, String model, int rokProd, String typSilnika) {
        this.nazwa = nazwa;
        this.model = model;
        this.rokProd = rokProd;
        this.typSilnika = typSilnika;
    }

    public String getNazwa() {
        return nazwa;
    }

    public String getModel() {
        return model;
    }

    public int getCena() {
        return cena;
    }

    public int getRokProd() {
        return rokProd;
    }

    public String getTypSilnika() {
        return typSilnika;
    }

    public List<Manufacturer> getManufacturersList() {
        return manufacturersList;
    }

    public void addToManufacturersList(String manName, int manYear, String manCountry) {
        manufacturersList.add(new Manufacturer(manName, manYear, manCountry));
    }

    public void listManufacturersList() {
        for (Manufacturer element : manufacturersList) {
            System.out.println(element);
        }
    }

    public void setManufacturersList(List<Manufacturer> manufacturersList) {
        this.manufacturersList = manufacturersList;
    }

    public boolean equalsSearch(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return getCena() == car.getCena() && getRokProd() == car.getRokProd() && Objects.equals(getNazwa(), car.getNazwa()) && Objects.equals(getModel(), car.getModel()) && Objects.equals(getTypSilnika(), car.getTypSilnika());
    }

    @Override
    public String toString() {
        return "Car{" +
                "nazwa='" + nazwa + '\'' +
                ", model='" + model + '\'' +
                ", cena=" + cena +
                ", rokProd=" + rokProd +
                ", typSilnika='" + typSilnika + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return  getRokProd() == car.getRokProd() && Objects.equals(getNazwa(), car.getNazwa()) && Objects.equals(getModel(), car.getModel()) && Objects.equals(getTypSilnika(), car.getTypSilnika());
    }



    @Override
    public int hashCode() {
        return Objects.hash(getNazwa(), getModel(), getCena(), getRokProd(), getTypSilnika());
    }
}
