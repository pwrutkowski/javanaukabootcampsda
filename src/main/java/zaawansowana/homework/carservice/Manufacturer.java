package zaawansowana.homework.carservice;

import java.util.Objects;

/*
Stwórz klasę Manufacturer , która będzie zawierać pola: nazwa, rok założenia, kraj. Uwzględnij wszystkie
niezbędne meotody oraz parametry konstruktora. Zaimplementuj metody hashCode() i equals()
 */
public class Manufacturer {
    private String manName;
    private int manYear;
    private String manCountry;

    public Manufacturer(String manName, int manYear, String manCountry) {
        this.manName = manName;
        this.manYear = manYear;
        this.manCountry = manCountry;
    }

    public String getManName() {
        return manName;
    }

    public int getManYear() {
        return manYear;
    }

    public String getManCountry() {
        return manCountry;
    }

    public void setManName(String manName) {
        this.manName = manName;
    }

    public void setManYear(int manYear) {
        this.manYear = manYear;
    }

    public void setManCountry(String manCountry) {
        this.manCountry = manCountry;
    }

    @Override
    public String toString() {
        return "Manufacturer{" +
                "manName='" + manName + '\'' +
                ", manYear='" + manYear + '\'' +
                ", manCountry='" + manCountry + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Manufacturer)) return false;
        Manufacturer that = (Manufacturer) o;
        return getManYear() == that.getManYear() && Objects.equals(getManName(), that.getManName()) && Objects.equals(getManCountry(), that.getManCountry());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getManName(), getManYear(), getManCountry());
    }
}
