package zaawansowana.homework.carservice;

import java.util.ArrayList;
import java.util.List;

/*
Stwórz klasę CarService , która będzie zawieraćw sobie listę aut , oraz będzie realizować poniższe metody:
    1.dodawanie aut do listy,
    2.usuwanie auta z listy,
    3.zwracanie listy wszystkich aut,
    4.zwracanie aut z silnikiem V 12 ,
    5.zwracanie aut wyprodukowanych przed rokiem 1999 ,
    6.zwracanie najdroższego auta,
    7.zwracanie najtańszego auta,
    8.zwracanie auta z co najmniej 3 producentami,
9.zwracanie listy wszystkich aut posortowanych,
    10.sprawdzanie czy konkretne auto znajduje się na liście,
    11.zwracanie listy aut wyprodukowanych przez konkretnego producenta,

 */
public class CarService {
    private List<Car> carList;

    public CarService() {
        carList = new ArrayList<>();
    }

    public void addCarToList(Car car) {
        carList.add(car);
    }

    public void removeCarFromList(Car car) {
        carList.remove(car);
    }

    public void listCarList() {
        for (int i = 0; i < carList.size(); i++) {
            System.out.println("Element o indeksie: " + i + " to " + carList.get(i));
        }
//        for (Car element : carList) {
//            System.out.println(element);
//        }
    }

    public void listV12() {
        for (Car element : carList) {
            if (element.getTypSilnika().equals("V12")) {
                System.out.println(element);
            }
        }
    }

    public void listBeforeYear(int year) {
        for (Car element : carList) {
            if (element.getRokProd() < year) {
                System.out.println(element);
            }
        }
    }

    public void maxPrice() {
        int max = 0;
        for (Car element : carList) {
            if (element.getCena() > max) {
                max = element.getCena();
            }
        }
        System.out.println(max);
        //Collections.max(carList.)
    }

    public void minPrice() {
        int min = 1000000;
        for (Car element : carList) {
            if (element.getCena() < min) {
                min = element.getCena();
            }
        }
        System.out.println(min);
        //Collections.min(carList.)
    }

    public void minManufacturers(int min) {
        for (Car element : carList) {
            if (element.getManufacturersList().size() >= min) {
                System.out.println("\n" + element);
                element.listManufacturersList();
            }
        }
    }

    public void sort() {
       //    Collections.sort();
    }

//10.sprawdzanie czy konkretne auto znajduje się na liście,
    public boolean checkIfOnList(Car car) {
        for (Car element : carList) {
            if (element.equalsSearch(car)) {
                return true;
            }
        }
        return false;
    }

//11.zwracanie listy aut wyprodukowanych przez konkretnego producenta,
    public void checkCarsByManufacturer(Manufacturer manu) {
        boolean flag = false;
        for (Car element : carList) {
            if (element.getManufacturersList().contains(manu)) {
                System.out.println("\nTak, dany producent produkuje: " + element);
                element.listManufacturersList();
                flag = true;
            }
        }
        if (!flag) {
            System.out.println("Nie ma auta produkowanego przez danego producenta!");
        }

    }

    public List<Car> getCarList() {
        return carList;
    }

    public void setCarList(List<Car> carList) {
        this.carList = carList;
    }
}
