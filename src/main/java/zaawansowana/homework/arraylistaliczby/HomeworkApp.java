package zaawansowana.homework.arraylistaliczby;

/*
Wypelnij arrayliste 10 losowo wygenerowanymi liczbami typu int.
Wypisz
a) wszystkie po kolei
b) wszystkie od końca
c) wszystkie na nieparzystych pozycjach
d) wszystkie podzielne przez 3
e) sumę wszystkich
f) sumę pierwszych 4
g) sumę ostatnich 5 większych od 2
h) ilość liczb idąc od początku tablicy, by ich suma przekroczyła 10

 */

import java.util.ArrayList;
import java.util.List;

public class HomeworkApp {

    public static void main(String[] args) {
        int gornaGranica = 10;
        List<Integer> lista = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            lista.add((int) (Math.random() * gornaGranica));
        }

        System.out.println("\nWypisanie po kolei");
        for (int random : lista) {
            System.out.println(random);
        }

        System.out.println("\nWypisanie od końca");
        for (int i = lista.size() - 1; i >= 0; i--) {
            System.out.println(lista.get(i));
        }

        System.out.println("\nWypisanie nieparzystych pierwszym sposobem (if (i%2!=0))");
        for (int i = 0; i < lista.size(); i++) {
            if (i % 2 != 0) {
                System.out.println("Na pozycji " + i + " liczba to: " + lista.get(i));
            }
        }

        System.out.println("\nWypisanie nieparzystych pierwszym sposobem: for(i=1;...;i+=2)");
        for (int i = 1; i < lista.size(); i += 2) {
            System.out.println("Na pozycji " + i + " liczba to: " + lista.get(i));
        }

        System.out.println("\nWypisanie podzielnych przez 3");
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) % 3 == 0) {
                System.out.println("Na pozycji " + i + " liczba to: " + lista.get(i) + " jest podzielna przez 3");
            }
        }

        System.out.println("\nSuma wszystkich");
        int suma = 0;
        for (int random : lista) {
            suma += random;
        }
        System.out.println(suma);

        System.out.println("\nSuma pierwszych 4");
        suma = 0;
        for (int i = 0; i < 4; i += 1) {
            suma += lista.get(i);
        }
        System.out.println(suma);

        System.out.println("\nSuma ostatnich 5 większych od 2");
        suma = 0;
        int counter = 0;
        int i = (lista.size() - 1);
        while (counter != 5 && i != 0) {
            if (lista.get(i) > 2) {
                counter++;
                suma += lista.get(i);


            }
            i--;
        }
        System.out.println(suma);

        System.out.println("\nilość liczb idąc od początku tablicy, by ich suma przekroczyła 10");
        suma = 0;
        counter = 0;
        i = 0;
        while (suma <= 10 && i != (lista.size() - 1)) {

            suma += lista.get(i);
            i++;
            counter++;


        }
        System.out.println(counter);

    }

//        Iterator<Integer> iterator = lista.iterator();
//        while (iterator.hasNext()) { //pętla while się dobrze nadaje to Iteratora. do while czasem też
//            String carFromIterator = iterator.next();
//            System.out.println(carFromIterator + " iterator");
//     //   lista.sort();
}

