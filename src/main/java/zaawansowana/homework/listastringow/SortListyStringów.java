package zaawansowana.homework.listastringow;

import java.util.Collections;
import java.util.List;

public class SortListyStringów {
    private List<String> lista;

    public SortListyStringów(List<String> lista) {
        this.lista = lista;
    }

    public List<String> sortReverseCaseSensitive() {
        Collections.sort(lista);
        Collections.reverse(lista);
        return lista;
    }

    public List<String> sortReverseCaseInsensitive() {
        Collections.sort(lista, new NameReverseCaseInsensitiveComparator());
        return lista;
    }

    @Override
    public String toString() {
        return "SortListyStringów{" +
                "lista=" + lista +
                '}';
    }
}
