package zaawansowana.homework.listastringow;

import java.util.ArrayList;
import java.util.List;

public class SortListyStringowApp {

    public static void main(String[] args) {

        List<String> listaStringow = new ArrayList<>();
        listaStringow.add("Zez");
        listaStringow.add("Lek");
        listaStringow.add("osa");
        listaStringow.add("As");
        listaStringow.add("Wirus");

        SortListyStringów listSort = new SortListyStringów(listaStringow);

        System.out.println(listSort.sortReverseCaseSensitive());


        System.out.println(listSort.sortReverseCaseInsensitive());


    }
}
