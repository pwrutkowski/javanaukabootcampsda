package zaawansowana.homework.listastringow;

import java.util.Comparator;

public class NameReverseCaseInsensitiveComparator implements Comparator<String> {

    @Override
    public int compare(String o1, String o2) {
        int result = o2.toLowerCase().compareTo(o1.toLowerCase());
        return result;
    }
}
