package zaawansowana.homework.computer;

import java.util.Objects;

/*
Stwórz klasę
Computer posiadającą pola określające cechy komputera: procesor, ram, karta grafiki, firma
oraz model. Zaimplementuj settery, gettery, konstruktor z wszystkimi polami, metody toString() oraz
equals() i hashcode()
Zainstancjuj kilka obiektów i sprawdź działanie metod.
 */
public class Computer {
    private String procesor;
    private int ram;
    private String kartaGraf;
    private String producent;
    private String model;

    public Computer(String procesor, int ram, String kartaGraf, String producent, String model) {
        this.procesor = procesor;
        this.ram = ram;
        this.kartaGraf = kartaGraf;
        this.producent = producent;
        this.model = model;
    }

    public String getProcesor() {
        return procesor;
    }

    public int getRam() {
        return ram;
    }

    public String getKartaGraf() {
        return kartaGraf;
    }

    public String getProducent() {
        return producent;
    }

    public String getModel() {
        return model;
    }

    public void setProcesor(String procesor) {
        this.procesor = procesor;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public void setKartaGraf(String kartaGraf) {
        this.kartaGraf = kartaGraf;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "Computer { " +
                "procesor='" + procesor + '\'' +
                ", ram=" + ram +
                ", kartaGraf='" + kartaGraf + '\'' +
                ", producent='" + producent + '\'' +
                ", model='" + model + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Computer)) return false;
        Computer computer = (Computer) o;
        return getRam() == computer.getRam() && Objects.equals(getProcesor(), computer.getProcesor()) && Objects.equals(getKartaGraf(), computer.getKartaGraf()) && Objects.equals(getProducent(), computer.getProducent()) && Objects.equals(getModel(), computer.getModel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProcesor(), getRam(), getKartaGraf(), getProducent(), getModel());
    }
}
