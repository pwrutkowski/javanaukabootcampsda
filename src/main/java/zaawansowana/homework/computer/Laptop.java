package zaawansowana.homework.computer;

import java.util.Objects;

/*
Stwórz klasę
Laptop rozszerzającą klasę Computer z poprzedniego zadania. Klasa Laptop powinna
dodatkowo zawierać parametr bateria.
Zaimplementuj dodatkowe gettery, settery, konstruktor oraz odpowiednio nadpisz metody
toString() oraz
equals() i hashcode()
Użyj odniesienia do metod klasy nadrzędnej.
 */

public class Laptop extends Computer {
    private int battery;
    private String color;

    public Laptop(String procesor, int ram, String kartaGraf, String producent, String model, int battery, String color) {
        super(procesor, ram, kartaGraf, producent, model);
        this.battery = battery;
        this.color = color;
    }

    public int getBattery() {
        return battery;
    }

    public String getColor() {
        return color;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Laptop " + super.toString() + "battery=" + battery + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Laptop)) return false;
        if (!super.equals(o)) return false;
        Laptop laptop = (Laptop) o;
        return getBattery() == laptop.getBattery() && Objects.equals(getColor(), laptop.getColor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getBattery(), getColor());
    }


}
