package zaawansowana.homework.computer;

public class CompApp {
    public static void main(String[] args) {
        Computer asus1 = new Computer("i7", 16, "Nvidia GFX980Ti", "Asus", "ROG");
        Computer asus2 = new Computer("i3", 4, "Zintegrowana", "Asus", "123");
        Computer acer = new Computer("i5", 8, "ATI Radeon 1050", "Acer", "Biznes");
        Computer acerduplikat = new Computer("i5", 8, "ATI Radeon 1050", "Acer", "Biznes");

        Computer sony = new Laptop("i7", 8, "Nvidia GFX260M", "Sony", "Vaio", 2200, "Silver");
        Computer sonyduplikat = new Laptop("i7", 8, "Nvidia GFX260M", "Sony", "Vaio", 2200, "Silver");
        Computer macbook = new Laptop("Apple M1", 8, "Zintegrowana", "Apple", "MacbookPro13", 2200, "Black");

        Laptop macademia1 = new Laptop("Apple M1", 8, "Zintegrowana", "Apple", "MacbookPro13", 2200, "Silver");
        Laptop macademia2 = new Laptop("Apple M1", 8, "Zintegrowana", "Apple", "MacbookPro13", 2200, "Silver");


        System.out.println(asus1);
        System.out.println(sony);
        System.out.println(macbook);
        System.out.println();

        asus1.setRam(32);
        System.out.println("Asus1 po upgrade RAM: " + asus1);

        macbook.setRam(16);
        System.out.println("Macbook po upgrade RAM: " + macbook);
        System.out.println();

        System.out.println(sonyduplikat.equals(sony));
        System.out.println();

        //daje true przy @@equalsach działających, a także przy działającym @@ w computer ale zakomentowanym @@ w Laptop
        //daje false, gdy zakomentuje się @equals i @hashcode w Computer
        //dlaczego???

        //sprawdzamy co gdy i typ i klasa jest laptop
        System.out.println(macademia1.equals(macademia2));
        //to samo
        //dodałem nawet String color do pól klasy Laptop, żeby sprawdzić, czy to nie problem z porównaniem typu prymitywnego int
    }
}
