package zaawansowana.konwertergalonow;

import java.util.Arrays;

public class GalConvApp {
    public static void main(String[] args) {
        GalConv myConverter = new GalConv();
        //System.out.println(Arrays.toString(myConverter.getGallonConversion()));
        myConverter.printTable();

        System.out.println();

        IGallonConverter x = new GalConv();
        // zmienna interfejs          //tworzymy nowy obiekt klasy implementującej interfejs GalConv!!!
        System.out.println(x.getGallonConversionTable());
       // System.out.println(x.getGallonConversionTable());


    }
}

/*
Aby skorzystac z interfejsu potrzebujesz klasy implemntujacej interfejs.
Nie można stworzyć instancji interfejsu. Interfejsy mogą być.
To znaczy, ze nie mozemy stworzyc obiekut na bazie interfejsu.
interfejs jest kontraktem.

po lewej stronie definiujemy typ oraz nazwe zmiennej
po prawej przypisujemy konkretny obiekt

 */