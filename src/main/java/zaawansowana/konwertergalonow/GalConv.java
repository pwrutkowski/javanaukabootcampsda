package zaawansowana.konwertergalonow;

import java.text.DecimalFormat;
import java.util.Arrays;

public class GalConv implements IGallonConverter {
    private String[] convTable;
//UWAGA, nie wszystkie liczby 10 mają reprezentację w systemie dwójkowym

    DecimalFormat newFormat = new DecimalFormat("#.###");

    public GalConv() {
        this.convTable =new String[100];
        fillTable();
    }

    private void fillTable() {
        for (int i=0; i<100; i++) {
            convTable[i] = (i+1) + " galonów to " + newFormat.format((i+1) * 3.785) + " litrów.";
        }
    }

    public String[] getGallonConversion() {
       return convTable;
    }

    public void printTable() {
        for (int i = 0; i < 100; i++) {
            //System.out.println(Arrays.toString(convTable));
            if (i % 10 == 0) {
                System.out.println();
            }
            System.out.println(convTable[i]);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String position : convTable) {
            sb.append(position);
            sb.append("\n");
        }
        return sb.toString();
    }

    @Override
    public String[] getGallonConversionTable() {
        return convTable;
    }
}
