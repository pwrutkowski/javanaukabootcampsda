package zaawansowana.konwertergalonow;

public interface IGallonConverter {
    public String[] getGallonConversionTable();
}