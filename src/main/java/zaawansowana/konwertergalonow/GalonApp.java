package zaawansowana.konwertergalonow;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;

public class GalonApp {

    /*
    Napisz program obliczajacy konwersje galonow na litry od 1 litra do 100 galonow. Zapisz wyniki konwersji do tabeli typu double.
    Wyniki wyswietl w blokach po 10 pozycji
    1 galon to 3,7854 litra
    Wyniki wyswietl w formie
    x galonow to x litrów
    grupujac po 10 wynikow w bloku
     */

    public static void main(String[] args) {
        Double[] listaGalonow = new Double[100];
        //HashMap<Integer, Double> litreGallon = new HashMap<Integer, Double>();
        //to komplkuje, ale trzeba wymusić żeby był amerykański system zapisu, bo nagle gdzieś wstawia przecinek zamiast kropkii parseDouble głupieje
        DecimalFormatSymbols symbolsEN_US = DecimalFormatSymbols.getInstance(Locale.US);
        //to żeby dać
        DecimalFormat newFormat = new DecimalFormat("#.###", symbolsEN_US);

        for (int i = 1; i <= 100; i++) {
            double wynik = Double.parseDouble(newFormat.format(i * 3.785));    //newFormat wyrzuca stringa, trzeba go przerobić na integera
            listaGalonow[i-1] = wynik;
            System.out.println(i + " galonów to " + listaGalonow[i-1] + " litrów");
            //litreGallon.put(i, wynik);
            //System.out.println(i + " galonów to " + litreGallon.get(i) + " litrów");
            if (i % 10 == 0) {
                System.out.println();
            }
        }
    }
}
