package zaawansowana.zrekrutacji;

//Napisz program, ktory bedzie liczyl liczbe wystapien okreslonych znakow w slowie.
//        Przykladowa sygnatura
//        public int countOccurrencesLettersInWord(String word, char[] letters){...}
//        gdzie word jest przekazanym slowem, a letters to znaki, ktorych wystapienia zliczamy
//        Przyklad
//public int countOccurrencesLettersInWord("Dom to nie auto", new char[] {'a','m'})
//        Wynik 2

public class LiczbaWystapienZnakow {
    private int suma;
    private int sumaDlaIndexOf;
    private int loopCounter;
    private int loopIndexOfCounter;

    public void sumowanieLiter() {
        suma++;
    }

    public void sumowanieLiterIndexOf() {
        sumaDlaIndexOf++;
    }

    public int checkString(String word, char[] letters) {
        suma = 0;  //zerowanie licznika bo używamy tej samej instancji
        for (char i : word.toCharArray()) {
            checkCharExistInTable(i, letters);
        }
        System.out.println("Stan licznika dla zwykłej metody: " + loopCounter);
        return suma;
    }

    private void checkCharExistInTable(char literka, char[] letters) {
        for (int j = 0; j < letters.length; j++) {
            loopCounter++;
            if (letters[j] == literka) {            // z drugiej strony powinniśmy przerwać
                sumowanieLiter();  //kilkukrotne wystąpienia tej samej litery w zdaniu!!!!
                break; //przerywa pętle, a coninue - kończy iterację
            }

        }
    }

    public int checkOccurenceOfEveryChar(String word, char[] letters) {
        sumaDlaIndexOf = 0; //zerowanie licznika bo używamy tej samej instancji
        for (int j = 0; j < letters.length; j++) {
            checkStringWithIndexOf(word, letters[j]);
        }
        System.out.println("Stan licznika dla metody z IndexOf: " + loopIndexOfCounter);
        return sumaDlaIndexOf;

    }

    public void checkStringWithIndexOf(String word, char literkaZTablicy) {
        int i = 0;
        while (i != (-1)) {

            i = word.indexOf(literkaZTablicy, i);

            if (i != -1) {
                sumowanieLiterIndexOf();
                i++;
            }
            loopIndexOfCounter++;
        }
    }

    public static void main(String[] args) {
        LiczbaWystapienZnakow testy = new LiczbaWystapienZnakow();

        System.out.println(testy.checkString("Dom to nie auto", new char[]{'a', 'm'}));
        System.out.println(testy.checkOccurenceOfEveryChar("Dom to nie auto", new char[]{'a', 'm'}));

        System.out.println(testy.checkString("Dom to nie auto", new char[]{'t', 'e', 'n'}));
        System.out.println(testy.checkOccurenceOfEveryChar("Dom to nie auto", new char[]{'t', 'e', 'n'}));
    }
}