package zaawansowana.zrekrutacji;

public class NajmniejszaPozytywna implements INajmniejszaPozytywna {


    @Override
    public int najmniejszaDodatniaSpoza(int[] doSprawdzenia) {
        for (int i = 1; i <= (doSprawdzenia.length + 1); i++) {         //przeszukujemy zbiór o jeden większy od wielkości tablicy (wtedy na pewno coś znajdziemy nawet jeśli będzie 123
            if (!czyJestWTablicy(i, doSprawdzenia)) {
                return i;
            }
        }
        return -1;  //tak jakoś błąd
    }

    public boolean czyJestWTablicy(int x, int[] tablica) {
        for (int j = 0; j <= (tablica.length - 1); j++) {               //przeszukujemy czy tablica zawiera daną liczbę dodatnią "x"
            if (tablica[j] == x) {
                return true;
            }
        }
        return false;
    }
    //Rozwiązanie Kuby
    @Override
    public int number(int[] tab) {
        int lowestPozitive = 1;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == lowestPozitive) {
                lowestPozitive += 1;
                i = 0;
            }
        } return lowestPozitive;
    }
}