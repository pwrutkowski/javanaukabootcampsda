package zaawansowana.zrekrutacji;

public interface INajmniejszaPozytywna {
    int najmniejszaDodatniaSpoza(int[] doSprawdzenia);
    int number(int[] tab);
}
