package zaawansowana.czyparzysta;

import java.util.Scanner;

public class CzyParzystaApp {
    //Napisz program sprawdzajacy, czy liczba jest parzysta.

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj liczbę. Sprawdzimy, czy jest parzysta:");
        int czyParz = Integer.parseInt(input.nextLine());
        if (czyParz % 2 == 0) {
            System.out.println("Liczba jest parzysta");
        } else {
            System.out.println("Liczba nie jest parzysta");
        }
    }
}
