package zaawansowana.car2;

public class CarApp {
    public static void main(String[] args) {

        Car car1 = new Car("Honda", 55, 5);
        Car car2 = new Car("Audi", 60, 8.5);
        Car[] cars = {car1, car2};
        for (Car x : cars) {
            System.out.println(x);
        }

        System.out.println("Zużycie paliwa na dystansie: " + car1.calculateFuelConsumption(200));
        System.out.println(car1);
        System.out.println();
        System.out.println("Lejemy pod korek, wlaliśmy: " + car1.fillFuel());
        System.out.println(car1);
        System.out.println();
        System.out.println("Zasięg: " + car1.getCarRange());
        System.out.println("Chcemy wlać 10 litrów, wlaliśmy: " + car1.fillFuel(10));
        System.out.println(car1);
        System.out.println();
        System.out.println("Chcemy przejechać 550 km. Jedziemy: " + car1.drive(550) + " km.");
        System.out.println("Stan po jeździe. " + car1);
        System.out.println();
        System.out.println("Chcemy wlać 10 litrów, wlaliśmy: " + car1.fillFuel(10));
        System.out.println(car1);
        System.out.println();


        System.out.println("Chcemy przejechać 1100 km. Jedziemy: " + car1.drive(1100) + " km.");
        System.out.println(car1);
        System.out.println();

        System.out.println("Chcemy wlać 100 litrów, wlaliśmy: " + car1.fillFuel(100));
        System.out.println(car1);
        System.out.println();
        System.out.println("Chcemy przejechać 1100 km. Jedziemy: " + car1.drive(1100) + " km.");
        System.out.println(car1);
        System.out.println();

        CarApp.max(car2);
        CarApp.go(car2);
        System.out.println();
        for (Car x : cars) {
            System.out.println(x);
        }

    }

    /*
    Można dostać się do jednego obiektu na różne sposoby
     */
    public static void go(Car o) {
        o.drive(15);
    }

    public static void max(Car o) {
        o.fillFuel();
    }

}
