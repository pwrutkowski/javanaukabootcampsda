package zaawansowana.car2;

public class Car {

    private int mileage; //typy proste domyślnie są = 0
    private double fuelLevel;
    private int tankCapacity;
    private double fuelConsumption;
    private String manufacturer;
    private int rezerwa = 50; //rezerwa w km

    public Car(String manufacturer, int tankCapacity, double fuelConsumption) {
        this.tankCapacity = tankCapacity;
        this.fuelConsumption = fuelConsumption;
        this.manufacturer = manufacturer;
    }

    public int getMileage() {
        return mileage;
    }

    public double getFuelLevel() {
        return fuelLevel;
    }

    public int getCarRange() {
        return (int) (100.00 * (fuelLevel / fuelConsumption));
    }

    double calculateFuelConsumption(int distance) {
        return fuelConsumption * distance / 100.00;
    }

    public int drive(int distance) {
        if (distance <= getCarRange()) {
            mileage = getMileage() + distance;
            fuelLevel = getFuelLevel() - calculateFuelConsumption(distance);
            return distance;
        } else {
            System.out.println("Nie przejedziesz tyle, wprowadź nową wartość.");
            return 0;
        }
    }

    public double fillFuel(double fuelToFill) {  //to dolewamy kilka litrów
        double ableToFill = (tankCapacity - fuelLevel);
        if ((fuelLevel + fuelToFill) <= tankCapacity) {
            fuelLevel += fuelToFill;
            return fuelToFill;
        } else if (fuelLevel >= tankCapacity) {
            System.out.println("Bak pełny. Wlałeś: 0 litrów");
            return 0;
        } else {
            fillFuel();  //dolewamy do pełna, używamy metody do pełna
            System.out.println("Za dużo chciałeś wlać. Dolałeś: " + ableToFill + " litrów.");
            return ableToFill;
        }
    }

    public double fillFuel() {                  //tankowanie pod korek
        if (fuelLevel >= tankCapacity) {
            return 0;
        } else {
            double oldLevel = fuelLevel;            //zmienna tymczasowa - stary stan paliwa}}
            fuelLevel = tankCapacity;   //tu dolewamy - po dopełnieniu nowy stan paliwa to jest pojemność zbiornika  //musimy to zapisać, bo inaczej nie zmienimy stanu zbiornika
            return tankCapacity - oldLevel;         //nowy stan minus stary stan daje to ile dolaliśmy //po returnie nic nie da się zrobić
        }
    }


    @Override
    public String toString() {
        return "Car{" +
                "manufacturer='" + manufacturer + '\'' +
                ", mileage=" + mileage +
                ", fuelLevel=" + fuelLevel +
                ", tankCapacity=" + tankCapacity +
                ", fuelConsumption=" + fuelConsumption +
                '}';
    }
}