package zaawansowana.stringbuilder;

public class StringBuilderPOC {
    public static void main(String[] args) {
        String dzien = "Dzien ";
        String plus = " plus ";
        String word = 1 + 1 + dzien + "dobry" + plus + " usmiech " + "a dzien " + " bedzie dobry" + 1 + 1 + (1 + 1);
        System.out.println(word);

        //dzieki StringBuilderowi konkatenujac Stringi nie tworzymy po drodze nikomu niepotrzebnych obiektow
        //ale UWAGA używajmy z rozwagą, czytelność kodu gora, raczej, gdy tworzymy obiekt w petlach

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Dzien");
        stringBuilder.append(" dobry ");
        stringBuilder.append(plus);
        stringBuilder.append("bedzie dobry");
        stringBuilder.append('r');
        String wordFromStringBuilder = stringBuilder.toString();
        System.out.println(wordFromStringBuilder);

        //chainowanie zapisu
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Dzien")
                .append(" dobry ")
                .append(plus)
                .append("bedzie niedobry")
                .append('r');
        String word2FromStringBuilder = stringBuilder2.toString();
        System.out.println(word2FromStringBuilder);
        System.out.println(stringBuilder2.reverse());


    }
}
