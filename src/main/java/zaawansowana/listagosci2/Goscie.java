package zaawansowana.listagosci2;

import java.util.*;

public class Goscie implements IGoscie {
    private Set<String> setGosci;

    public Goscie() {
        setInit();
    }

    public void setInit() {
        setGosci = new HashSet<>();
    }

    @Override
    public boolean addName(String guest) {
                                            //        if (setGosci == null) {
                                            //            setInit();                                     //opóźniona inicjalizacja, ale tylko wtedy kiedy jest sens
                                            //            System.out.println("Checkpoint: Pierwszy gość");
                                            //            return setGosci.add(guest);
                                            //        }
        guest = guest.trim();
        if (guest.isEmpty()) {
            System.out.println("Checkpoint: Niepoprawne dane");
            return false;
        }
                                            //        if (isNameOnList(guest)) {                       //To niepotrzebne bo .add zwroci false, kiedy bedzie zle
                                            //            System.out.println("Checkpoint: " + guest + " jest już na liście.");
                                            //            return false;
                                            //        }
        return setGosci.add(guest); //set.add robi swoje i zwraca true/false

    }

    @Override
    public boolean isNameOnList(String guest) {
        return setGosci.contains(guest);
    }

    @Override
    public List<String> listList() {
        List<String> listList = new ArrayList<>(setGosci);
        return listList;
    }

    @Override
    public String toString() {
        return listList().toString();
    }
}