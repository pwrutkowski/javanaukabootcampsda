package zaawansowana.listagosci2;

import java.util.List;

public interface IGoscie {
    boolean addName(String guest);
    boolean isNameOnList (String guest);
    List<String> listList();

}
