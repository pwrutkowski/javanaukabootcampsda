package zaawansowana.account;

public class Account {
    private String name;
    private int balance = 0;
    private boolean debit = false;
    int maxDebit = -1000;
    // balance = 0; -nie możemy tak zrobić jeśli nie jesteśmy w metodzie, w klasie main

    public void deposit(int amount) {
        if (amount <= 0) {
            System.out.println("Kwota musi być dodatnia");
            return;
        }
        int balancePrzed = balance;
        balance += amount;
        if (balance > 0 && debit) {                         //Jeśli byłeś w debecie a już nie jesteś to
            debit = false;                                  //Kasujemy debet
            System.out.println("Jesteś już nad kreską!");   //Wyświetlami komunikat
        }
        System.out.println("Wpłata. Przed " + balancePrzed + "PLN. | Kwota: " + amount + "PLN | Po: " + balance + "PLN");
    }

    public void withdraw(int amount) {
        if (amount <= 0) {
            System.out.println("Kwota musi być dodatnia");
            return;
        }
        int balancePrzed = balance;
        if (!debit && (balance-amount)>maxDebit) {        //Jeśli nie ma debetu a ewentualny debet nie przekroczy 1000
            balance -= amount;                      //To odejmujemy od balance
            if (balance < 0) {                      //Jeśli po odejmowaniu weszliśmy na minus
                debit = true;                       //debit na true
                System.out.println("Wszedłeś na minus! Więcej nie wyjmiesz!");
            }
        }
        else {
            System.out.println("Stan konta ujemny lub operacja przekracza dopuszczalny debet.");
        }
        System.out.println("Wypłata. Przed " + balancePrzed + "PLN. | Kwota: " + amount + "PLN | Po: " + balance + "PLN");
    }

    public void transfer (int amount, Account account2) {
        if (amount <= 0) {
            System.out.println("Kwota musi być dodatnia");
            return;
        }
        int tmp = balance;
        this.withdraw(amount);
        if (tmp != balance) {
            account2.deposit(amount);
        } else {
            System.out.println("Przelew nie zostanie wykonany. Brak możliwości wypłaty środków z konta.");
        }
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Account{" +
                "name='" + name + '\'' +
                ", balance=" + balance +
                ", debit=" + debit +
                ", maxDebit=" + maxDebit +
                '}';
    }
}