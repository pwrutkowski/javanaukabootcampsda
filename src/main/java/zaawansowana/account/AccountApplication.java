package zaawansowana.account;

public class AccountApplication {
    public static void main(String[] args) {
        Account account = new Account();
        Account account2 = new Account();

        account.setName("Konto Premium");
        account2.setName("Konto Drugie");

        System.out.println("Nazwa: " + account.getName());
        System.out.println("Stan konta: " + account.getBalance());
        account.deposit(200);

        account.withdraw(150);

        account.withdraw(150);

        System.out.println();
        account.withdraw(150);

        System.out.println();
        account.deposit(200);

        System.out.println();
        System.out.println(account);
        System.out.println(account2);

        System.out.println();
        account.transfer(5000,account2);
        System.out.println("Po przelewie będzie taki stan:");
        System.out.println(account);
        System.out.println(account2);


    }
}
