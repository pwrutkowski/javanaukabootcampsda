package zaawansowana;

public class Rozgrzewka3 {
    public static void main(String[] args) {
       //dla integerów nie ma analoga jak string pool
        //Typy obiektowe porownuje za pomoca equals

        Integer liczba1 = 100;
        Integer liczba2 = 100;
        System.out.println(liczba1 == liczba2);
        System.out.println("Typ zlożony poniżej 128: " + (liczba1 == liczba2));

        Integer liczba3 = 200;
        Integer liczba4 = 200;
        System.out.println("Typ zlożony ponad 128: " + (liczba1 == liczba4));
        //Java zapisuje w cache wartości w przedziale -128 do 128
        //Cache - taki pool dla integerów, dla dużych intów nie działa

        System.out.println(liczba1.equals(liczba2));
        System.out.println(liczba3.equals(liczba4));

        //typy proste mozemy porownywac za pomoca == zawsze, tu nie equals
        int liczba5 = 200;
        int liczba6 = 200;
        System.out.println("typy proste " + (liczba5 == liczba6));
    }
}
