package zaawansowana.lotto;

import java.util.ArrayList;
import java.util.List;

/*
Napisz program ktory symuluje losowanie lotto.
Losuje szesc liczb z zakresu 1 - 49
Wyswietla wyniki i zapisuje do historii losowania
Program ma dzialac z poziomu menu
Funkjce
Losuj
Wyswietl historyczne wyniki
 */
public class Lotto implements ILotto{
    private List<int[]> historia;

    public Lotto() {
        historia = new ArrayList<>();
    }

    @Override
    public void Losuj() {
        MaszynaLosujaca masz = new MaszynaLosujaca();

        masz.startMaszynyLosujacej();

        int[] wyniki = masz.losowanie();

       // wyswietlWynikiLosowania(wyniki);

        historia.add(wyniki);
    }


    @Override
    public void PokazHistorie() {

        for (int[] each : historia) {
            for (int eacheach : each) {
                System.out.print(eacheach + ", ");
            }
            System.out.println();
        }
    }

    public void wyswietlWynikiLosowania(int[] wyniki) {
        for (int each : wyniki) {
            System.out.print(each + ", ");
        }
        System.out.println();
    }


    public List<int[]> PrzekazHistorie() {

        return historia;
    }

    @Override
    public String toString() {
        return "Lotto{" +
                "historia=" + historia +
                '}';
    }
}
