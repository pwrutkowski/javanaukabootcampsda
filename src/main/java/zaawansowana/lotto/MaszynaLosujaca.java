package zaawansowana.lotto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MaszynaLosujaca {
    private List<Integer> beben;  //lista, żeby dało się usunąć wylosowaną kulkę
    private int[] wyniki = new int[6];

    public MaszynaLosujaca() {
        beben = new ArrayList<>();
    }

    public void startMaszynyLosujacej() {
        beben.clear();
        for (int i = 1; i < 50; i++) {
            beben.add(i);
        }
    }

    public int[] losowanie() {
        for (int j = 0; j < 6; j++) {
            int losowaLiczba = (int)(Math.random()*(beben.size()-j));
            wyniki[j] = beben.get(losowaLiczba);
            beben.remove(losowaLiczba);
        }
        return wyniki;
    }

    public int[] losowanieShuffle() {
        Collections.shuffle(beben);
        for (int j = 0; j < 6; j++) {
            this.wyniki[j] = beben.get(j);
        }
        return wyniki;
    }

    @Override
    public String toString() {
        return "MaszynaLosujaca{" +
                "wyniki=" + Arrays.toString(wyniki) +
                '}';
    }
}
/*
Rozwiazanie Krysi
Set, żeby nie dało się dodać dwa razy tego samego
 public Set<Integer> getLiczba() {
        int i;
        Integer x = null;
        if (liczby.size() < 6 || x==0) {
            for (i = 0; i < 6; i++) {
                Random rnd = new Random();
                //random.nextInt(max - min + 1) + min
                x = rnd.nextInt(49);
                liczby.add(x);
            }
            i--;
        }
        return liczby;
    }
 */