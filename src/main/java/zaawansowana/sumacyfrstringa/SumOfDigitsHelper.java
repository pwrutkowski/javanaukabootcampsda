package zaawansowana.sumacyfrstringa;

public class SumOfDigitsHelper {
    //private String textToSearch;
    private int suma = 0;

    //    public SumOfDigitsHelper(String textToSearch) {
    //        this.textToSearch = textToSearch;
    //    }

    public SumOfDigitsHelper() {
    }

    public boolean czyCyfra(char znak) {
        return Character.isDigit(znak);
    }

    public void sumowanieCyfr(int cyfra) {
        suma += cyfra;
    }

    public int liczbowaWartoscZnaku(char znak) {
        return Character.getNumericValue(znak);
    }

    public int getSuma() {
        return suma;
    }

    public int checkString(String textToSearch) {
        for (char i : textToSearch.toCharArray()) {
            if (czyCyfra(i)) {
                sumowanieCyfr(liczbowaWartoscZnaku(i));
            }
        }
        System.out.println("Suma cyfr w tekście: ");
        return getSuma();
    }


}
