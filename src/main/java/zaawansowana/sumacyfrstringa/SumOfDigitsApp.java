package zaawansowana.sumacyfrstringa;

import java.util.Scanner;

public class SumOfDigitsApp {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj tekst do policzenia cyfr:");
        String text = input.nextLine();
        SumOfDigitsHelper suDO = new SumOfDigitsHelper();

        do {
            System.out.println(suDO.checkString(text));
            System.out.println("Podaj nowy tekst do policzenia cyfr:");
            text = input.nextLine();
            suDO = new SumOfDigitsHelper();
        } while (!text.equalsIgnoreCase("exit"));

    }
}
