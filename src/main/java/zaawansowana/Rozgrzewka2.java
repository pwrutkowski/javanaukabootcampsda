package zaawansowana;

public class Rozgrzewka2 {

    public static void main(String[] args) {
        //String pool - mechanizm zarzadzania tworzeniem obietkow String w Javie, zanim zostanie utworzony nowy obiekt
        //typu String sprawdzamy, czy juz nie zostal wczesniej utworzony i jesli zostal to nie tworzymy nowego
        //tylko odwolujemy sie/przpisujemy ten ktory jest

        //String pool - może być na rozmowie
        //String pool - worek/pula stringów, java sprawdza, czy nowy string nie istnieje w puli, jeśli tak, to tylko przypisuje, nie tworzy nowego
        //Java zarządza za nas pamięcią
        //Jest też coś takiego jak garbage collector


        String word1 = "text";
        String word2 = "text";
        String word3 = "t" + "ext";
        String word4 = new String("text"); //wymuszamy utworzenie nowego obiektu
                                                    //tak się nie robi, nie ma to uzasadnienia, marnujemy pamięć

        // operator == porównuje adres w pamięci aby sprawdzic, czy to ten sam obiekt
        System.out.println(word1 == word2);
        System.out.println(word2 == word3);
        System.out.println(word1 == word3);
        System.out.println(word1 == word4);

        //Właściwy sposób porównywania obiektów, sprawdzamy czy to taki sam obiekt
        System.out.println(word1.equals(word4));
    }
}
