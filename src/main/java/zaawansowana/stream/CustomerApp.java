package zaawansowana.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CustomerApp {

    public static void main(String[] args) {

        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer("John", 7));
        customers.add(new Customer("John", 7));
        customers.add(new Customer("Adam", 700));
        customers.add(new Customer("Arnold", 200));
        customers.add(new Customer("Zenon", 300));

        List<Customer> customerMoreThan100Points = customers
                .stream()
                .filter(c->c.getPoints()>100)
             //   .filter(c->c.getPoints()>100&&c.getName().equals("Adam"))
                .collect(Collectors.toList());

        System.out.println(customerMoreThan100Points);

        List<Customer> result = customers.stream().distinct().collect(Collectors.toList()); //działa tylko z hashcode i equals
        System.out.println(result);

//        Collections.sort(customers);

        List<Customer> resultBis = customers.stream()
                .sorted()
                .distinct()
                .filter(c->c.getPoints()>10)
                .collect(Collectors.toList());

        System.out.println(resultBis);

        long count = customers.stream().filter(c->c.getPoints() > 500).count();
        System.out.println(count);




    }
}
