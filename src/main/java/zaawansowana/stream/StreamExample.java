package zaawansowana.stream;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExample {

    public static void main(String[] args) {

        String[] names = new String[]{"John", "Adam", "Zenon", "Arnold"};
        Stream<String> stream = Arrays.stream(names);

        stream = Stream.of("John", "Adam", "Zenon", "Arnold");

        List<String> namess = Arrays.asList(names);
        stream = namess.stream();

        Consumer<String> c = name -> System.out.println(name);
        Consumer<String> cc = System.out::println;

//        @FunctionalInterface
//        public interface Consumer<T> {
//            /**
//             * Performs this operation on the given argument.
//             *
//             * @param t the input argument
//             */
//            void accept(T t);

//        stream.forEach(cc);

//        @FunctionalInterface
//        public interface Predicate<T> {
//            /**
//             * Evaluates this predicate on the given argument.
//             *
//             * @param t the input argument
//             * @return {@code true} if the input argument matches the predicate,
//             * otherwise {@code false}
//             */
//            boolean test(T t);

        Predicate<String> p = name -> name.startsWith("A");

//        stream.filter(p).forEach(cc);
        //nie modyfikuje listy, tworze nowa
        List<String> result = namess.stream()
                .filter(name -> name.startsWith("A"))
                .collect(Collectors.toList());

        System.out.println(result);

    }

}
