package zaawansowana.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class StreamStringExample {

    public static void main(String[] args) {

        List<String> names = new ArrayList<>();
        names.add("Marek");
        names.add("Rambo");
        names.add("Rocky");
        names.add("Terminator");

        System.out.println(names.contains("Rambo"));

        for (String name : names) {
            if (name.equalsIgnoreCase("Rambo")) {
                System.out.println("Rambo jest na liście");
                break;
            }
        }

        Predicate<String> p = name -> name.equalsIgnoreCase("Rambo");
        boolean result = names.stream().anyMatch(p);

        boolean result2 = names.stream().anyMatch(name -> name.equalsIgnoreCase("Rambo"));
        System.out.println(result2);

        boolean resultAllNamesLenghtMoreThan2 = names.stream().allMatch(name -> name.length() > 2);
        System.out.println(resultAllNamesLenghtMoreThan2);

        boolean resultt = names.stream().noneMatch(name -> name.length() > 2 && name.equalsIgnoreCase("Rambo"));
        System.out.println(resultt);

        boolean resulttt = names.stream().noneMatch(name -> name.length() > 2 && name.equalsIgnoreCase("Kot"));
        System.out.println(resulttt);
    }
}
