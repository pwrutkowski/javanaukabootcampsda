package zaawansowana.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamFiltering {

    public static void main(String[] args) {

        List<String> names = new ArrayList<>(Arrays.asList("aJohn", "Adam", "Zbyszek", "Benek", "Adam", "Adam"));

        names = names.stream().filter(name -> name.contains("a")).collect(Collectors.toList());

        System.out.println(names);

        boolean result = names.stream().allMatch(name -> !name.isEmpty());
        System.out.println(result);

        boolean result2 = names.stream().anyMatch(name -> name.contains("y"));
        System.out.println(result2);
    }

}