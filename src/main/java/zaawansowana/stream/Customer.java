package zaawansowana.stream;

/*
Klasy dające dostęp do interfejsu Stream z pakietu java.util.stream
umożliwiają funkcyjny sposób przetwarzania danych.
Strumienie (tzw. streamy) reprezentują sekwencyjny zestaw elementów
i pozwalają na wykonywanie różnych operacji na tych elementach

flatMap  - słuzy do łączenia kilku list
 */

import java.util.Objects;

public class Customer implements Comparable<Customer> {
    private String name;
    private int points;

    public Customer(String name, int points) {
        this.name = name;
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", points=" + points +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;
        Customer customer = (Customer) o;
        return getPoints() == customer.getPoints() && Objects.equals(getName(), customer.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPoints());
    }

    @Override
    public int compareTo(Customer o) {
        return this.getName().compareTo(o.getName());
    }
}
