package zaawansowana.wzorceprojektowe;

import java.util.ArrayList;
import java.util.List;

public class SingletonLogger {

//plusy: jest jeden i przez to zajmuje mniej pamieci lub dobry jesli tworzenie go wymaga czasu i zasobow to oszczędzamy ten czas i zasoby
    //przyklady, jedno połączenie do bazy danych, sekwencja, bufor wydruku, centralny loger
    // minus: może utrudnić testowanie i wprowadzać zależności
    //przechowujemy instancje obiektu w zmiennej statycznej
    //zmienna statyczna jest przymiotem klasy, jest współdzielona

    private static SingletonLogger instance;

    private SingletonLogger() { //prywatny konstruktor, żeby nie dało się stworzyć obiektu
    }

    //zmienna obiektu, gdzie będziemy przechowywali logi
    private List<String> logs = new ArrayList<>();

    //metoda statyczna zwarca instancję singletona, 'zawsze ta sama' (chociaż nie musi być zawsze)
    public static SingletonLogger getInstance(){
        //odwlekamy inicjalizację zmiennej do momentu pierwszego uzyciua, oszczedzamy w ten sposób pamiec
        if (instance == null){
            instance = new SingletonLogger();
        }
        return instance;
    }

    public void log(String logEntry){
        logs.add(logEntry);
    }

    public String getLogs() {
        return logs.toString();
    }
}
