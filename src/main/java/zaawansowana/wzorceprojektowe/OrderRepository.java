package zaawansowana.wzorceprojektowe;

import java.util.ArrayList;
import java.util.List;

public class OrderRepository {

    private List<Order> orders = new ArrayList<>();

    public boolean addOrder(Order order) {
        SingletonLogger.getInstance().log("order added " + order.getOrderNumber());  //poprosiliśmy o instancję loggera i wywołaliśmy na niej metodę
        return orders.add(order);
    }

    public List<Order> getOrders() {
        return new ArrayList<>(orders);
    }

}
