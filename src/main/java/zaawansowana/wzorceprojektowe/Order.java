package zaawansowana.wzorceprojektowe;

public class Order {

    private String orderDescription;
    private int orderNumber;

    public Order(String orderDescription, int orderNumber) {
        this.orderDescription = orderDescription;
        this.orderNumber = orderNumber;
    }

    public Order(String orderDescription) {
        this.orderDescription = orderDescription;
        this.orderNumber = SingletonOrderNumberGenerator.getInstance().getOrderNumber();
    }

    public String getOrderDescription() {
        return orderDescription;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderDescription='" + orderDescription + '\'' +
                ", orderNumber='" + orderNumber + '\'' +
                '}';
    }
}
