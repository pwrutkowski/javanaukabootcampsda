package zaawansowana.wzorceprojektowe;

public class SingletonLoggerOrdersApp {

    public static void main(String[] args) {

        OrderRepository repository = new OrderRepository();
        Order order = new Order("car order", SingletonOrderNumberGenerator.getInstance().getOrderNumber());
        Order order2 = new Order("truck order", SingletonOrderNumberGenerator.getInstance().getOrderNumber());
        Order order3 = new Order("truck order");

        repository.addOrder(order);
        repository.addOrder(order2);
        repository.addOrder(order3);

        System.out.println(SingletonLogger.getInstance().getLogs());     /// albo SingletonLogger l = SingletonLogger.getInstance();  czyli nie konstruktor tylko nazwanie przywoływanej instancji
                                                                            //System.out.println(l.getLogs());
    }
}
