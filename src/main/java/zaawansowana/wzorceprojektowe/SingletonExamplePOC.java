package zaawansowana.wzorceprojektowe;

public class SingletonExamplePOC {
    public static void main(String[] args) {


        //to nie zadziała, bo konstruktor jest prywatny
   //     SingletonExample singleton = new SingletonExample();


        //3 razy prosiłem o obiekt i za każdym razem dostałem ten sam
        SingletonExample singleton = SingletonExample.getInstance();
        System.out.println(singleton.getSequence());

        SingletonExample singleton2 = SingletonExample.getInstance();
        System.out.println(singleton.getSequence());

        SingletonExample singleton3 = SingletonExample.getInstance();
        System.out.println(singleton3.getSequence());
        System.out.println(SingletonExample.getInstance().getSequence());

        //potwierdzam, że zmienne odwołują się do tego samego obiektu
        System.out.println(singleton==singleton2);
        System.out.println(singleton2==singleton3);
    }
}
