package zaawansowana.wzorceprojektowe;

public class SingletonExampleLazy {

    //pole instancji
    private int sequence = 1;

    //cecha klasy, nie obiektu
    private static SingletonExampleLazy instance ; //specjalnie prywatny kontruktor , żeby nikt nie mógł go wywołać z zewnątrz
    //zmienne statyczne sa przechowywane w pamieci przez caly czas, kiedy program dziala

    //prywatny konstruktor
    private SingletonExampleLazy() {
    }

    //statyczna metoda , która zawsze zwraca tę samą instancję
    public static SingletonExampleLazy getInstance(){
        if (instance == null) {
            instance = new SingletonExampleLazy();
        }
        return instance;
    }
    //zwraca kolejną sekwencję i podbija o jeden
    public int getSequence() {
        return sequence++;
    }
}
