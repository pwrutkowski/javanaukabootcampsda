package zaawansowana.wzorceprojektowe;

public class SingletonExample {
    //pole instancji
    private int sequence = 0;

    //cecha klasy, nie obiektu
    private static SingletonExample instance = new SingletonExample(); //specjalnie prywatny kontruktor , żeby nikt nie mógł go wywołać z zewnątrz

    //prywatny konstruktor
    private SingletonExample() {
    }

    //statyczna metoda , która zawsze zwraca tę samą instancję
    public static SingletonExample getInstance() {
        return instance;
    }
    //zwraca kolejną sekwencję i podbija o jeden
    public int getSequence() {
        return sequence++;
    }
}
