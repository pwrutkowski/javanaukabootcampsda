package zaawansowana.wzorceprojektowe;

public class SingletonOrderNumberGenerator {

    private int orderNumber = 1;

    private SingletonOrderNumberGenerator() {
    }

    private static SingletonOrderNumberGenerator instance = new SingletonOrderNumberGenerator();

    public static SingletonOrderNumberGenerator getInstance(){
        return instance;
    }

    //logika biznesowa
    public int getOrderNumber(){
        return orderNumber++;
    }

}
