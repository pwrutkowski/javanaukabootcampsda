package zaawansowana.wzorceprojektowe.decorator;

public class CarImp implements ICar{

    @Override
    public void startEngine() {
        System.out.println("Basic car engine start CarImp");
    }
}
