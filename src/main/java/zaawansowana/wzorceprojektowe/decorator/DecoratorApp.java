package zaawansowana.wzorceprojektowe.decorator;

public class DecoratorApp {

    public static void main(String[] args) {
        ICar car = new CarImp();
        car.startEngine();

        System.out.println();

        car = new BasicCarDecorator(car);
        car.startEngine();

        System.out.println();

        car = new PoliceCarDecorator(car);
        car.startEngine();

        System.out.println();

        car = new LuxuryCarDecorator(car);
        car.startEngine();
    }
}
