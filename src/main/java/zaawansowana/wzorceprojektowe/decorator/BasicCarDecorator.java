package zaawansowana.wzorceprojektowe.decorator;

public class BasicCarDecorator  implements ICar{

    private ICar basicCar;

    public BasicCarDecorator(ICar basicCar) {
        this.basicCar = basicCar;
    }

    @Override
    public void startEngine() {
        this.basicCar.startEngine();
    }
}
