package zaawansowana.wzorceprojektowe.decorator.pizza;


public class SosPomidorowyDecorator extends CiastoDecorator {

    private int sosPomidorowyPrice = 5;

    public SosPomidorowyDecorator(IPizza ciasto) {
        super(ciasto);
    }

    @Override
    public int getPrice() {
        return super.getPrice() + sosPomidorowyPrice;
    }
}
