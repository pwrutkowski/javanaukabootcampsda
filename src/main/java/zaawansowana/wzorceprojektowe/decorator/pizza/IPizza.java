package zaawansowana.wzorceprojektowe.decorator.pizza;

public interface IPizza {

    int getPrice();
}
