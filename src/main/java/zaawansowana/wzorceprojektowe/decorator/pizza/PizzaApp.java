package zaawansowana.wzorceprojektowe.decorator.pizza;
//Zadanie polega na implementacji algorytmu obliczania ceny pizzy przy uzyciu wzorca decoratora.
// Analogicze nalezy stworzyc bazowa klase pizzy z metoda int getPrice()
// i udekorowac ja dodatkowami jak np. mozzarella, szynka, pieczarki.
// Sprawdzic dzialanie programu tworacz pizze z kilkoma dodatkami za pomoca utworzonego kodu i wyswietlic cene.

public class PizzaApp {
    public static void main(String[] args) {
        IPizza ciasto = new Ciasto();
        System.out.println(ciasto.getPrice());

//        ciasto = new CiastoDecorator(ciasto);
//        System.out.println(ciasto.getPrice());

      //  ciasto = new SosPomidorowyDecorator(ciasto);
       // System.out.println(ciasto.getPrice());

        ciasto = new MozarellaDecorator(ciasto);
        ciasto = new MozarellaDecorator(ciasto);
        System.out.println(ciasto.getPrice());
    }
}
