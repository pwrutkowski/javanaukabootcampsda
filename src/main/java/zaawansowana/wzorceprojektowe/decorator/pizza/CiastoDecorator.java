package zaawansowana.wzorceprojektowe.decorator.pizza;

public class CiastoDecorator implements IPizza {

    private IPizza ciasto;

    public CiastoDecorator(IPizza ciasto) {
        this.ciasto = ciasto;
    }

    @Override
    public int getPrice() {
        return ciasto.getPrice();
    }

}

