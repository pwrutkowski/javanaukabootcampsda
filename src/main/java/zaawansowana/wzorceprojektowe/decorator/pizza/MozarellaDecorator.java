package zaawansowana.wzorceprojektowe.decorator.pizza;

public class MozarellaDecorator extends CiastoDecorator {

    private int mozarellaPrice = 7;

    public MozarellaDecorator(IPizza ciasto) {
        super(ciasto);
    }

    @Override
    public int getPrice() {
        return super.getPrice() + mozarellaPrice;
    }
}
