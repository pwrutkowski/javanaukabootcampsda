package zaawansowana.wzorceprojektowe.decorator;

public class LuxuryCarDecorator extends BasicCarDecorator{


    public LuxuryCarDecorator(ICar basicCar) {
        super(basicCar);
    }

    @Override
    public void startEngine() {
        super.startEngine();
        System.out.println("Chill champagne");
    }
}
