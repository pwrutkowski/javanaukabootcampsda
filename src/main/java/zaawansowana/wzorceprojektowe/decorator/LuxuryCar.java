package zaawansowana.wzorceprojektowe.decorator;

public class LuxuryCar extends Car{

    @Override
    public void drive() {
        System.out.println("Chill champagne");
        super.drive();
    }
}
