package zaawansowana.adultperson;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class DataHelper {

    public static LocalDate getDateFromString(String stringInput) {
        final String FORMAT = "yyyy-MM-dd";
        DateTimeFormatter format = DateTimeFormatter.ofPattern(FORMAT);
        LocalDate wczytanaData = LocalDate.parse(stringInput, format);
        return wczytanaData;
    }

    public static LocalDate getDateFromScanner() {
        Scanner input = new Scanner(System.in);
        final String FORMAT = "dd.MM.yyyy";
        System.out.println("Podaj datę w formacie " + FORMAT + ": ");
        String stringDatyScan = input.nextLine();
        DateTimeFormatter format = DateTimeFormatter.ofPattern(FORMAT);
        LocalDate wczytanaData = LocalDate.parse(stringDatyScan, format);
        System.out.println(wczytanaData);
        return wczytanaData;
    }
}
