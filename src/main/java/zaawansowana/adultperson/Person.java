package zaawansowana.adultperson;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Person implements IsAdult {
    private String name;
    private String surname;
    private LocalDate birthDate;

    public Person(String name, String surname, LocalDate birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    @Override
    public boolean isAdult() {
//        LocalDate dataGraniczna = LocalDate.now().minusYears(18);     //data graniczna to dzisiejszy dzień minus 18 lat
//        return birthDate.isBefore(dataGraniczna);                     //czy data urodzenia jest przed datą graniczną
        Period per = Period.between(birthDate, LocalDate.now());
        return (per.getYears() >= 18);
    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
}
