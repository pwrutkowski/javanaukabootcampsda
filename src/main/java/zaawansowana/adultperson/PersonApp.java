package zaawansowana.adultperson;

import java.text.ParseException;
import java.time.LocalDate;

public class PersonApp {
    public static void main(String[] args) throws ParseException {

        IsAdult Franek = new Person("Franek", "Kowalski", DataHelper.getDateFromString("2019-10-19"));
        IsAdult Pawel = new Person("Pawel", "Rutkowski", DataHelper.getDateFromString("1984-05-28"));

        Person Ewa = new Person("Ewa", "Nowak", DataHelper.getDateFromString("2002-05-28"));
        Person Pola = new Person("Pola", "Negri", DataHelper.getDateFromString("2003-05-28"));

        System.out.println("Czy Franek pełnoletni? " + Franek.isAdult());
        System.out.println("Czy Pawel pełnoletni? " + Pawel.isAdult());

        System.out.println(Ewa.getName() + " Czy pełnoletnia? " + Ewa.isAdult());
        System.out.println(Pola.getName() + " Czy pełnoletnia? " + Pola.isAdult());


    }
}
