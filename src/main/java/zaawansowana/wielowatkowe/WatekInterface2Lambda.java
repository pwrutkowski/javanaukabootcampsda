package zaawansowana.wielowatkowe;

public class WatekInterface2Lambda implements Runnable {
    //interface to jest runnable
    //aby utworzyć wątek z użyciem interfejsu trzeba zaimplementować ten interfejs - runnable
//kiedy można to lepiej robic to interfejsem
    @Override
    public void run() {
        System.out.println("pozdrowienia z wątku");
        try {
            for (int i = 0; i < 10; i++) {
                Thread.sleep(400);
                System.out.println("Iteracja numer" + i + " z interfejsu");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
