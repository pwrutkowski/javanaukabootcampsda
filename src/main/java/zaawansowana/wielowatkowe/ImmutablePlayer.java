package zaawansowana.wielowatkowe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ImmutablePlayer {
//Klasa powinna być finalna aby nie zmienić zachowania w klasie dziedziczącej
    //finalne pola klasy
    private final int ranking;
    private final int age;
    private final String name;
    private final List<String> hobby;   //lista jest instancją, więc można zmienić jej stan



    public ImmutablePlayer(int ranking, int age, String name, List<String> hobby) {
        this.ranking = ranking;
        this.age = age;
        this.name = name;
       // this.hobby = hobby;  //do tego też możemy się dorwać
        if (hobby == null) {  //ten if na potrzeby innej metody (immutableplayersortin)
        this.hobby = Collections.emptyList();
        } else {
            this.hobby = new ArrayList<>(hobby);   //więc to trzeba dać, żeby w miejscu konstruktora już robię kopię listy od nowa
            //tworzymy kopię tworząc obiekt

        }
    }


    public int getRanking() {
        return ranking;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public List<String> getHobby() {
        // return hobby;                 //normalnie by było to, ale przez to można oszukać
        return new ArrayList<>(hobby); //jeśli zrobimy taki myk, to zawsze będziemy zwracać listę stringów na bazie
    }                             //będzie odporne na gethobby().set
                    //tworzy kopię zwaracając obiekt

    //brak setterów


    @Override
    public String toString() {
        return "ImmutablePlayer{" +
                ", name='" + name + '\'' +
                "ranking=" + ranking +
                ", age=" + age +
                ", hobby=" + hobby +
                '}';
    }
}
