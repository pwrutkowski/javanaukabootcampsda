package zaawansowana.wielowatkowe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WatkiPOC2 {
    public static void main(String[] args) throws InterruptedException {

        List<String> names = new ArrayList<>(Arrays.asList("Zenon", "Adam", "Wacek", "Julia"));
        System.out.println(names);

        Thread sortingThread = new MyThread2(names);
        //chcemy przypisać go do klasy Thread, bo z niej chcemy czerpać

        sortingThread.start();
        //Thread.sleep(500);
        System.out.println("Hej");
        System.out.println(names);
        sortingThread.join();   //w ten sposób czekamy na zkończenie działania wątku
                                //tym samym may pewność, że wystartował i się zakończył

        //jesli nie poczekamy, to może wyslwietlić listę zanim ją posortujemy. nie poczeka na koniec wątku
        //Bedziemy oczekiwali posortowanej listy, a nie bedzie posortowana, poniewaz watek sortujacy nie ruszy/nie zakonczy dzialania
        System.out.println("Hej jeszcze raz");
        System.out.println(names);

    }
}
