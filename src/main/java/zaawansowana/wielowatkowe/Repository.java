package zaawansowana.wielowatkowe;

import java.util.Collections;
import java.util.List;

public class Repository {
    private List<String> data;

    public Repository(List<String> data) {
        this.data = data;
    }

    //slowo kluczowe synchronized jest najprostszym sposobem na synchronizacje watkow

    //public synchronized void sort() {  //dajemy synchronized, żeby się nie kłóciły
    public void sort() {
        Collections.sort(data);
    }

    public List<String> getData() {
        return data;
    }
}
