package zaawansowana.wielowatkowe;

import java.util.ArrayList;
import java.util.List;

public class KopiaPlytkaVsGleboka {

    public static void main(String[] args) {


        List<Name> names = new ArrayList<>();
        names.add(new Name("John"));
        names.add(new Name("Rocky"));
        names.add(new Name("Rambo"));
        System.out.println("---names---");
        System.out.println(names);

        //kopia płytka
        List<Name> namesCopy = new ArrayList<>(names);
        System.out.println("---namesCopy---");
        System.out.println(namesCopy);

        Name john = namesCopy.get(0);
        john.setName("psikus");
        System.out.println("---names---");
        System.out.println(names);

        //bo to obiekty ze zmiennymi immutable, to tylko referencje do tych samych pól w pamięci

        //przywracamy tu names do stanu pierwotnego
        Name johnBack = namesCopy.get(0);
        johnBack.setName("John");
        System.out.println("---Przywracamy-names---");
        System.out.println(names);

        //Jeśli wykonamy kopię płytką listy przechowującej obiekty mutowalne obie listy nadal będą powiązane
        //zmiana stanu obiektu na jednej liście może wywołać zmianę stanu obiektu na drugiej liście
        //jeśli chcemy, żeby listy były niezależne, to musimy wykonać kopię głęboką

        //Tu robimy kopię głęboką
        List<Name> namesDeepCopy = new ArrayList<>();
        for (Name n : names) {
            namesDeepCopy.add(new Name(n.getName()));
        }
        Name johnDeep = namesDeepCopy.get(0);
        johnDeep.setName("psikus");

        System.out.println("---names---");
        System.out.println(names);

        System.out.println("---namesDeepCopy---");
        System.out.println(namesDeepCopy);
    }
}
