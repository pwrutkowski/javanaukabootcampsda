package zaawansowana.wielowatkowe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RunnableLambda {

    public static void main(String[] args) throws InterruptedException {

        Runnable hello = () -> System.out.println("Hello from thread");

        Thread x = new Thread(hello);
        x.start();


        Thread y = new Thread(() -> System.out.println("Od razu bez ceregieli"));
        y.start();

        List<String> names = new ArrayList<>(Arrays.asList("John", "Adam", "Zbyszek", "Beniu"));
        Thread u = new Thread(()-> Collections.sort(names));
        u.start(); //włączam
        u.join(); //czekam na zakończenie
        System.out.println(names);

        Thread boom = new Thread(()->{
            for (int i = 10; i > 0; i--) {
                System.out.println(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Boom");
        });

        boom.start();
    }
}
