package zaawansowana.wielowatkowe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ImmutablePOC {

    public static void main(String[] args) {

        final String name = "Jan";  //final - zawsze pod name będzie Jan, poza tym Stringa nie da się zmienić, tylko raz przypisujemy  obiekt do zmiennej

        final Name n = new Name("Jan"); //możemy zmienić stan obiektu, nie możemy przypisać innego obiektu/instancji
        n.setName("John");
        System.out.println(n.toString());
                                            //mutowalność obiektu a zmienna final to dwie odrebne sprawy
//        n = new Name("Asia");             //<--tego nie da się zrobić
/*
Dlaczego to ważne? bo ktoś może chcieć skorzystać z naszego obiektu i chcieć zmienić jego stan, bo tak mu wygodniej, wtedy w wielowątkowości zrobi się bałagan

 */
//        ImmutablePlayer player = new ImmutablePlayerMod(22, 19, "Jan");
//        (ImmutablePlayerMod)player).setNameMode("John");
//        System.out.println(player.getName());

        List<String> hobby = new ArrayList<>(Arrays.asList("dom","kot","pies"));
        ImmutablePlayer player = new ImmutablePlayer(99,19,"Jan", hobby);
        System.out.println(player);

        //generalnie tak się nie robi
        player.getHobby().set(0, "psikus");
        //player.getHobby().clear();
        System.out.println(player);

        hobby.set(1, "hahahaha to nie koniec psikusów");
        System.out.println(player);
    }
}
