package zaawansowana.wielowatkowe;

public class MyThread extends Thread {
//tutaj wielowątkowość przez dziedziczenie, ale lepiej przez interfejs w miarę możliwości
    //w metodzie run wpisujemy instrukcje, ktore maja sie wykonac w oddzielnym watku

    //aby zadziałało trzeba napdpisać metodę run
    @Override
    public void run() {
        //super.run();
        System.out.println("pozdrowienia z wątku");
        try {
            for (int i = 0; i < 10; i++) {
                Thread.sleep(400);
                System.out.println("Iteracja numer" + i + " ID " + this.getId() + " Name: " + this.getName());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
