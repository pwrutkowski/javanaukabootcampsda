package zaawansowana.wielowatkowe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

public class RepoWatkiPOC {

    public static void main(String[] args) throws InterruptedException {

        List<String> names = new ArrayList<>(Arrays.asList("Zenon", "Adam", "Wacek", "Julia"));
        //List<String> names = new Vector<>(Arrays.asList("Zenon", "Adam", "Wacek", "Julia")); //rodzaj synchronizowanej listy
        // innym sposobem na synchronizację jest użycie Vectora zamiast ArrayListy, ona ma wbudowane mechanizmy synchronizacji
        // zabiera sporo pamięci, więc w miarę możliwości nie używamy, na pewno nie tam
        Repository x = new Repository(names);

        Thread watek = new RepositoryThread(x);
        Thread watek2 = new RepositoryThread(x);
        watek.start();
        watek2.start();

        watek.join();

        System.out.println(x.getData());

    }
}
