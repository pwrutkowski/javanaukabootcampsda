package zaawansowana.wielowatkowe;

public class RepositoryThread extends Thread{

    private Repository repo;

    public RepositoryThread(Repository repo) {
        this.repo = repo;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            repo.sort();
        }
    }
}
