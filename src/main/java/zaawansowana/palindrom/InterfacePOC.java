package zaawansowana.palindrom;


/*
Interfejs daje możliwość szybkiej zmiany implementacji

W zależności w jaki sposób odwołuję się do
 */
public class InterfacePOC {
    public static void main(String[] args) {
        InterfacePalindrom aaa = new FakePalindrom();
        System.out.println(aaa.isPalindrom());

        aaa = new Palindrom("kajaakk");
        System.out.println(aaa.isPalindrom());

        aaa = new Palindrom("kok");
        System.out.println(aaa.isPalindrom());

        System.out.println();

        Palindrom obiektKlasyPalindrom = new Palindrom("kajak");
        System.out.println(obiektKlasyPalindrom.isPalindrom());
      //  obiektKlasyPalindrom.printWord();
        
        Palindrom tenSamObiektKlasyPalindrom = obiektKlasyPalindrom;
     //   tenSamObiektKlasyPalindrom.printWord();



        InterfacePalindrom tenSamObiektAleOdwolujeSiePrzezInterfejs = obiektKlasyPalindrom;
    //    tenSamObiektAleOdwolujeSiePrzezInterfejs.
    }
}

