package zaawansowana.palindrom;


import java.util.Scanner;

public class Palindrom implements InterfacePalindrom {

    private String inputString;

    public Palindrom(String inputString) {
        this.inputString = inputString;
    }

    public boolean czyPalindrom() {
        for (int i = 0; i < (inputString.length() / 2); i++) {
            if (inputString.charAt(i) != inputString.charAt(inputString.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }

    public static boolean czyPalindrom(String inputString) {
       StringBuilder stringBuilder = new StringBuilder(inputString);
       return inputString.equals(stringBuilder.reverse().toString());
    }

    public static boolean isPalindrom(String inputString) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(inputString);
        String reverseString = stringBuilder.reverse().toString();
        return inputString.equals(reverseString);
    }

    public void nameResult(boolean check) {
        if (check) {
            System.out.println("To jest palindrom");
        } else {
            System.out.println("To nie palindrom!");
        }
    }

    @Override
    public boolean isPalindrom() {
        StringBuilder stringBuilder = new StringBuilder(inputString);
        return inputString.equals(stringBuilder.reverse().toString());
    }
}
