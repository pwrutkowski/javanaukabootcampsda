package zaawansowana.palindrom;
/*
Interfejs daje możliwość szybkiej zmiany implementacji.
mogę nie czekać na to aż jakiś zespół faktycznie zmieni, tylko zasymulować nowe działanie?
 */
public class FakePalindrom implements InterfacePalindrom {
    @Override
    public boolean isPalindrom() {
        return true;
    }
}
