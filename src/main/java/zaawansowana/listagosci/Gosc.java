package zaawansowana.listagosci;

import java.util.Arrays;

public class Gosc {
    private String[] listaGosci;
    private String imie;
    private int counter;

    public Gosc(int ilGosci) {
        this.listaGosci = new String[ilGosci];
    }

    public boolean isListaGosciPelna() {
        return counter >= listaGosci.length;
    }


    public void dodajGoscia(String imie) {
        if (counter >= listaGosci.length) {
            System.out.println("Liczba gości pełna. " + imie + " nie wejdzie.");
            return;
        }
        for (int i = 0; i < listaGosci.length; i++) {
            if (imie.equals(listaGosci[i])) {
                System.out.println(imie + " już jest na liście. Nie wejdzie.");
                return;
            }
        }
        listaGosci[counter++]=imie;
        //  counter++;
    }


    public void usunGoscia(String imie) {

    }

    public String getListaGosci() {
        return Arrays.toString(listaGosci);
    }

}
