package zaawansowana.listagosci;

import java.util.Arrays;

public class GuestRepository {

    private String[] repository;
    private int index = 0;
//    private int index;

    public GuestRepository(int size) {
        repository = new String[size];
    }

    public void addGuest(String guest) {
        guest = guest.trim();
        if (guest.isEmpty()) {
            System.out.println("Niepoprawne dane");
            return;
        }
        if (index >= repository.length) {
            System.out.println("Lista gosci pelna");
            return;
        }
        if (isListContainsGuest(guest)) {
            System.out.println("Gosc juz jest na liscie");
            return;
        }
        repository[index++] = guest;
    }

    public boolean isListFull() {
        return index >= repository.length;
    }

    private boolean isListContainsGuest(String guest) {
        for (String tmpGuest : repository) {
            if (guest.equalsIgnoreCase(tmpGuest))
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return Arrays.toString(repository);
    }
}