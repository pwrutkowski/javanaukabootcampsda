package zaawansowana.listagosci;

import java.util.Scanner;

public class GoscApp {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj liczbę gości: ");
        //int ilGosci = Integer.parseInt(input.nextLine());

        int ilGosci = 5;
        Gosc nowaLista = new Gosc(ilGosci);
/*
        while (!nowaLista.isListaGosciPelna()) {
            System.out.println("Podaj imię gościa: ");
            String imie = input.nextLine();
            nowaLista.dodajGoscia(imie);
        }
*/
        nowaLista.dodajGoscia("Kamil");
        nowaLista.dodajGoscia("Mariusz");
        nowaLista.dodajGoscia("Zofia");
        nowaLista.dodajGoscia("Agnieszka");
        nowaLista.dodajGoscia("Agnieszka");
        nowaLista.dodajGoscia("Józef");
        nowaLista.dodajGoscia("Grażyna");

        System.out.println(nowaLista.getListaGosci());
        System.out.println(nowaLista.isListaGosciPelna());
    }
}
