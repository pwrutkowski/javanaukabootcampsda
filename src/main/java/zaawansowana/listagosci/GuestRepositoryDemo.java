package zaawansowana.listagosci;

import java.util.Scanner;

public class GuestRepositoryDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        GuestRepository x = new GuestRepository(3);
        while (!x.isListFull()) {
            System.out.println("Podaj imie goscia");
            x.addGuest(scanner.nextLine());
        }
        System.out.println("Lista gosci");
        System.out.println(x);
    }
}