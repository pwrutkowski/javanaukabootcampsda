package zaawansowana.child;

public class Child {

    private String name;
    private String surname;

    public Child(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "Child{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}