package zaawansowana.child;

public class ChildApp {

    public static void main(String[] args) {

        Child john = new Child("John", "Rambo");
        ChildRepository repository = new ChildRepository(john);
        System.out.println(repository.toString());

        john.setSurname("Terminator");
        System.out.println(repository.toString());

        //jeśli usuniemy toString...
        // Domyslna reprezentacja zwraca getClass().getName() + "@" + Integer.toHexString(hashCode());
    }

}