package zaawansowana.child;

public class ChildRepository {

    private Child child;

    public ChildRepository(Child child) {
        this.child = child;
    }

    public Child getChild() {
        return child;
    }

    @Override
    public String toString() {
        return child.toString();
    }
}