package zaawansowana.child;

public class ChildApp2 {

    public static void main(String[] args) {

        Child john = new Child("John", "Rambo");
        ChildRepository repository = new ChildRepository(john);
        System.out.println(repository.toString());

        Child johnCopy = john;
//        johnCopy.setSurname("Terminator");

        Child[] array = new Child[1];
        array[0] = johnCopy;
        array[0].setSurname("Psikus");

        System.out.println(repository.toString());
    }
}