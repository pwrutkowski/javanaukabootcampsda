package zaawansowana.calculator;
                        /*
                         Na podstawie metody add(int a, int b) zaimplementuj kolejne metody:
                        1. int subtract(int a, int b) - odejmowanie: a - b
                        2. int multiply(int a, int b) - mnozenie: a * b
                        3. double divide(int a, int b) - dzielenie: a / b
                        4. boolean isPositive(int a) - sprawdza, czy liczba jest dodatnia
                        5. boolean isNegative(int a) - sprawdza, czy liczba jest ujemna
                        6. boolean isOdd(int a) - sprawdza, czy liczba jest nieparzysta
                        7. int min(int a, int b) - zwraca mniejsza z liczb
                        8. int max(int a, int b) - zwraca wieksza z liczb
                        9. double average(int a, int b) - zwraca srednia z liczb
                        10. int power(int a, int x) - zwraca a do potegi x
                          Na podstawie powyzszych metod, zaimplementuj 3 argumentowe wersje tych metod:
                        1. int add(int a, int b, int c)
                        2. int subtract(int a, int b, int c)
                        3. int multiply(int a, int b, int c)
                        4. double divide(int a, int b, int c)
        5. int min(int a, int b, int c)
        6. int max(int a, int b, int c)
                        7. double average(int a, int b, int c)
                         */

public class Calculator {

    public int add(int a, int b) {
        return a + b;
    }

    public int add(int a, int b, int c) {
        return a + b + c;
    }

    public int subtract(int a, int b) {
        return a - b;
    }

    public int subtract(int a, int b, int c) {
        return a - b - c;
    }

    public int multiply(int a, int b) {
        return a * b;
    }

    public int multiply(int a, int b, int c) {
        return a * b * c;
    }

    public double divide(int a, int b) {
        return 1.0 * a / b;
    }

    public double divide(int a, int b, int c) {
        return (1.0 * (a / b) ) / c;
    }

    public boolean isPositive(int a) {
        return a > 0;
    }

    public boolean isNegative(int a) {
        return a < 0;
    }

    public boolean isOdd(int a) {
        return a%2!=0;
    }
    public int min(int a, int b) {
        if (a > b) {
            return b;
        } else if (a < b) {
            return a;
        } else {
            System.out.println("Liczby równe lub inny błąd.");
            return b;
        }
    }
/*
    public int min(int a, int b, int c) {
    }

 */

    public int max(int a, int b) {
        if (a > b) {
            return a;
        } else if (a < b) {
            return b;
        } else {
            System.out.println("Liczby równe lub inny błąd.");
            return b;
        }
    }
/*
    public int max(int a, int b, int c) {

    }

 */

    public double average(int a, int b) {
        return (a+b)/2.0;
    }

    public double average(int a, int b, int c) {
        return (a+b+c)/3.0;
    }

    int power(int a, int x) {
        return (int) Math.pow(a, x);
    }


}
