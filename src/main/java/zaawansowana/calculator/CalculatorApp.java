package zaawansowana.calculator;

public class CalculatorApp {
    public static void main(String[] args) {

        Calculator calc = new Calculator();
        System.out.println("Add 5 + 12: " + calc.add(5, 12));
        System.out.println("Add 5 + 12 & -7: " + calc.add(5, 12, -7));
        System.out.println("Subtract 12-5: " + calc.subtract(12, 5));
        System.out.println("Subtract 12-5-6: " + calc.subtract(12, 5, 6));
        System.out.println("Multiply 5x12: " + calc.multiply(5, 12));
        System.out.println("Multiply 5x2x3: " + calc.multiply(5, 2, 3));
        System.out.println("Divide 10/5: " + calc.divide(10, 4));
        System.out.println("Divide (20/2)/5: " + calc.divide(20, 2, 5));
        System.out.println("Is -5 negative: " + calc.isNegative(-5));
        System.out.println("Is +5 negative: " + calc.isNegative(5));
        System.out.println("Is -5 positive: " + calc.isPositive(-5));
        System.out.println("Is +5 positive: " + calc.isPositive(5));
        System.out.println("Is +5 odd: " + calc.isOdd(5));
        System.out.println("Is -4 odd: " + calc.isOdd(-4));
        System.out.println("Min z 1 i 2: " + calc.min(1,2));
        System.out.println("Max z 1 i 2: " + calc.max(1,2));
        System.out.println("Srednia z 1 i 2: " + calc.average(1,2));
        System.out.println("Srednia z 1 i 2 i 3: " + calc.average(1,2, 3));
        System.out.println("2 do 2 potęgi: " + calc.power(2,3));
        
    }
}
