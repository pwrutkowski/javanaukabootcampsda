package zaawansowana.Punkt2D;

public class Punkt2DApp {
    public static void main(String[] args) {
        Punkt2D punkt1 = new Punkt2D(3, 1);
        Punkt2D punkt2 = new Punkt2D(2, 4);

        System.out.println(punkt1.sectionLength(punkt2));
        System.out.println(Punkt2D.sectionLength(punkt1, punkt2));
    }
}
