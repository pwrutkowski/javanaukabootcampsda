package zaawansowana.Punkt2D;

public class Punkt2D {
    private double x;
    private double y;

    public Punkt2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double sectionLength(Punkt2D point) {
        double x = point.getX() - this.getX();   //mogłoby być this.x
        double y = point.getY() - this.getY();
        double dl = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        return dl;
    }

    public static double sectionLength(Punkt2D point1, Punkt2D point2) {
        double x = point1.getX() - point2.getX();
        double y = point1.getY() - point2.getY();
        double dl = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        return dl;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

}
