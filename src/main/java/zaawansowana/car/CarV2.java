package zaawansowana.car;

public class CarV2 {

    private String vin;
    private String model;
    private int serialNumber;

    public static  int counter = 0; // zlicza liczbę utworzonych obiektów

    public CarV2(String vin, String model) {
        this.vin = vin;
        this.model = model;
        this.serialNumber = ++counter;
    }

    public String getVin() {
        return vin;
    }

    public String getModel() {
        return model;
    }

    public int getCarNumber() {
        return serialNumber;
    }
}
