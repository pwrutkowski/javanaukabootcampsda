package zaawansowana.car;

public class Car {
    private String vin;
    private String model;

    public static  int counter = 0; // zlicza liczbe utworzonych obiektow klasy Car

    public Car(String vin, String model) {
        this.vin = vin;
        this.model = model;
        counter++;
    }

    public String getVin() {
        return vin;
    }

    public String getModel() {
        return model;
    }
}
