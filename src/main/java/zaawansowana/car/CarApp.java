package zaawansowana.car;

public class CarApp {
    public static void main(String[] args) {

        System.out.println("Counter " + Car.counter);
        Car car1 = new Car("f9asda", "Mercedes");
        System.out.println("Counter " + Car.counter);
        Car car2 = new Car("assa", "Audi");
        System.out.println("Counter " + Car.counter);
        Car car3 = new Car("ttgtg", "BMW");
        System.out.println("Counter " + Car.counter);

        //Generalnie nie odwolujemy sie do zmiennych statycznych poprzez obiekt tak jak ponizej
        //do zmiennej statycznej odwolujemy się przez klasę System.out.println(Car.counter);
        System.out.println("Counter Merc" + car1.counter);
        System.out.println("Counter BMW" + car3.counter);
    }
}
