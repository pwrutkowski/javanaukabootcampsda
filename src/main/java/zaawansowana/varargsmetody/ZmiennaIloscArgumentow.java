package zaawansowana.varargsmetody;
//varargs
public class ZmiennaIloscArgumentow {

    void printArgs(int firstArg, int... numbers) {
        System.out.println("Argument stały: " + firstArg);
        for (int i = 0; i < numbers.length; i++) {
            System.out.println("Argument ze zmiennej grupy: " + numbers[i]);
        }
    }

    public static void main(String[] args) {
       ZmiennaIloscArgumentow varvar = new ZmiennaIloscArgumentow();

        varvar.printArgs(5, 4);

        System.out.println();

        varvar.printArgs(5, 4, 5, 6, 456);
    }
}
