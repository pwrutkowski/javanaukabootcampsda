package zaawansowana.listysortowanie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class App {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("Ziutek");
        names.add("Franek");
        names.add("Ania");
        //poniżej bazowy przypadek sortowania, wymaga imlementacji interfejsu Comparable
        Collections.sort(names);
        System.out.println(names);

        List<Child> children = new ArrayList<>();
        children.add(new Child("Ziutek", 15));
        children.add(new Child("Franek", 60));
        children.add(new Child("Franek", 1));
        children.add(new Child("Ania",99));

        System.out.println();
        System.out.println(children);
        Collections.sort(children);

        System.out.println();
        System.out.println(children);

        //tutaj coś było

        Comparator<Child> ageComparator = new ChildrenAgeComparator();
        Collections.sort(children, ageComparator);
        System.out.println(children);

        System.out.println();
        Collections.sort(children, new ChildNameReverseComparator());   //można tak to wywołać
        //children.sort(new ChildNameReverseComparator());              //albo tak wywołać
        System.out.println(children);


        System.out.println();
        Collections.sort(children, new ComparatorTurboName());
        System.out.println(children);

//        można też chainować

        Comparator<Child> nameReverseComparator = new ChildNameReverseComparator();
        Comparator<Child> turboComparator = new ComparatorTurboName();

        Comparator<Child> ageageComparator = new ChildrenAgeComparator();
        Collections.sort(children, nameReverseComparator.thenComparing(turboComparator));


    }
}
