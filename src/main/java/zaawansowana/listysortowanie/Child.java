package zaawansowana.listysortowanie;


//Comparabele implementuje na obiekcie, który chcemy aby się sortował w liście, bez przekazywania comparatora
public class Child implements Comparable<Child> {
    private String name;
    private int age;

    public Child(String name, int age) {
        this.name = name;
        this.age = age;

    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Child{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Child o) {
        return this.name.compareTo(o.getName());
       // int result = o.getName().compareTo(this.name);
      //  int result = this.age.compareTo(o.getAge());
     //   return result;
    }
//    public int compareAge(Child o) {
//        return this.name.compareTo(o.getName());
//    }

}
