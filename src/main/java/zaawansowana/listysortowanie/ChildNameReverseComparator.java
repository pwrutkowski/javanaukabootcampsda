package zaawansowana.listysortowanie;

import java.util.Comparator;

public class ChildNameReverseComparator implements Comparator<Child> {
   //chcemy od z do a, dlatego najpierw jest o2  a potem o1
    //dla od a do z, byłoby o1 i potem o2

    @Override
    public int compare(Child o1, Child o2) {
        int result = o2.getName().compareTo(o1.getName());
        return result;
    }
}
