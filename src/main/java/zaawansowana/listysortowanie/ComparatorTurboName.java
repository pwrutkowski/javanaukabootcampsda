package zaawansowana.listysortowanie;

import java.util.Comparator;

public class ComparatorTurboName implements Comparator<Child> {
    @Override
    public int compare(Child o1, Child o2) {
        int result = o1.getName().compareTo(o2.getName());
        if (result == 0) {
            result = Integer.compare(o1.getAge(), o2.getAge());
        }
        return result;
    }
}
