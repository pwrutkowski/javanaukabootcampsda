package zaawansowana.listysortowanie;

import java.util.Comparator;

public class ChildrenAgeComparator implements Comparator<Child> {
    @Override
    public int compare(Child o1, Child o2) {
        int result = Integer.compare(o1.getAge(), o2.getAge());
        return result;
    }
}
