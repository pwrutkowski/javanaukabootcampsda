package zaawansowana.hashmapKASAfiskalna;

public interface IKasa {
    void addProductToBase(Produkt p);
    void removeProductFromBase(String kod);
    void addProductToBill(String kod);
    void removeProductFromBill(String kod);
    double closeBillAndSum();

}
