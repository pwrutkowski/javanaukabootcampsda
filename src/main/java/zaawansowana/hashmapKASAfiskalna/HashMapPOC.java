package zaawansowana.hashmapKASAfiskalna;

import java.util.HashMap;
import java.util.Map;

public class HashMapPOC {
    public static void main(String[] args) {
        //mapowanie klucz - wartosc
        Map<String, String> map = new HashMap<>();
        map.put("Dzik", "Jan Kowalski");
        map.put("Tiger", "Tomasz Kowalski");
        map.put("Lampart", "Tomasz Kowalski");
        //przypisze nową wartość do klucza
        map.put("Dzik", "John Rambo");

        System.out.println(map);

        //Podobno przyporządkowanie adresu www do IP jest hashmapowe
        //barcode --> cena
        //szybki dostęp

        map.remove("Dzik");
        System.out.println(map);
        System.out.println(map.get("Tiger"));
        System.out.println(map.get("Dzik"));

        System.out.println("po kluczach");
        for (String x : map.keySet()) {
            System.out.println(x);
        }
        System.out.println("po całych wpisach");
        for (Map.Entry<String, String> x : map.entrySet()){
            System.out.println((x.getKey() + " " + x.getValue()));
        }
    }
}
