package zaawansowana.hashmapKASAfiskalna;

public class KasjerApp {
    public static void main(String[] args) {
        Produkt product1 = new Produkt("1234", 5.50,"herbata");
        Produkt product2 = new Produkt( "54413", 22.50,"kawa");
        Produkt product3 = new Produkt( "345345", 2.00,"mleko");
        Produkt product4 = new Produkt( "99345", 20.00,"fajki");

        IKasa kasa = new Kasa();
        kasa.addProductToBase(product1);
        kasa.addProductToBase(product2);
        kasa.addProductToBase(product3);
        kasa.addProductToBase(product4);
        kasa.removeProductFromBase(product4.getKod());

        kasa.addProductToBill(product1.getKod());
        kasa.addProductToBill(product2.getKod());
        kasa.addProductToBill(product2.getKod());
        kasa.addProductToBill(product3.getKod());
        kasa.removeProductFromBill(product3.getKod());
        System.out.println("Suma: " + kasa.closeBillAndSum());


    }
}
