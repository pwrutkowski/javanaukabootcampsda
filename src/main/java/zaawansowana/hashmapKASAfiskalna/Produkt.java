package zaawansowana.hashmapKASAfiskalna;
/*
Utworz progam uproszczona kase sklepowa
Mamy klase produktu, ktora zawiera 3 pola

program ma kilka opcji
dodaj produkt do listy produktow
usun produkt z listy produktow
oblicz cene zakupow, program bedzie pytal o kod kolejnych produktow do momentu zamkniecia paragonu i wyswietli
podsumowanie paragonu. Postaraj sie skorzystac z zalet obiektowosci i interfejsow
 */
public class Produkt {
    private String kod;
    private Double cena;
    private String nazwa;

    public Produkt(String kod, Double cena, String nazwa) {
        this.kod = kod;
        this.cena = cena;
        this.nazwa = nazwa;
    }

    public String getKod() {
        return kod;
    }

    public Double getCena() {
        return cena;
    }

    public String getNazwa() {
        return nazwa;
    }

    @Override
    public String toString() {
        return "Produkt{" +
                "kod='" + kod + '\'' +
                ", cena=" + cena +
                ", nazwa='" + nazwa + '\'' +
                '}';
    }
}
