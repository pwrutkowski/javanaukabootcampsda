package zaawansowana.hashmapKASAfiskalna;

/*
Utworz progam uproszczona kase sklepowa
Mamy klase produktu, ktora zawiera 3 pola
nazwa produktu
kod produktu
cene produktu
program ma kilka opcji
dodaj produkt do listy produktow
usun produkt z listy produktow
oblicz cene zakupow, program bedzie pytal o kod kolejnych produktow do momentu zamkniecia paragonu i wyswietli
podsumowanie paragonu. Postaraj sie skorzystac z zalet obiektowosci i interfejsow
 */

import java.util.*;

public class Kasa implements IKasa {

    private Map<String, Produkt> dataBase = new HashMap<>();
    private List<String> billSummary = new ArrayList<>();
    private double sum;

    @Override
    public void addProductToBase(Produkt p) {
        dataBase.put(p.getKod(), p);
        System.out.println("Baza danych wyglada teraz tak: " + dataBase);
    }

    @Override
    public void removeProductFromBase(String kod) {
        dataBase.remove(kod);
        System.out.println("Baza danych wyglada teraz tak: " + dataBase);
    }

    @Override
    public void addProductToBill(String kod) {
        billSummary.add(kod);
    //    sum +=dataBase.get(kod).getCena();
    //    System.out.println(billSummary);
     //   System.out.println("Rachunek po dodaniu: " + sum);
    }

    @Override
    public void removeProductFromBill(String kod) {
        billSummary.remove(kod);
       // sum -=dataBase.get(kod).getCena();
      //  System.out.println("Rachunek po odjęciu: " + sum);
    }

    @Override
    public double closeBillAndSum() {
        for (String eachkod : billSummary) {
            sum += (dataBase.get(eachkod)).getCena();
        }
        return sum;
    }

    @Override
    public String toString() {
        return "Kasa{" +
                "dataBase=" + dataBase +
                ", bill=" + sum +
                '}';
    }
}
