package zaawansowana.wyjatki;

public class WyjatkiApp {

    //pokazuje w jaki sposób możemy wykorzystać mechanizm obsługi wyjątków w celu uzyskania poprawnosci danych
    public static void main(String[] args) throws IllegalSurnameArgumentException {  //checked exception wymusza deklarację w nazwie metody, że może nastąpić
        Child child = new Child("John", "Rambo");
        System.out.println(child);
        try {
            child.setSurname("psikus1");
        } catch (IllegalArgumentException exc) {
            exc.printStackTrace();
            System.out.println("Uwaga, wystapił wyjątek!");
            //przykładem prawidłowej obsługi błędu byłoby wyświetlenie w tym miejscu popup'a z informacją o wprowadzaeniu błędnych wartości
            //i poprosić o ponowne podanie wartości jeśli pracuje z użytkownikiem przez interfejs graficzny

            // catch powininen zawierać obsługę błędu. nie powinno się zostawiać pustego catcha

            //zazwyczaj logujemy wystapienie wyjątku z informacją o argumentach ktore mogly spowodowac blad

        }
        System.out.println(child);

        System.out.println("\n");
        Integer integer = null;
        try {
            integer = Integer.parseInt("sdfsdf");
        } catch (IllegalArgumentException exc) {
            exc.printStackTrace();
            System.out.println("Uwaga, wystapił wyjątek!");
            //przykładem prawidłowej obsługi błędu byłoby wyświetlenie w tym miejscu popup'a z informacją o wprowadzaeniu błędnych wartości
            //i poprosić o ponowne podanie wartości jeśli pracuje z użytkownikiem przez interfejs graficzny

            // catch powininen zawierać obsługę błędu. ni powinno się zostawiać pustego catcha
        }

        System.out.println();
        System.out.println("Wyjątki checked");
        System.out.println();

        Child child2 = new Child("John", "Rambo");
        System.out.println(child);
        try {
            child2.setSurnameCheckedException("psikus1");
        } catch (IllegalSurnameArgumentException ex) {
            ex.getMessage();
        }
        System.out.println(child);
    }
}
