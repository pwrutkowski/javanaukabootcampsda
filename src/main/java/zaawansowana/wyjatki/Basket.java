package zaawansowana.wyjatki;

/*
Stwórz klase Basket, ktora imituje koszyk i przechowuje aktualna ilosc elementow w koszyku.
Dodaj metode addToBasket(), ktora dodaje element do koszyka (zwiekszajac aktualny stan o 1 ) oraz
metode removeFromBasket(), ktora usuwa element z koszyka (zmniejszajac aktualny stan o 1
Koszyk może przechowywac od 0 do 10 elementow.
W przypadku, kiedy uxytkownik chce wykonac akcje usuniecia przy stanie 0 lub dodania przy stanie 10 ,
rzuc odpowiedni runtime exception
BasketFullException lub BasketEmptyException.

Zamień wyjątki BasketFullException oraz BasketEmptyException z poprzedniego zadania na wyjątki typu checked exception. Obsłuż je.
 */
public class Basket {
    private int ilEl;

    public Basket() {
    }

    public void addToBasket() {

        if (ilEl >= 10) {
            throw new BasketFullException();
        } else {
            ilEl++;
        }
    }

    public void tryToAdd() {
        try {
            addToBasket();
        } catch (BasketFullException e) {
            System.out.println("Błąd!" + e.getMessage());
        }
    }

    public void removeFromBasket() {
        if (ilEl <= 0) {
            throw new BasketEmptyException();
        } else {
            ilEl--;
        }
    }

    public void tryToRemove() {
        try {
            removeFromBasket();
        } catch (BasketEmptyException r) {
            r.getMessage();
            System.out.println("Błąd!" + r.getMessage());
        }
    }

    @Override
    public String toString() {
        return "Basket{" +
                "ilEl=" + ilEl +
                '}';
    }

    public static void main(String[] args) {
        Basket basket = new Basket();

        System.out.println(basket);
        basket.tryToAdd();
        System.out.println(basket);
        basket.tryToAdd();
        System.out.println(basket);
        basket.tryToRemove();
        System.out.println(basket);
        basket.tryToRemove();
        System.out.println(basket);
        basket.tryToRemove();
        System.out.println(basket);
        basket.tryToAdd();
        basket.tryToAdd();
        basket.tryToAdd();
        basket.tryToAdd();
        basket.tryToAdd();
        basket.tryToAdd();
        basket.tryToAdd();
        basket.tryToAdd();
        System.out.println(basket);
        basket.tryToAdd();
        System.out.println(basket);
        basket.tryToAdd();
        basket.tryToAdd();
        System.out.println(basket);

    }
}
