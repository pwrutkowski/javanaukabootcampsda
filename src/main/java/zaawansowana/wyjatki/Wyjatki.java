package zaawansowana.wyjatki;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Wyjatki {

    public static void main(String[] args) {
        System.out.println("Podaj cyfrę");
        Scanner scan = new Scanner(System.in);
        int number;
        //bazowy przypadek obsługi błędu unchecked
        try {
            number = scan.nextInt();
            //Poniżej podajemy klasę wyjątku który będziemy obsługiwać
            //to oznacza w uproszeczeniu, że jeśli wystąpi inny wyjątek może nie zostać obsłużony
            //w catch nie piszemy Exception, bo wtedy przechwycimy wszystkie Exception
            //my chcemy przechwytywać konretne wyjątki i podejmować konretne akcje
        } catch (InputMismatchException e) {
            System.out.println("W tym miejscu przechwytuję wyjątek ");
            number = -1;
        } finally {
            System.out.println("Ten blok jest opcjonalny. Ale jeśli jest to wykona się za każdym razem");
        }
        System.out.println("Podałeś cyfrę " + number);
    }
}
