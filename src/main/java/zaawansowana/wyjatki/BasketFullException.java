package zaawansowana.wyjatki;

public class BasketFullException extends RuntimeException {
    public BasketFullException() {
        super("Koszyk pełny!");
    }
}
