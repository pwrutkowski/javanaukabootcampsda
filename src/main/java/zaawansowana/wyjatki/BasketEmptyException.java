package zaawansowana.wyjatki;

public class BasketEmptyException extends RuntimeException {
    public BasketEmptyException() {
        super("Koszyk pusty!");
    }
}
