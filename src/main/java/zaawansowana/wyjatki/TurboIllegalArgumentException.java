package zaawansowana.wyjatki;

public class TurboIllegalArgumentException extends IllegalArgumentException{
    public TurboIllegalArgumentException(String s) {
        super(s);
    }
        //wyjątek unchecked
    //Skąd wiedzieć którą klasę wyjątku rozszerzyć: Taką, której nazwa będzie mówiła o tym co się stało
}
