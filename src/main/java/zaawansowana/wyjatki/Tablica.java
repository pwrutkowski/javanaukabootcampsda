package zaawansowana.wyjatki;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Tablica {

    public int wczytajLiczbeScannerem() {
        Scanner scan = new Scanner(System.in);
        int number=0;
        try {
            number = scan.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Błąd, nie podałeś cyfry.");
        }
        return number;
    }

    public void czytajDopokiDobra() {

    }

    public static void main(String[] args) {
        int rozmiar = 0;
        Tablica obsluga = new Tablica();

        do {
            System.out.println("Podaj rozmiar tablicy");
            rozmiar = obsluga.wczytajLiczbeScannerem();
        } while (rozmiar <= 0);

        int[] tablica = new int[rozmiar];


        Scanner scan = new Scanner(System.in);
        for (int i = 0; i < tablica.length; i++) {
            System.out.println("Podaj cyfrę do tablicy");
            try {
                int number = scan.nextInt();

                tablica[i] = number;
            } catch (InputMismatchException e) {
                System.out.println("Błąd, nie podałeś cyfry.");
                scan.skip("((?<!\\R)\\s)*");

                //albo tak: System.out.println("bufor zawiera: " + scan.nextLine());
                i--;    //i powtarzamy tą iterację pętli
            }
        }
        System.out.println(Arrays.toString(tablica));


    }
}
