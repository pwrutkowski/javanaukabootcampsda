package zaawansowana.wyjatki;

public class Child {
    private String name;
    private String surname;

    public Child(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public void setSurname(String surname) {
        if (!isSurnameValid(surname)) {
         //   throw new IllegalArgumentException("Zły argument " + surname);
            throw new TurboIllegalArgumentException("Zły argument " + surname);
        }
        this.surname = surname;
    }

    public void setSurnameCheckedException(String surname) throws IllegalSurnameArgumentException {
        if (!isSurnameValid(surname)) {
            throw new IllegalSurnameArgumentException("Zły argument " + surname);
        }
        this.surname = surname;
    }
    //metoda, ktora zwraca false, jesli surname zawiera cyfre, inaczeh zwraca true
    //czyli waliduje imie pod katem tego, ze nie moze zawierac cyfry
    private boolean isSurnameValid(String surname) {
        for (int i = 0; i < surname.length(); i++) {
            if (Character.isDigit(surname.charAt(i))) {// jesli zawiera cyfrę to zwraca false - walidacja nazwiska pod kątem zawartości cyfr
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "Child{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
