package zaawansowana.set;

import java.util.*;

public class SetExample {

    public static void main(String[] args) {

        //Set przechowuje unikalne wartości i nie możemy odwołać się do elementu po indeksie. nie możemy sortować seta
        Set<String> names = new HashSet<>();
        names.add("Franek");
        names.add("Roman");
        names.add("Roman");

        System.out.println("Czy jest z nami Roman " + names.contains("Roman"));

        Iterator<String> iterator = names.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        for (String name : names) {
            System.out.println(name);
        }

        List<String> cars = new ArrayList<>();
        cars.add("opel");
        cars.add("opel");
        cars.add("mercedes");

        Set<String> carsSet = new HashSet<>(cars);
        cars = new ArrayList<>(carsSet);
        System.out.println(cars);


    }
}
