package zaawansowana.listadzieciwklasie;

import java.util.ArrayList;
import java.util.List;

public class ChildServiceSingletonLogger {

    private static ChildServiceSingletonLogger instance;

    private ChildServiceSingletonLogger() {
    }

    private List<String> logs = new ArrayList<>();

    public static ChildServiceSingletonLogger getInstance() {
        if (instance == null) {
            instance = new ChildServiceSingletonLogger();
        }
        return instance;
    }

    public void log(String logEntry) {
        logs.add(logEntry);
    }

    public String getLogs() {
        return logs.toString();
    }

    public void listLogs() {
        for (String each : logs) {
            System.out.println(each);
        }
    }

}

