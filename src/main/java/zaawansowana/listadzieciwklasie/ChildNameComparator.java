package zaawansowana.listadzieciwklasie;

import java.util.Comparator;

public class ChildNameComparator implements Comparator<Child> {


    @Override
    public int compare(Child o1, Child o2) {
        int result = o1.getName().compareTo(o2.getName());
        return result;
    }
}
