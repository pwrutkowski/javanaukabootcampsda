package zaawansowana.listadzieciwklasie;

import zaawansowana.wzorceprojektowe.SingletonLogger;

public class ChildApp {


    public static void main(String[] args) {


        IChildService childService = new ChildService();

        childService.addChildToList(new Child("Krzysztof", "10221000055"));
        childService.addChildToList(new Child("Zuzanna","10231307045"));
        childService.addChildToList(new Child("Julia","10250930034"));
        childService.addChildToList(new Child("Antoni","10262900123"));
        childService.addChildToList(new Child("Marta","10242600330"));
        childService.addChildToList(new Child("Stanisław","10212107043"));
        childService.addChildToList(new Child("","30212107043"));
        childService.addChildToList(new Child("Stanisław","10212107043"));
        childService.addChildToList(new Child("Stanisław","30212107043"));

        System.out.println("Sortuj po peselu");
        childService.sortPesel();

        childService.listChildList();

        System.out.println("Sortuj po imieniu");
        childService.sortChildList();

        childService.listChildList();

        System.out.println("\nUsuwamy duplikaty, nadpisujemy listę");
        childService.removeDuplicates();

        childService.listChildList();

        System.out.println("\nMenu");
        childService.menuChoice();
        System.out.println("\nPo wszystkim lista wygląda tak:");
        childService.listChildList();

        //System.out.println(ChildServiceSingletonLogger.getInstance().getLogs());
        ChildServiceSingletonLogger.getInstance().listLogs();
    }
}
