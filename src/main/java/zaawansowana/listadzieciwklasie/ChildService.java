package zaawansowana.listadzieciwklasie;

import java.util.*;
import java.util.regex.Pattern;

public class ChildService implements IChildService {
    private List<Child> childList;

    public ChildService() {
        childList = new ArrayList<>();
    }

    @Override
    public boolean addChildToList(Child ob) {
        if (isNameValid(ob.getName()) || isAddPESELValid(ob.getPesel())) {
            ChildServiceSingletonLogger.getInstance().log("Add Child demand accepted: name=\"" + ob.getName() + "\", pesel=\"" + ob.getPesel() + "\"");
            return childList.add(ob);
        }
        ChildServiceSingletonLogger.getInstance().log("Add Child demand rejected: name(" + isNameValid(ob.getName()) + ")=\"" + ob.getName() + "\", pesel(" + isAddPESELValid(ob.getPesel()) + ")=\"" + ob.getPesel() + "\"");
        return false;
    }

    private boolean isNameValid(String name) {
        if (name == null || name.isBlank()) {
            return false;
        }
        Pattern pattern = Pattern.compile("[a-z A-Z]+");
        return pattern.matcher(name).matches();
    }

    private boolean isAddPESELValid(String pesel) {
        if (pesel == null || pesel.isBlank()) {
            return false;
        }
        for (Child each : childList) {
            if (each.getPesel() == pesel) {
                return false;
            }
        }
        Pattern pattern = Pattern.compile("[0-9]+");
        return pattern.matcher(pesel).matches();
    }

    public void removeChildFromList(Child ob) {
        if (isNameValid(ob.getName())) {
            ChildServiceSingletonLogger.getInstance().log("Remove Child demand accepted: name=\"" + ob.getName() + "\", pesel=\"" + ob.getPesel() + "\"");
            childList.remove(ob);
        }
        ChildServiceSingletonLogger.getInstance().log("Remove Child demand rejected: name=\"" + ob.getName() + "\", pesel=\"" + ob.getPesel() + "\"");

    }

    @Override
    public void listChildList() {
      //  System.out.println(childList);
        childList.forEach(each -> System.out.println(each));
    }

    @Override
    public String getChildrenList() {
        return childList.toString();
    }

    @Override
    public void sortChildList() {
        ChildServiceSingletonLogger.getInstance().log("Sort Child list by name command");
//        Collections.sort(childList, new ChildNameComparator());
        Collections.sort(childList, (c1, c2) -> c1.getName().compareTo(c2.getName()));
    }

    @Override
    public void sortPesel() {
        ChildServiceSingletonLogger.getInstance().log("Sort Child list by PESEL command");
//        Collections.sort(childList, new ChildPeselComparator());
        Collections.sort(childList, (c1, c2) -> Double.compare(Double.valueOf(c1.getPesel()), Double.valueOf(c2.getPesel())));
    }

    @Override
    public void removeDuplicates() {
        ChildServiceSingletonLogger.getInstance().log("Remove Duplicates command");
        Set<Child> listaSet = new HashSet<>(childList);
        childList = new ArrayList<>(listaSet);
        //childList = new ArrayList<>(new HashSet<>(childList)); //tak by było jednolinijkowo
        sortChildList();
    }

    @Override
    public void setNamesToUpperCase() {
        ChildServiceSingletonLogger.getInstance().log("To uppercase command");
        for (Child bebka : childList) {
            bebka.setName(bebka.getName().toUpperCase());
        }
     //   childList.forEach(each -> each.setName(each.getName().toUpperCase()));
    }

    @Override
    public void setNamesToLowerCase() {
        ChildServiceSingletonLogger.getInstance().log("To lowercase command");
        for (Child bebka : childList) {
            bebka.setName(bebka.getName().toLowerCase());
        }
        //   childList.forEach(each -> each.setName(each.getName().toLowerCase()));
    }

    @Override
    public void menuChoice() {
        int wybor = scanChoice();
        switch (wybor) {
            case 1:
                String[] dane = scanName();
                addChildToList(new Child(dane[0], dane[1]));
                break;
            case 2:
                listChildList();
                break;
            case 3:
                sortChildList();
                break;
            case 4:
                removeDuplicates();
                break;
            case 5:
                setNamesToUpperCase();
                break;
            case 6:
                setNamesToLowerCase();
                break;
            case 7:
                String[] daneUsu = scanName();
                removeChildFromList(new Child(daneUsu[0], daneUsu[1]));
                break;
            default:
                System.out.println("Nieprawidłowy wybór");
                ChildServiceSingletonLogger.getInstance().log("Illegal Menu entry: " + wybor);
        }
    }

    public int scanChoice() {
        String menuString = "Wybierz co chcesz zrobić: \n" +
                "1.Dodaj dziecko do listy\n" +
                "2.Wyswielt liste dzieci\n" +
                "3.Posortuj liste dzieci\n" +
                "4.Wyczysc z duplikatow liste dzieci\n" +
                "5.Zmien wszystkie litery imion dzieci na duze\n" +
                "6.Zmien wszystkie litery imion dzieci na male\n" +
                "7.Usuń dziecko";
        Scanner scan = new Scanner(System.in);
        while (true) {
            System.out.println();
            System.out.println(menuString);
            int wybor;
            try {
                wybor = Integer.valueOf(scan.nextLine());
                //  wybor = Integer.parseInt(scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Dokonaj wyboru wprowadzajac cyfre");
                continue;
            }
            return wybor;
        }
    }

    public String[] scanName() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj imię i pesel oddzielone spacją");
        String wczytaneLiczby = scan.nextLine();
        String[] dane = wczytaneLiczby.split("\\s+");
        return dane;
    }

    @Override
    public String toString() {
        return "ChildService{" +
                "childList=" + childList +
                '}';
    }
}
