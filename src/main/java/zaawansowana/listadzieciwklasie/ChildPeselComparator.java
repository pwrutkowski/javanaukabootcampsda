package zaawansowana.listadzieciwklasie;

import java.util.Comparator;

public class ChildPeselComparator implements Comparator<Child> {
    @Override
    public int compare(Child o1, Child o2) {
        int result = o1.getPesel().compareTo(o2.getPesel());
        return result;
    }
}
