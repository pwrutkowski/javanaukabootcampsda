package zaawansowana.listadzieciwklasie;

import java.util.Objects;

/*
        Napisz program do przechowywania listy imion dzieci, ktore chodza do klasy. Identyfikatorem dziecka bedzie imie - String.
        Program ma posiadac nastepujace funkcjonalnosci:
        -dodaj dziecko do listy
        -wyswielt liste dzieci
        -posortuj liste dzieci
        -wyczysc z duplikatow liste dzieci
        -zmien wszystkie litery imion na duze
        -zmien wszystkie litery na male
        Program ma dzialac z listy komend i komunikowac sie z uzytkownikiem przez menu wyswietlane na standardowym wyjsciu. Uzytkownik podaje numer przypisany do pozycji menu i w ten sposob wywoluje okreslona funkcjonalnosc.
            Menu:
            1.dodaj dziecko do listy
            2.wyswielt liste dzieci
            3.posortuj liste dzieci
            4.wyczysc z duplikatow liste dzieci
            5.zmien wszystkie litery imion dzieci na duze
            6.zmien wszystkie litery imion dzieci na male
            Rozwiaz zadanie uzywajac interfejsów
            o

 */
public class Child {
    private String name;
    private String pesel;

    public Child(String name, String pesel) {
        this.name = name;
        this.pesel = pesel;

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getPesel() {
        return pesel;
    }

    @Override
    public String toString() {
        return "Child{" +
                "name='" + name + '\'' +
                ", pesel='" + pesel + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Child)) return false;
        Child child = (Child) o;
        return getName().equals(child.getName()) && getPesel().equals(child.getPesel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPesel());
    }
}
