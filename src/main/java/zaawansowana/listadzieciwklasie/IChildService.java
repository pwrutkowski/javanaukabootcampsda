package zaawansowana.listadzieciwklasie;

public interface IChildService {

    boolean addChildToList(Child ob);

    void listChildList();

    String getChildrenList();

    void sortChildList();

    void sortPesel();

    void removeDuplicates();

    void setNamesToUpperCase();

    void setNamesToLowerCase();

    void menuChoice();


}
