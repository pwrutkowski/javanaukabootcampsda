package zaawansowana.printer;

public class PrinterApp {
    public static void main(String[] args) {

        String text = "PsiA DupA";  //ustalamy stringa

        PrInterface lowBlow = new PrinterLowerCase();  //nowy obiekt klasy PrintLowerCase typu PrInterface o nazwie lowBlow
        lowBlow.printText(text);                        //

        PrInterface upBlow = new PrinterUpperCase();
        upBlow.printText(text);
    }
}
