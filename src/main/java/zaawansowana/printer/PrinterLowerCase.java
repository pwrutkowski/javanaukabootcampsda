package zaawansowana.printer;

public class PrinterLowerCase implements PrInterface{

    @Override
    public void printText(String text) {
        System.out.println(text.toLowerCase());
    }
}
