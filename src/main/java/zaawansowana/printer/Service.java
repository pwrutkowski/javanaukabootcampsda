package zaawansowana.printer;

public class Service implements PrinterService {

    @Override
    public void print(PrInterface printer, String textToPrint) {
        printer.printText(textToPrint);
    }
}
