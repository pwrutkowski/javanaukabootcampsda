package zaawansowana.printer;

public class ServiceApp {
    public static void main(String[] args) {
        String text = "TeXt";
        PrinterService pserv = new Service();
        PrInterface printer = new PrinterLowerCase();

        pserv.print(printer, text);

        printer = new PrinterUpperCase();
        pserv.print(printer, text);
    }
}
