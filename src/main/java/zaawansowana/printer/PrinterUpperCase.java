package zaawansowana.printer;

public class PrinterUpperCase implements PrInterface{

    @Override
    public void printText(String text) {
        System.out.println(text.toUpperCase());
    }
}
