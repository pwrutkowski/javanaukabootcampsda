package zaawansowana.calcobiekty;

public class CalculatorStatic {
    /*
    Pola statyczne są współdzielone przez wszystkie obiekty tej klasy.
    Należą one do klasy, a nie do konkretnego obiektu.
    Słowo kluczowe static oznacza, że metoda może być wywołana na poziomie klasy,
    bez utworzenia instancji klasy.
    Metoda taka nie może się odnosić do pól niestatycznych oraz wywoływać niestatycznych metod klasy.
    Nie mamy dostępu do obiektu klasy. Metody statyczne mają dostęp jedynie do metod i pól,
    które także są statyczne.
     */

    private int element = 5;

    public static int sum(int a, int b) {
        return a + b;

        //wewnatrze metody statycznej nie mozemy sie odwolac do niestatycznych pol klasy
//        return a + b + element;
    }

    //inne przyklady klasy statycznej Math

}