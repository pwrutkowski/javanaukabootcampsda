package zaawansowana.calcobiekty;

public class CalculatorApp {

    /*
    Pola statyczne są współdzielone przez wszystkie obiekty tej klasy.
    Należą one do klasy, a nie do konkretnego obiektu.
    Słowo kluczowe static oznacza, że metoda może być wywołana na poziomie klasy,
    bez utworzenia instancji klasy.
    Metoda taka nie może się odnosić od pól niestatycznych oraz wywoływać niestatycznych metod klasy.
    Nie mamy dostępu do obiektu klasy. Metody statyczne mają dostęp jedynie do metod i pól,
    które także są statyczne.
     */
    public static void main(String[] args) {
// metoda niestatyczna wymaga obiektu, aby ja wywolac
        Calculator calculator = new Calculator();
        System.out.println(calculator.sum(2,2));

        //metode statyczna mozemy wywolac bez instancji klasy
        System.out.println(CalculatorStatic.sum(3, 3));

    }

}