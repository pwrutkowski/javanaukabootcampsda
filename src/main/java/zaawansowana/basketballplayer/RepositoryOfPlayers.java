package zaawansowana.basketballplayer;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class RepositoryOfPlayers implements IBasketRepo {

    List<BasketballPlayer> playerList = new ArrayList<>();


    @Override
    public boolean add(BasketballPlayer player) {
        return playerList.add(player);
    }

    @Override
    public void remove(String name, String surname) {
        //boolean test(T t);
        //Predicate<BasketballPlayer> playerPredicate = (BasketballPlayer player) -> player.getName().equals(name)&&player.getSurname().equals(surname);
        //playerList.removeIf(playerPredicate);
        playerList.removeIf((player) -> player.getName().equals(name)&&player.getSurname().equals(surname));

//        List<BasketballPlayer> collect = playerList.stream().filter(player -> !player.getName().equalsIgnoreCase(name)
//        && !player.getSurname().equalsIgnoreCase(surname)).collect(Collectors.toList());                                  //stream
    }

    @Override
    public void removeDuplicates() {
        playerList = new ArrayList<>(new HashSet<>(playerList));
        //playerList = playerList.stream().distinct().collect(Collectors.toList());     //stream
    }

    @Override
    public List<BasketballPlayer> getPlayers() {
      //  return new ArrayList<>(playerList);               //można dać kopię powierzchowną, nie mamy setterów, więc wartości nie zmienimy ale mozemy usunąć playerów
        return Collections.unmodifiableList(playerList);    //jest też taki myk
    }

    @Override
    public void sortByName() {
    Collections.sort(playerList);  //ale trzeba zdefiniować compareTo w Playerze
    //playerList.sort((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
    //playerList = playerList.stream().sorted().collect(Collectors.toList());       //stream
    }

    @Override
    public void sortBySurname() {
     //   Comparator<BasketballPlayer> cmp = (BasketballPlayer o1, BasketballPlayer o2) -> o1.getSurname().compareToIgnoreCase(o2.getSurname());
     //   playerList.sort(cmp);
        playerList.sort((o1, o2) -> o1.getSurname().compareToIgnoreCase(o2.getSurname()));
    }

    @Override
    public void sortByWeight() {
        //playerList.sort((o1, o2) -> Integer.compare(o1.getWeight(), o2.getWeight()));
        //playerList.sort(Comparator.comparingInt(BasketballPlayer::getWeight));
        playerList.sort(Comparator.comparing(BasketballPlayer::getWeight));

    }

    @Override
    public void sortByHeight() {
        playerList.sort(Comparator.comparing(BasketballPlayer::getHeight));
    }

    @Override
    public void sortByRating() {
        playerList.sort(Comparator.comparing(BasketballPlayer::getRating));
    }

    @Override
    public String toString() {
        return "RepositoryOfPlayers{" +
                "Size=" + playerList.size() +
                ", playerList=" + playerList +
                '}';
    }
}
