package zaawansowana.basketballplayer;

import java.util.Scanner;

public class AppDemo {

    public static void main(String[] args) {

        RepositoryOfPlayers repo = new RepositoryOfPlayers();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Witaj w programie");
        String menu = "Dokonaj wyboru:\n1.Dodaj zawodnika.\n2.Usuń zawodnika.\n3.Posortuj.\n4.Wyświetl."
        +"\n5.Sortuj wedlug nazwiksa\n6.Sortuj wedlug wzrostu\n7.Sortuj wedlug wagi\n8.Sortuj wedlug ratingu\n9.Wyswietl zawodnikow\n10.Zakończ program";

        do {
            System.out.println(menu);
            int selectedValue;
            try {
                selectedValue = Integer.valueOf(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Podaj właściwą wartość z przedziału 1-4");
                continue;
            }
            switch (selectedValue) {
                case 1:
                    System.out.println("Podaj dane zawodnika, oddzielając spacją imię nazwisko wzrost wagę rating.");
                    String data = scanner.nextLine();
                    BasketballPlayer player;
                    try {
                        player = new BasketballPlayer(data);
                    } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
                        System.out.println("Podano niewłaściwe dane zawodnika. Podaj prawidłowe.");
                        continue;
                    }
                    repo.add(player);
                    break;
                case 2:
                    System.out.println("Podaj dane zawodnika do usunięcia");
                    break;
                case 3:
                    System.out.println("Sortuję");
                    break;
                case 4:
                    System.out.println("Sortuję");
                    System.out.println(repo.getPlayers());
                    break;
                case 5:
                    repo.sortBySurname();
                    System.out.println("Sortuje wedlug nazwiska");
                    break;
                case 6:
                    repo.sortByHeight();
                    System.out.println("Sortuje wedlug wzrostu");
                    break;
                case 7:
                    repo.sortByWeight();
                    System.out.println("Sortuje wedlug wagi");
                    break;
                case 8:
                    repo.sortByRating();
                    System.out.println("Sortuje wedlug ratingu");
                    break;
                case 9:
                    System.out.println(repo.getPlayers());
                    break;
                case 10:
                    System.exit(-1);
                default:
                    System.out.println("Nie dokonano wlasciwego wyboru. Wybierz miedzy 1-10");
            }


        } while (true);

    }
}
