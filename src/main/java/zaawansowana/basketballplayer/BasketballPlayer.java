package zaawansowana.basketballplayer;

import java.util.Objects;

public class BasketballPlayer implements Comparable<BasketballPlayer>/* , Comparator<BasketballPlayer>*/ {
    private String name;
    private String surname;
    private int height;
    private int weight;
    private int rating;

    public BasketballPlayer(String name, String surname, int height, int weight, int rating) {
        this.name = name;
        this.surname = surname;
        this.height = height;
        this.weight = weight;
        this.rating = rating;
    }
    public BasketballPlayer(String data) {
        String [] playerData = data.split("\\s+");
        this.name = playerData[0];
        this.surname = playerData[1];
        this.height = Integer.valueOf(playerData[2]);
        this.weight = Integer.valueOf(playerData[3]);
        this.rating = Integer.valueOf(playerData[4]);
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getHeight() {
        return height;
    }

    public int getWeight() {
        return weight;
    }

    public int getRating() {
        return rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BasketballPlayer)) return false;
        BasketballPlayer that = (BasketballPlayer) o;
        return getHeight() == that.getHeight() && getWeight() == that.getWeight() && getRating() == that.getRating() && Objects.equals(getName(), that.getName()) && Objects.equals(getSurname(), that.getSurname());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname(), getHeight(), getWeight(), getRating());
    }

    @Override
    public int compareTo(BasketballPlayer o) {
        return this.getName().compareToIgnoreCase(o.getName());
    }

//    @Override
//    public int compare(BasketballPlayer p1, BasketballPlayer p2) {
//        return p1.getSurname().compareToIgnoreCase(p2.getSurname());
//    }

    @Override
    public String toString() {
        return "BasketballPlayer{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", rating=" + rating +
                '}';
    }



}
