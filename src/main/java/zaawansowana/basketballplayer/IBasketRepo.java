package zaawansowana.basketballplayer;

import java.util.List;

public interface IBasketRepo {
    boolean add(BasketballPlayer player);
    void remove(String name, String surname);
    void removeDuplicates();
    List<BasketballPlayer> getPlayers();
    void sortByName();
    void sortBySurname();
    void sortByWeight();
    void sortByHeight();
    void sortByRating();


}
