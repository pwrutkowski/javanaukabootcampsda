package zaawansowana.basketballplayer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class DygresjaApp {

    public static void main(String[] args) {
        List<BasketballPlayer>team = new ArrayList<>();

        team.add(new BasketballPlayer("Michael", "Jordan", 200, 100, 1));
        team.add(new BasketballPlayer("Michal", "Jordan", 200, 100, 1));

        Predicate<BasketballPlayer> p = new Predicate<BasketballPlayer>() {
            @Override
            public boolean test(BasketballPlayer basketballPlayer) { return basketballPlayer.getName().equals("Michael"); }
        };
        Predicate<BasketballPlayer> plambda = (BasketballPlayer player) -> player.getName().equalsIgnoreCase("Michael")
                && player.getSurname().equalsIgnoreCase("Jordan");

        System.out.println(team);
        team.removeIf(p);
        System.out.println(team);
    }
}
