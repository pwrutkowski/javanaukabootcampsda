package zaawansowana.basketballplayer;

public class BasketRepoApp {

    public static void main(String[] args) {

        IBasketRepo test = new RepositoryOfPlayers();
        test.add(new BasketballPlayer("Kamil", "Kowalski", 178, 87, 10));
        test.add(new BasketballPlayer("Paweł", "Rutkowski", 177, 78, 100));
        test.add(new BasketballPlayer("Michael", "Jordan", 200, 100, 1));
        test.add(new BasketballPlayer("Michał", "Jordan", 200, 100, 1));
        test.add(new BasketballPlayer("Michał", "Jordan", 200, 100, 1));
        test.add(new BasketballPlayer("Adam", "Słodowy", 180, 90, 150));

        System.out.println(test);

        test.remove("Kamill", "Kowalski");

        System.out.println(test);

        test.removeDuplicates();

        System.out.println(test);

        test.sortByName();

        System.out.println(test);

        test.sortBySurname();

        System.out.println(test);

        test.sortByWeight();

        System.out.println(test);
    }
}
