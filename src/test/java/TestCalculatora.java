import zaawansowana.calculator.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestCalculatora {

    int a = 20;
    int b = 5;
    int c = 2;
    Calculator calc;

    @BeforeEach
    public void before() {
        calc = new Calculator();
    }

    @Test
    public void testDodawaniaDwoch() {
        //Given

        //When
        int wynik = calc.add(a, b);
        //Then
        Assertions.assertEquals(25, wynik);
    }

    @Test
    public void testDodawaniaDwochSkrajnych() {
        //Given
        int a = -2147483648;
        int b = 2147483647;

        //When
        int wynik = calc.add(a, b);
        //Then
        Assertions.assertEquals(-1, wynik);
    }

    @Test
    public void testDodawaniaTrzech() {
        //Given

        //When
        int wynik = calc.add(a, b, c);

        //Then
        Assertions.assertEquals(27, wynik);
    }

    @Test
    public void testOdejmowaniaDwoch() {
        //Given

        //When
        double wynik = calc.subtract(a, b);
        //Then
        Assertions.assertEquals(15, wynik);
    }

    @Test
    public void testOdejmowaniaDwochSkrajnychUjemnych() {
        //Given
        int a = -2147483648;
        int b = -2147483647;
        //When
        int wynik = calc.subtract(a, b);
        //Then
        Assertions.assertEquals(-1, wynik);
    }

    @Test
    public void testOdejmowaniaDwochSkrajnychDodatnich() {
        //Given
        int a = 2147483647;
        int b = 2147483646;

        //When
        int wynik = calc.subtract(a, b);
        //Then
        Assertions.assertEquals(1, wynik);
    }

    @Test
    public void testOdejmowaniaTrzech() {
        //Given

        //When
        int wynik = calc.subtract(a, b, c);
        //Then
        Assertions.assertEquals(13, wynik);
    }

    @Test
    public void testMnozeniaDwoch() {
        //Given

        //When
        int wynik = calc.multiply(a, b);
        //Then
        Assertions.assertEquals(100, wynik);
    }

    @Test
    public void testMnozeniaTrzech() {
        //Given

        //When
        int wynik = calc.multiply(a, b, c);
        //Then
        Assertions.assertEquals(200, wynik);
    }

    @Test
    public void testMnozeniaZero() {
        //Given
        int a = 0;
        int b = 5;

        //When
        int wynik = calc.multiply(a, b);

        //Then
        Assertions.assertEquals(0, wynik);
    }

    @Test
    public void testDzieleniaDwoch() {
        //Given

        //When
        double wynik = calc.divide(a, b);

        //Then
        Assertions.assertEquals(4, wynik);
    }

    @Test
    public void testDzieleniaTrzech() {
        //Given

        //When
        double wynik = calc.divide(a, b, c);

        //Then
        Assertions.assertEquals(2, wynik);
    }

    @Test
    public void testDzieleniaZera() {
        //Given
        int a = 0;
        int b = 5;

        //When
        double wynik = calc.divide(a, b);

        //Then
        Assertions.assertEquals(0, wynik);
    }
/*
    @Test
    public void testDzieleniaPrzezZero() {

        //Given
        int a = 5;
        int b = 0;

        //When
        //Tu zaczynamy blok który spodziewa się wywalenia wyjątku z jakiegoś działania
        Exception except = Assertions.assertThrows(ArithmeticException.class, () -> {  //definicja jakiego błędu się spodziewamy (arytmetycznego)
            calc.divide(a, b);  //to jest to działanie
        });         // to jest klamra i nawias końca tego bloku
        String actualMessage = except.getMessage();
        String expectedMessage = "by zero";     //spodziewamy się że wywali błąd dzielenia przez zero

        //Then
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

 */

}