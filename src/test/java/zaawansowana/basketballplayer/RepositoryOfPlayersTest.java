package zaawansowana.basketballplayer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RepositoryOfPlayersTest {

    private RepositoryOfPlayers repo;
    private static BasketballPlayer jordan = new BasketballPlayer("Michael", "Jordan", 200, 100, 1);

    @BeforeEach
    void setUp() {
        repo = new RepositoryOfPlayers();
       // repo.add(jordan);
    }

    @Test
    void shouldAddPlayerToRepository() {
        Assertions.assertEquals(repo.getPlayers().size(), 0);
        repo.add(jordan);
        Assertions.assertEquals(repo.getPlayers().size(), 1);
    }

    @Test
    void shouldRemoveDuplicateFromRepository() {
        repo.add(jordan);
        repo.add(jordan);
        repo.add(jordan);
        Assertions.assertEquals(repo.getPlayers().size(), 3);
        repo.removeDuplicates();
        Assertions.assertEquals(repo.getPlayers().size(), 1);
    }

    @Test
    void shouldRemovePlayerByNameAndSurname() {
        repo.add(jordan);
        repo.add(jordan);
        Assertions.assertEquals(repo.getPlayers().size(), 2);
        repo.remove("Michael", "Jordan");
        Assertions.assertEquals(repo.getPlayers().size(), 0);
    }

    @Test
    void shouldSortPlayerByName() {
        repo.add(jordan);
        BasketballPlayer player = new BasketballPlayer("Adam", "Złotko", 195, 90, 5);
        repo.add(player);
        Assertions.assertEquals(repo.getPlayers().get(0), jordan);
        repo.sortByName();
        Assertions.assertEquals(repo.getPlayers().get(0), player);

    }

}